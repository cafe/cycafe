module unload gcc
module load gcc/9.3.0
rm -f PyCafe.cpp
rm -f PyCafe.h
rm -f PyCafe.pxd
ln -s PyCafe_sls.pxd PyCafe.pxd
rm -f PyCafe.pyx
ln -s PyCafe_sls.pyx PyCafe.pyx

source /opt/gfa/python 3.8
/ioc/python/latest/bin/python setup_py310.py build_ext -b ./python3.10/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.10/lib/${EPICS_HOST_ARCH}
