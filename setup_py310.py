import os
import sys
from distutils.core import setup
from distutils.extension import Extension
from Cython.Compiler.Main import default_options
default_options['emit_linenums'] = True
from Cython.Build import cythonize
from numpy import get_include

#runtime_library_dirs do not override LD_LIBRARY_PATH!
_CAFE_VERSION='1.14.4'
_EPICS_VERSION='3.12.8'

setup(
    ext_modules = cythonize([Extension('PyCafe',['PyCafe.pyx'],
        language="c++",
        include_dirs=[  '/ioc/python/latest/include/python3.10',
                        os.environ['EPICS'] + '/base/include',
                        os.environ['EPICS'] + '/base/include/os/Linux',
                        '/opt/gfa/cafe/boost/boost_1_61_0/include',
                        '/opt/gfa/cafe/boost/boost_1_61_0/include/boost',
                        '/opt/gfa/cafe/cpp/cafe-1.14.4-py310-gcc-9.3.0/include',                      
                        '.', get_include()],
        library_dirs=[  os.environ['EPICS'] + '/base/lib/' + os.environ['EPICS_HOST_ARCH'],
                        '/opt/gfa/cafe/cpp/cafe-1.14.4-py310-gcc-9.3.0/lib/' + os.environ['EPICS_HOST_ARCH'], 
                        '/ioc/python/latest/lib',
                        os.environ['PSI_PREFIX'] + '/Programming/gcc/9.3.0/lib64',
                        os.environ['PSI_PREFIX'] + '/Programming/gcc/9.3.0/lib'
                                          ],
        runtime_library_dirs=[os.environ['EPICS'] + '/base/lib/' + os.environ['EPICS_HOST_ARCH'],
                              '/opt/gfa/cafe/cpp/cafe-1.14.4-py310-gcc-9.3.0/lib/' + os.environ['EPICS_HOST_ARCH'], 
                              '/ioc/python/latest/lib',
                              os.environ['PSI_PREFIX'] + '/Programming/gcc/9.3.0/lib64',
                              os.environ['PSI_PREFIX'] + '/Programming/gcc/9.3.0/lib'
                                          ],
        libraries=['ca','Com','dl','cafe'])
                        ], annotate=True, 
                           compiler_directives={'embedsignature': False, 'language_level': 3, 
                               'c_string_type': 'str', 'c_string_encoding' : 'ascii',  
                               'py2_import': False, 'warn.unreachable': False,
                               'remove_unreachable': False},
                           compile_time_env={'PY_VERSION_HEX':sys.hexversion, 
                               'PY_EXT_C': True, 'BS_CAFE': False,
                               'CAFE_VERSION': _CAFE_VERSION, 'EPICS_VERSION': _EPICS_VERSION }
                           )
     )
