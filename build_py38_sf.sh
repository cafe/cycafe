module unload gcc
module load gcc/7.3.0
rm -f PyCafe.cpp
rm -f PyCafe.h
rm -f PyCafe.pxd
ln -s PyCafe_sf.pxd PyCafe.pxd
rm -f PyCafe.pyx
ln -s PyCafe_sf.pyx PyCafe.pyx
source /opt/gfa/python 3.8
python setup_py38_sf.py build_ext -b ./python3.8-sf/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.8-sf/lib/${EPICS_HOST_ARCH}
