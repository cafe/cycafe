module unload gcc
module load gcc/6.3.0
#rm -f PyCafe.cpp
#rm -f PyCafe.h
#rm -f PyCafe.pxd
#ln -s PyCafe_sls.pxd PyCafe.pxd
#rm -f PyCafe.pyx
#ln -s PyCafe_sls.pyx PyCafe.pyx
source /opt/gfa/python 3.5
python setup_py35_sls.py build_ext -b ./python3.5-sls/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.5-sls/lib/${EPICS_HOST_ARCH}
