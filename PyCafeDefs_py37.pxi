
# define threshold
cdef int PYCAFE_PRINT_HIGH = 9
cdef int PYCAFE_PRINT_MED = 5
cdef int PYCAFE_PRINT_LOW = 1
cdef int PYCAFE_PRINT_NONE = 0

cdef int PYCAFE_PRINT_LEVEL = PYCAFE_PRINT_NONE

#ctypedef shared_ptr[vector[double]] vdouble_ptr
#ctypedef shared_ptr[vector[float]] vfloat_ptr

IF PY_EXT_C:
    include "PyCafeDefs_pub.pxi"
ELSE:
    include "PyCafeDefs_api.pxi" 

'''

#Correct memory management a la KWSmith p.190
cdef class _finalizer:
	cdef void *_data
	def __dealloc__(self):		
		if self._data is not NULL:
			free(self._data)
		return
	
cdef void set_base(cnp.ndarray arr, void * carr):
		cdef _finalizer fin = _finalizer()
		fin._data = <void *> carr
		cnp.set_array_base(arr, fin)
		return
'''

cdef class CyCa:
    cdef readonly unsigned int CY_DBE_VALUE
    cdef readonly unsigned int CY_DBE_LOG
    cdef readonly unsigned int CY_DBE_ARCHIVE
    cdef readonly unsigned int CY_DBE_ALARM
    cdef readonly unsigned int CY_DBE_PROPERTY
    cdef readonly unsigned int CY_DBR_PRIMITIVE
    cdef readonly unsigned int CY_DBR_PLAIN
    cdef readonly unsigned int CY_DBR_STS
    cdef readonly unsigned int CY_DBR_TIME
    cdef readonly unsigned int CY_DBR_GR
    cdef readonly unsigned int CY_DBR_CTRL
    cdef readonly unsigned int CY_DBR_PUT
    cdef readonly unsigned int CY_DBR_STSACK
    cdef readonly unsigned int CY_DBR_CLASS
    cdef readonly unsigned int CY_DBR_NONE
    cdef readonly	unsigned int CY_DBR_STRING  # returns a NULL terminated string
    cdef readonly	unsigned int CY_DBR_SHORT  # returns an unsigned short
    cdef readonly	unsigned int CY_DBR_INT  # returns an unsigned short
    # returns an IEEE floating point value
    cdef readonly	unsigned int CY_DBR_FLOAT
    # returns an unsigned short which is the enum item
    cdef readonly	unsigned int CY_DBR_ENUM
    cdef readonly	unsigned int CY_DBR_CHAR  # returns an unsigned char
    cdef readonly	unsigned int CY_DBR_LONG  # returns an unsigned long
    # returns a double precision floating point number
    cdef readonly	unsigned int CY_DBR_DOUBLE
    # returns a string status structure (dbr_sts_string)
    cdef readonly	unsigned int CY_DBR_STS_STRING
    # returns a short status structure (dbr_sts_short)
    cdef readonly	unsigned int CY_DBR_STS_SHORT
    # returns a short status structure (dbr_sts_int)
    cdef readonly	unsigned int CY_DBR_STS_INT
    # returns a float status structure (dbr_sts_float)
    cdef readonly	unsigned int CY_DBR_STS_FLOAT
    # returns an enum status structure (dbr_sts_enum)
    cdef readonly	unsigned int CY_DBR_STS_ENUM
    # returns a char status structure (dbr_sts_char)
    cdef readonly	unsigned int CY_DBR_STS_CHAR
    # returns a long status structure (dbr_sts_long)
    cdef readonly	unsigned int CY_DBR_STS_LONG
    # returns a double status structure (dbr_sts_double)
    cdef readonly	unsigned int CY_DBR_STS_DOUBLE
    # returns a string time structure (dbr_time_string)
    cdef readonly	unsigned int CY_DBR_TIME_STRING
    # returns a short time structure (dbr_time_short)
    cdef readonly	unsigned int CY_DBR_TIME_SHORT
    # returns a short time structure (dbr_time_short)
    cdef readonly	unsigned int CY_DBR_TIME_INT
    # returns a float time structure (dbr_time_float)
    cdef readonly	unsigned int CY_DBR_TIME_FLOAT
    # returns an enum time structure (dbr_time_enum)
    cdef readonly	unsigned int CY_DBR_TIME_ENUM
    # returns a char time structure (dbr_time_char)
    cdef readonly	unsigned int CY_DBR_TIME_CHAR
    # returns a long time structure (dbr_time_long)
    cdef readonly	unsigned int CY_DBR_TIME_LONG
    # returns a double time structure (dbr_time_double)
    cdef readonly	unsigned int CY_DBR_TIME_DOUBLE
    # returns a graphic string structure (dbr_gr_string)
    cdef readonly	unsigned int CY_DBR_GR_STRING
    # returns a graphic short structure (dbr_gr_short)
    cdef readonly	unsigned int CY_DBR_GR_SHORT
    # returns a graphic short structure (dbr_gr_int)
    cdef readonly	unsigned int CY_DBR_GR_INT
    # returns a graphic float structure (dbr_gr_float)
    cdef readonly	unsigned int CY_DBR_GR_FLOAT
    # returns a graphic enum structure (dbr_gr_enum)
    cdef readonly	unsigned int CY_DBR_GR_ENUM
    # returns a graphic char structure (dbr_gr_char)
    cdef readonly	unsigned int CY_DBR_GR_CHAR
    # returns a graphic long structure (dbr_gr_long)
    cdef readonly	unsigned int CY_DBR_GR_LONG
    # returns a graphic double structure (dbr_gr_double)
    cdef readonly	unsigned int CY_DBR_GR_DOUBLE
    # returns a control string structure (dbr_ctrl_int)
    cdef readonly	unsigned int CY_DBR_CTRL_STRING
    # returns a control short structure (dbr_ctrl_short)
    cdef readonly	unsigned int CY_DBR_CTRL_SHORT
    # returns a control short structure (dbr_ctrl_int)
    cdef readonly	unsigned int CY_DBR_CTRL_INT
    # returns a control float structure (dbr_ctrl_float)
    cdef readonly	unsigned int CY_DBR_CTRL_FLOAT
    # returns a control enum structure (dbr_ctrl_enum)
    cdef readonly	unsigned int CY_DBR_CTRL_ENUM
    # returns a control char structure (dbr_ctrl_char)
    cdef readonly	unsigned int CY_DBR_CTRL_CHAR
    # returns a control long structure (dbr_ctrl_long)
    cdef readonly	unsigned int CY_DBR_CTRL_LONG
    # returns a control double structure (dbr_ctrl_double)
    cdef readonly	unsigned int CY_DBR_CTRL_DOUBLE

    # cdef readonly  int CY_ECA_TIMEOUT

    cdef readonly  int CY_ECA_NORMAL  # 1
    cdef readonly  int CY_ECA_ALLOCMEM  # 48
    cdef readonly  int CY_ECA_TOLARGE  # 72
    cdef readonly  int CY_ECA_TIMEOUT  # 80
    cdef readonly  int CY_ECA_BADTYPE  # 114
    cdef readonly  int CY_ECA_INTERNAL  # 142
    cdef readonly  int CY_ECA_GETFAIL  # 152
    cdef readonly  int CY_ECA_PUTFAIL  # 160
    cdef readonly  int CY_ECA_BADCOUNT  # 176
    cdef readonly  int CY_ECA_BADSTR  # 186
    cdef readonly  int CY_ECA_DISCONN  # 192
    cdef readonly  int CY_ECA_DBLCHNL  # 200
    cdef readonly  int CY_ECA_EVDISALLOW  # 210
    cdef readonly  int CY_ECA_BADMONID  # 242
    cdef readonly  int CY_ECA_BADMASK  # 330
    cdef readonly  int CY_ECA_IODONE  # 339
    cdef readonly  int CY_ECA_IOINPROGRESS  # 347
    cdef readonly  int CY_ECA_BADSYNCGRP  # 354
    cdef readonly  int CY_ECA_PUTCBINPROG  # 362
    cdef readonly  int CY_ECA_NORDACCESS  # 368
    cdef readonly  int CY_ECA_NOWTACCESS  # 376
    cdef readonly  int CY_ECA_ANACHRONISM  # 386
    cdef readonly  int CY_ECA_NOSEARCHADDR  # 392
    cdef readonly  int CY_ECA_NOCONVERT  # 400
    cdef readonly  int CY_ECA_BADCHID  # 410
    cdef readonly  int CY_ECA_BADFUNCPTR  # 418
    cdef readonly  int CY_ECA_ISATTACHED  # 424
    cdef readonly  int CY_ECA_UNAVAILINSERV  # 432
    cdef readonly  int CY_ECA_CHANDESTROY  # 440
    cdef readonly  int CY_ECA_BADPRIORITY  # 450
    cdef readonly  int CY_ECA_NOTTHREADED  # 458
    cdef readonly  int CY_ECA_16KARRAYCLIENT  # 464
    cdef readonly  int CY_ECA_CONNSEQTMO  # 472
    cdef readonly  int CY_ECA_UNRESPTMO  # 480

    cdef readonly  int ICAFE_CS_NEVER_CONN
    cdef readonly  int ICAFE_CS_PREV_CONN
    cdef readonly  int ICAFE_CS_CONN
    cdef readonly  int ICAFE_CS_CLOSED
    cdef readonly  int ICAFE_CS_DISCONN
    cdef readonly  int ICAFE_CS_UNKNOWN

    cdef readonly  int ICAFE_TYPENOTCONN
    cdef readonly  int ICAFE_RULE_FALSE
    cdef readonly  int ICAFE_BADCOUNT
    cdef readonly  int ICAFE_CALLBACK_NOT_YET_INVOKED
    cdef readonly  int ICAFE_WAITING_FOR_PREV_CALLBACK
    cdef readonly  int ICAFE_CACHE_EMPTY
    cdef readonly  int ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT
    cdef readonly  int ICAFE_MONITOR_DELAYED_AS_CONN_DOWN
    cdef readonly  int ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE
    cdef readonly  int ICAFE_SET_AND_GET_MISMATCH

    cdef readonly  int ICAFE_CA_OP_GET
    cdef readonly  int ICAFE_CA_OP_PUT
    cdef readonly  int ICAFE_CA_OP_CREATE_CHANNEL
    cdef readonly  int ICAFE_CA_OP_ADD_EVENT
    cdef readonly  int ICAFE_CA_OP_CLEAR_EVENT
    cdef readonly  int ICAFE_CA_OP_OTHER
    cdef readonly  int ICAFE_CA_OP_CONN_DOWN
    cdef readonly  int ICAFE_CA_OP_CONN_UP

    cdef readonly  int ICAFE_DAQ_RUN
    cdef readonly  int ICAFE_DAQ_PAUSED
    cdef readonly  int ICAFE_DAQ_STOPPED

    cdef readonly  int ECAFE_LOAD_COLLECTION
    cdef readonly  int ECAFE_LOAD_GROUP

    cdef readonly  int ICAFE_NORMAL
    cdef readonly  int ICAFE_SUCCESS

    cdef readonly  int ECAFE_NODATA
    cdef readonly  int	ECAFE_INVALID_TYPE
    cdef readonly  int ECAFE_BADCOUNT
    cdef readonly  int	ECAFE_BADSTR
    cdef readonly  int ECAFE_BADTYPE
    cdef readonly  int	ECAFE_NO_CONVERT
    cdef readonly  int ECAFE_NULLCONTEXT
    cdef readonly  int	ECAFE_NULLCHID
    cdef readonly  int ECAFE_NULLEVID
    cdef readonly  int	ECAFE_UNKNOWN_COLLECTION
    cdef readonly  int ECAFE_EMPTY_COLLECTION
    cdef readonly  int	ECAFE_COLLECTION_PREV_DEF
    cdef readonly  int ECAFE_COLLECTION_INVALID_MEMBER
    cdef readonly  int	ECAFE_RULE_FALSE
    cdef readonly  int ECAFE_UNKNOWN_GROUP
    cdef readonly  int	ECAFE_EMPTY_GROUP
    cdef readonly  int ECAFE_GROUP_PREV_DEF
    cdef readonly  int	ECAFE_INVALID_HANDLE
    cdef readonly  int ECAFE_INVALID_GROUP_HANDLE
    cdef readonly  int	ECAFE_NORDACCESS
    cdef readonly  int ECAFE_NOWTACCESS
    cdef readonly  int	ECAFE_TIMEOUT
    cdef readonly  int ECAFE_CANNOT_OPEN_FILE
    cdef readonly  int	ECAFE_INVALID_SWITCH_CASE
    cdef readonly  int	ECAFE_PVALIAS_PREV_DEF
    cdef readonly  int ECAFE_PVALIAS_INVALID
    cdef readonly  int ECAFE_PVNAME_PREV_DEF_AS_PVALIAS
    cdef readonly  int ECAFE_DEVICE_ATTRIB_NOT_FOUND
    cdef readonly  int ECAFE_HASH_UNIQUEID_EXISTS
    cdef readonly  int ECAFE_WRONG_CA_CONTEXT
    cdef readonly  int ECAFE_INVALID_CAFENUM_POLICY_TYPE
    cdef readonly  int ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED
    cdef readonly  int ECAFE_INVALID_ENUM_INDEX
    cdef readonly  int ECAFE_PVGROUP_GROUPHANDLE_MISMATCH
    cdef readonly  int ECAFE_TIMEOUT_SET_AND_MATCH
    cdef readonly  int ECAFE_HANDLE_MISMATCH_SET_AND_MATCH
    cdef readonly  int ECAFE_INCONSISTENT_CONTAINER_CORRECTED

    cdef readonly  int ECAFE_BPM_DATA_IS_INVALID

    cdef readonly  int ECAFE_BITSHUFF_DECOMPRESS
    cdef readonly  int ECAFE_BITSHUFF_ALLOCMEM
    cdef readonly  int ECAFE_BITSHUFF_REALLOCMEM
    cdef readonly  int ECAFE_BITSHUFF_BADCOUNT

    cdef readonly  int ECAFE_BSREAD_MULTIPART_MESS_NODATA
    cdef readonly  int ECAFE_BSREAD_PARSEFAIL_MAINHEADER
    cdef readonly  int ECAFE_BSREAD_PARSEFAIL_DATAHEADER
    cdef readonly  int ECAFE_BSREAD_ZMQSTREAM_NULL

    cdef readonly  int ERRNO_EINTR
    cdef readonly  int ERRNO_EAGAIN
    cdef readonly  int ERRNO_EFAULT
    cdef readonly  int ERRNO_EINVAL
    cdef readonly  int ERRNO_ENOTSOCK
    cdef readonly  int ERRNO_EPROTONOSUPPORT

    cdef readonly  int	SEV_NO_ALARM
    cdef readonly  int	SEV_MINOR
    cdef readonly  int	SEV_MAJOR
    cdef readonly  int	SEV_INVALID

    cdef readonly  int	STAT_NO_ALARM
    cdef readonly  int	STAT_READ
    cdef readonly  int	STAT_WRITE
    cdef readonly  int	STAT_HIHI
    cdef readonly  int	STAT_HIGH
    cdef readonly  int	STAT_LOLO
    cdef readonly  int	STAT_LOW
    cdef readonly  int	STAT_STATE
    cdef readonly  int	STAT_COS
    cdef readonly  int	STAT_COMM
    cdef readonly  int	STAT_TIMEOUT
    cdef readonly  int	STAT_HWLIMIT
    cdef readonly  int	STAT_CALC
    cdef readonly  int	STAT_SCAN
    cdef readonly  int	STAT_LINK
    cdef readonly  int	STAT_SOFT
    cdef readonly  int	STAT_BAD_SUB
    cdef readonly  int	STAT_UDF
    cdef readonly  int	STAT_DISABLE
    cdef readonly  int	STAT_SIMM
    cdef readonly  int	STAT_READ_ACCESS
    cdef readonly  int	STAT_WRITE_ACCESS

    # ctypedef enum ChannelRequestPolicyKind:
    cdef readonly unsigned int WITHOUT_CALLBACK
    cdef readonly unsigned int WITH_CALLBACK_DEFAULT
    cdef readonly unsigned int WITH_CALLBACK_USER_SUPPLIED

    # ChannelFlushSendBufferPolicyKind:
    cdef readonly unsigned int WITH_FLUSH_IO
    cdef readonly unsigned int WITH_PEND_IO
    cdef readonly unsigned int WITH_PEND_EVENT
    cdef readonly unsigned int WITH_POLL

    # ChannelWhenToFlushSendBufferPolicyKind:
    cdef readonly unsigned int FLUSH_AUTOMATIC
    cdef readonly unsigned int FLUSH_NOW
    cdef readonly unsigned int FLUSH_AFTER_EACH_CHANNEL_CREATION
    cdef readonly unsigned int FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION
    cdef readonly unsigned int FLUSH_AFTER_EACH_MESSAGE
    cdef readonly unsigned int FLUSH_AFTER_EACH_GROUP_CREATION
    cdef readonly unsigned int FLUSH_DESIGNATED_TO_CLIENT

    # ChannelGetCacheWaitPolicyKind:
    cdef readonly unsigned int GET_CACHE_NO_CHECK
    cdef readonly unsigned int GET_CACHE_NO_WAIT
    cdef readonly unsigned int GET_CACHE_NOW
    cdef readonly unsigned int GET_CACHE_WAIT

    # ChannelGetActionWhenMonitorPolicyKind:
    cdef readonly unsigned int	GET_FROM_CACHE
    cdef readonly unsigned int	GET_FROM_IOC

    # ChannelRequestDataTypePolicyKind:
    cdef readonly unsigned int NATIVE_DATATYPE
    # The smaller in byte size of type requested and native datatype
    cdef readonly unsigned int LOWEST_DATATYPE

    # ChannelWaitForResponsePolicyKind:
    cdef readonly unsigned int BLOCKING
    cdef readonly unsigned int WAIT
    cdef readonly unsigned int NON_BLOCKING
    cdef readonly unsigned int NO_WAIT

    # StatusMessageKind:
    cdef readonly unsigned int NO_MESSAGE
    cdef readonly unsigned int PRE_REQUEST
    cdef readonly unsigned int FROM_REQUEST
    cdef readonly unsigned int FROM_PEND
    cdef readonly unsigned int FROM_CALLBACK
    cdef readonly unsigned int FROM_MESSAGE

    # CallbackProgressKind:
    cdef readonly unsigned int NOT_INITIATED
    cdef readonly unsigned int PENDING
    cdef readonly unsigned int COMPLETE

    # deines.h:
    cdef readonly double DEFAULT_TIMEOUT_PEND_EVENT
    cdef readonly double DEFAULT_TIMEOUT_PEND_IO

    cdef readonly string INVALID_ENUM_RETURN_STRING
    cdef readonly short INVALID_ENUM_RETURN_VALUE

    cdef readonly unsigned short BSREAD_ZEROMQ_HIGH_WATER_MARK
    cdef readonly short          BSREAD_ZEROMQ_TIMEOUT_MS

    def __cinit__(self):

        #self.CY_ECA_TIMEOUT  		 =ECA_TIMEOUT
        self.CY_ECA_NORMAL = ECA_NORMAL
        self.CY_ECA_ALLOCMEM = ECA_ALLOCMEM
        self.CY_ECA_TOLARGE = ECA_TOLARGE
        self.CY_ECA_TIMEOUT = ECA_TIMEOUT
        self.CY_ECA_BADTYPE = ECA_BADTYPE
        self.CY_ECA_INTERNAL = ECA_INTERNAL
        self.CY_ECA_GETFAIL = ECA_GETFAIL
        self.CY_ECA_PUTFAIL = ECA_PUTFAIL
        self.CY_ECA_BADCOUNT = ECA_BADCOUNT
        self.CY_ECA_BADSTR = ECA_BADSTR
        self.CY_ECA_DISCONN = ECA_DISCONN
        self.CY_ECA_DBLCHNL = ECA_DBLCHNL
        self.CY_ECA_EVDISALLOW = ECA_EVDISALLOW
        self.CY_ECA_BADMONID = ECA_BADMONID
        self.CY_ECA_BADMASK = ECA_BADMASK
        self.CY_ECA_IODONE = ECA_IODONE
        self.CY_ECA_IOINPROGRESS = ECA_IOINPROGRESS
        self.CY_ECA_BADSYNCGRP = ECA_BADSYNCGRP
        self.CY_ECA_PUTCBINPROG = ECA_PUTCBINPROG
        self.CY_ECA_NORDACCESS = ECA_NORDACCESS
        self.CY_ECA_NOWTACCESS = ECA_NOWTACCESS
        self.CY_ECA_ANACHRONISM = ECA_ANACHRONISM
        self.CY_ECA_NOSEARCHADDR = ECA_NOSEARCHADDR
        self.CY_ECA_NOCONVERT = ECA_NOCONVERT
        self.CY_ECA_BADCHID = ECA_BADCHID
        self.CY_ECA_BADFUNCPTR = ECA_BADFUNCPTR
        self.CY_ECA_ISATTACHED = ECA_ISATTACHED
        self.CY_ECA_UNAVAILINSERV = ECA_UNAVAILINSERV
        self.CY_ECA_CHANDESTROY = ECA_CHANDESTROY
        self.CY_ECA_BADPRIORITY = ECA_BADPRIORITY
        self.CY_ECA_NOTTHREADED = ECA_NOTTHREADED
        self.CY_ECA_16KARRAYCLIENT = ECA_16KARRAYCLIENT
        self.CY_ECA_CONNSEQTMO = ECA_CONNSEQTMO
        self.CY_ECA_UNRESPTMO = ECA_UNRESPTMO

        self.ICAFE_NORMAL = ICAFE_NORMAL
        self.ICAFE_SUCCESS = ICAFE_SUCCESS

        self.ICAFE_CS_NEVER_CONN = ICAFE_CS_NEVER_CONN
        self.ICAFE_CS_PREV_CONN = ICAFE_CS_PREV_CONN
        self.ICAFE_CS_CONN = ICAFE_CS_CONN
        self.ICAFE_CS_CLOSED = ICAFE_CS_CLOSED
        self.ICAFE_CS_DISCONN = ICAFE_CS_DISCONN
        self.ICAFE_CS_UNKNOWN = ICAFE_CS_UNKNOWN

        self.ICAFE_TYPENOTCONN = ICAFE_TYPENOTCONN
        self.ICAFE_RULE_FALSE = ICAFE_RULE_FALSE
        self.ICAFE_BADCOUNT = ICAFE_BADCOUNT
        self.ICAFE_CALLBACK_NOT_YET_INVOKED = ICAFE_CALLBACK_NOT_YET_INVOKED
        self.ICAFE_WAITING_FOR_PREV_CALLBACK = ICAFE_WAITING_FOR_PREV_CALLBACK
        self.ICAFE_CACHE_EMPTY = ICAFE_CACHE_EMPTY
        self.ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT = ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT
        self.ICAFE_MONITOR_DELAYED_AS_CONN_DOWN = ICAFE_MONITOR_DELAYED_AS_CONN_DOWN
        self.ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE = ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE
        self.ICAFE_SET_AND_GET_MISMATCH = ICAFE_SET_AND_GET_MISMATCH

        self.ICAFE_DAQ_RUN = ICAFE_DAQ_RUN
        self.ICAFE_DAQ_PAUSED = ICAFE_DAQ_PAUSED
        self.ICAFE_DAQ_STOPPED = ICAFE_DAQ_STOPPED

        self.ICAFE_CA_OP_GET = ICAFE_CA_OP_GET
        self.ICAFE_CA_OP_PUT = ICAFE_CA_OP_PUT
        self.ICAFE_CA_OP_CREATE_CHANNEL = ICAFE_CA_OP_CREATE_CHANNEL
        self.ICAFE_CA_OP_ADD_EVENT = ICAFE_CA_OP_ADD_EVENT
        self.ICAFE_CA_OP_CLEAR_EVENT = ICAFE_CA_OP_CLEAR_EVENT
        self.ICAFE_CA_OP_OTHER = ICAFE_CA_OP_OTHER
        self.ICAFE_CA_OP_CONN_DOWN = ICAFE_CA_OP_CONN_DOWN
        self.ICAFE_CA_OP_CONN_UP = ICAFE_CA_OP_CONN_UP

        self.ECAFE_LOAD_COLLECTION = ECAFE_LOAD_COLLECTION
        self.ECAFE_LOAD_GROUP = ECAFE_LOAD_GROUP

        self.ECAFE_NODATA = ECAFE_NODATA
        self.ECAFE_INVALID_TYPE = ECAFE_INVALID_TYPE
        self.ECAFE_BADCOUNT = ECAFE_BADCOUNT
        self.ECAFE_BADSTR = ECAFE_BADSTR
        self.ECAFE_BADTYPE = ECAFE_BADTYPE
        self.ECAFE_NO_CONVERT = ECAFE_NO_CONVERT
        self.ECAFE_NULLCONTEXT = ECAFE_NULLCONTEXT
        self.ECAFE_NULLCHID = ECAFE_NULLCHID
        self.ECAFE_NULLEVID = ECAFE_NULLEVID
        self.ECAFE_UNKNOWN_COLLECTION = ECAFE_UNKNOWN_COLLECTION
        self.ECAFE_EMPTY_COLLECTION = ECAFE_EMPTY_COLLECTION
        self.ECAFE_COLLECTION_PREV_DEF = ECAFE_COLLECTION_PREV_DEF
        self.ECAFE_COLLECTION_INVALID_MEMBER = ECAFE_COLLECTION_INVALID_MEMBER
        self.ECAFE_RULE_FALSE = ECAFE_RULE_FALSE
        self.ECAFE_UNKNOWN_GROUP = ECAFE_UNKNOWN_GROUP
        self.ECAFE_EMPTY_GROUP = ECAFE_EMPTY_GROUP
        self.ECAFE_GROUP_PREV_DEF = ECAFE_GROUP_PREV_DEF
        self.ECAFE_INVALID_HANDLE = ECAFE_INVALID_HANDLE
        self.ECAFE_INVALID_GROUP_HANDLE = ECAFE_INVALID_GROUP_HANDLE
        self.ECAFE_NORDACCESS = ECAFE_NORDACCESS
        self.ECAFE_NOWTACCESS = ECAFE_NOWTACCESS
        self.ECAFE_TIMEOUT = ECAFE_TIMEOUT
        self.ECAFE_CANNOT_OPEN_FILE = ECAFE_CANNOT_OPEN_FILE
        self.ECAFE_INVALID_SWITCH_CASE = ECAFE_INVALID_SWITCH_CASE
        self.ECAFE_PVALIAS_PREV_DEF = ECAFE_PVALIAS_PREV_DEF
        self.ECAFE_PVALIAS_INVALID = ECAFE_PVALIAS_INVALID
        self.ECAFE_PVNAME_PREV_DEF_AS_PVALIAS = ECAFE_PVNAME_PREV_DEF_AS_PVALIAS
        self.ECAFE_DEVICE_ATTRIB_NOT_FOUND = ECAFE_DEVICE_ATTRIB_NOT_FOUND
        self.ECAFE_HASH_UNIQUEID_EXISTS = ECAFE_HASH_UNIQUEID_EXISTS
        self.ECAFE_WRONG_CA_CONTEXT = ECAFE_WRONG_CA_CONTEXT
        self.ECAFE_INVALID_CAFENUM_POLICY_TYPE = ECAFE_INVALID_CAFENUM_POLICY_TYPE
        self.ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED = ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED
        self.ECAFE_INVALID_ENUM_INDEX = ECAFE_INVALID_ENUM_INDEX
        self.ECAFE_PVGROUP_GROUPHANDLE_MISMATCH = ECAFE_PVGROUP_GROUPHANDLE_MISMATCH
        self.ECAFE_TIMEOUT_SET_AND_MATCH = ECAFE_TIMEOUT_SET_AND_MATCH
        self.ECAFE_HANDLE_MISMATCH_SET_AND_MATCH = ECAFE_HANDLE_MISMATCH_SET_AND_MATCH
        self.ECAFE_INCONSISTENT_CONTAINER_CORRECTED = ECAFE_INCONSISTENT_CONTAINER_CORRECTED

        self.ECAFE_BPM_DATA_IS_INVALID = ECAFE_BPM_DATA_IS_INVALID
        self.ECAFE_BITSHUFF_DECOMPRESS = ECAFE_BITSHUFF_DECOMPRESS
        self.ECAFE_BITSHUFF_ALLOCMEM = ECAFE_BITSHUFF_ALLOCMEM
        self.ECAFE_BITSHUFF_REALLOCMEM = ECAFE_BITSHUFF_REALLOCMEM
        self.ECAFE_BITSHUFF_BADCOUNT = ECAFE_BITSHUFF_BADCOUNT

        self.ECAFE_BSREAD_MULTIPART_MESS_NODATA = ECAFE_BSREAD_MULTIPART_MESS_NODATA
        self.ECAFE_BSREAD_PARSEFAIL_MAINHEADER = ECAFE_BSREAD_PARSEFAIL_MAINHEADER
        self.ECAFE_BSREAD_PARSEFAIL_DATAHEADER = ECAFE_BSREAD_PARSEFAIL_DATAHEADER
        self.ECAFE_BSREAD_ZMQSTREAM_NULL = ECAFE_BSREAD_ZMQSTREAM_NULL

        self.ERRNO_EINTR = ERRNO_EINTR
        self.ERRNO_EAGAIN = ERRNO_EAGAIN
        self.ERRNO_EFAULT = ERRNO_EFAULT
        self.ERRNO_EINVAL = ERRNO_EINVAL
        self.ERRNO_ENOTSOCK = ERRNO_ENOTSOCK
        self.ERRNO_EPROTONOSUPPORT = ERRNO_EPROTONOSUPPORT

        self.SEV_NO_ALARM = SEV_NO_ALARM
        self.SEV_MINOR = SEV_MINOR
        self.SEV_MAJOR = SEV_MAJOR
        self.SEV_INVALID = SEV_INVALID

        self.STAT_NO_ALARM = STAT_NO_ALARM
        self.STAT_READ = STAT_READ
        self.STAT_WRITE = STAT_WRITE
        self.STAT_HIHI = STAT_HIHI
        self.STAT_HIGH = STAT_HIGH
        self.STAT_LOLO = STAT_LOLO
        self.STAT_LOW = STAT_LOW
        self.STAT_STATE = STAT_STATE
        self.STAT_COS = STAT_COS
        self.STAT_COMM = STAT_COMM
        self.STAT_TIMEOUT = STAT_TIMEOUT
        self.STAT_HWLIMIT = STAT_HWLIMIT
        self.STAT_CALC = STAT_CALC
        self.STAT_SCAN = STAT_SCAN
        self.STAT_LINK = STAT_LINK
        self.STAT_SOFT = STAT_SOFT
        self.STAT_BAD_SUB = STAT_BAD_SUB
        self.STAT_UDF = STAT_UDF
        self.STAT_DISABLE = STAT_DISABLE
        self.STAT_SIMM = STAT_SIMM
        self.STAT_READ_ACCESS = STAT_READ_ACCESS
        self.STAT_WRITE_ACCESS = STAT_WRITE_ACCESS

        self.CY_DBE_VALUE = DBE_VALUE
        self.CY_DBE_LOG = DBE_LOG
        self.CY_DBE_ARCHIVE = DBE_ARCHIVE
        self.CY_DBE_ALARM = DBE_ALARM
        self.CY_DBE_PROPERTY = DBE_PROPERTY
        self.CY_DBR_PRIMITIVE = DBR_PRIMITIVE
        self.CY_DBR_PLAIN = DBR_PLAIN
        self.CY_DBR_STS = DBR_STS
        self.CY_DBR_TIME = DBR_TIME
        self.CY_DBR_GR = DBR_GR
        self.CY_DBR_CTRL = DBR_CTRL
        # DBR_PUT_ACKT and DBR_PUT_ACKS Write only - used from global alarm acknowledge.
        self.CY_DBR_PUT = DBR_PUT
        self.CY_DBR_STSACK = DBR_STSACK  # is DBR_STSACK_STRING
        self.CY_DBR_CLASS = DBR_CLASS   # is DBR_CLASS_NAME,
        self.CY_DBR_NONE = DBR_NONE
        self.CY_DBR_STRING = DBR_STRING
        self.CY_DBR_SHORT = DBR_SHORT  # returns an unsigned short
        self.CY_DBR_INT = DBR_INT  # returns an unsigned short
        self.CY_DBR_FLOAT = DBR_FLOAT  # returns an IEEE floating point value
        self.CY_DBR_ENUM = DBR_ENUM  # returns an unsigned short which is the enum item
        self.CY_DBR_CHAR = DBR_CHAR  # returns an unsigned char
        self.CY_DBR_LONG = DBR_LONG  # returns an unsigned long
        self.CY_DBR_DOUBLE = DBR_DOUBLE  # returns a double precision floating point number
        # returns a string status structure (dbr_sts_string)
        self.CY_DBR_STS_STRING = DBR_STS_STRING
        # returns a short status structure (dbr_sts_short)
        self.CY_DBR_STS_SHORT = DBR_STS_SHORT
        # returns a short status structure (dbr_sts_int)
        self.CY_DBR_STS_INT = DBR_STS_INT
        # returns a float status structure (dbr_sts_float)
        self.CY_DBR_STS_FLOAT = DBR_STS_FLOAT
        # returns an enum status structure (dbr_sts_enum)
        self.CY_DBR_STS_ENUM = DBR_STS_ENUM
        # returns a char status structure (dbr_sts_char)
        self.CY_DBR_STS_CHAR = DBR_STS_CHAR
        # returns a long status structure (dbr_sts_long)
        self.CY_DBR_STS_LONG = DBR_STS_LONG
        # returns a double status structure (dbr_sts_double)
        self.CY_DBR_STS_DOUBLE = DBR_STS_DOUBLE
        # returns a string time structure (dbr_time_string)
        self.CY_DBR_TIME_STRING = DBR_TIME_STRING
        # returns a short time structure (dbr_time_short)
        self.CY_DBR_TIME_SHORT = DBR_TIME_SHORT
        # returns a short time structure (dbr_time_short)
        self.CY_DBR_TIME_INT = DBR_TIME_INT
        # returns a float time structure (dbr_time_float)
        self.CY_DBR_TIME_FLOAT = DBR_TIME_FLOAT
        # returns an enum time structure (dbr_time_enum)
        self.CY_DBR_TIME_ENUM = DBR_TIME_ENUM
        # returns a char time structure (dbr_time_char)
        self.CY_DBR_TIME_CHAR = DBR_TIME_CHAR
        # returns a long time structure (dbr_time_long)
        self.CY_DBR_TIME_LONG = DBR_TIME_LONG
        # returns a double time structure (dbr_time_double)
        self.CY_DBR_TIME_DOUBLE = DBR_TIME_DOUBLE
        # returns a graphic string structure (dbr_gr_string)
        self.CY_DBR_GR_STRING = DBR_GR_STRING
        # returns a graphic short structure (dbr_gr_short)
        self.CY_DBR_GR_SHORT = DBR_GR_SHORT
        # returns a graphic short structure (dbr_gr_int)
        self.CY_DBR_GR_INT = DBR_GR_INT
        # returns a graphic float structure (dbr_gr_float)
        self.CY_DBR_GR_FLOAT = DBR_GR_FLOAT
        # returns a graphic enum structure (dbr_gr_enum)
        self.CY_DBR_GR_ENUM = DBR_GR_ENUM
        # returns a graphic char structure (dbr_gr_char)
        self.CY_DBR_GR_CHAR = DBR_GR_CHAR
        # returns a graphic long structure (dbr_gr_long)
        self.CY_DBR_GR_LONG = DBR_GR_LONG
        # returns a graphic double structure (dbr_gr_double)
        self.CY_DBR_GR_DOUBLE = DBR_GR_DOUBLE
        # returns a control string structure (dbr_ctrl_int)
        self.CY_DBR_CTRL_STRING = DBR_CTRL_STRING
        # returns a control short structure (dbr_ctrl_short)
        self.CY_DBR_CTRL_SHORT = DBR_CTRL_SHORT
        # returns a control short structure (dbr_ctrl_int)
        self.CY_DBR_CTRL_INT = DBR_CTRL_INT
        # returns a control float structure (dbr_ctrl_float)
        self.CY_DBR_CTRL_FLOAT = DBR_CTRL_FLOAT
        # returns a control enum structure (dbr_ctrl_enum)
        self.CY_DBR_CTRL_ENUM = DBR_CTRL_ENUM
        # returns a control char structure (dbr_ctrl_char)
        self.CY_DBR_CTRL_CHAR = DBR_CTRL_CHAR
        # returns a control long structure (dbr_ctrl_long)
        self.CY_DBR_CTRL_LONG = DBR_CTRL_LONG
        # returns a control double structure (dbr_ctrl_double)
        self.CY_DBR_CTRL_DOUBLE = DBR_CTRL_DOUBLE

        # ctypedef enum ChannelRequestPolicyKind:
        self.WITHOUT_CALLBACK = WITHOUT_CALLBACK
        self.WITH_CALLBACK_DEFAULT = WITH_CALLBACK_DEFAULT
        self.WITH_CALLBACK_USER_SUPPLIED = WITH_CALLBACK_USER_SUPPLIED

        # ChannelFlushSendBufferPolicyKind:
        self.WITH_FLUSH_IO = WITH_FLUSH_IO
        self.WITH_PEND_IO = WITH_PEND_IO
        self.WITH_PEND_EVENT = WITH_PEND_EVENT
        self.WITH_POLL = WITH_POLL

        # ChannelWhenToFlushSendBufferPolicyKind:
        self.FLUSH_AUTOMATIC = FLUSH_AUTOMATIC
        self.FLUSH_NOW = FLUSH_NOW
        self.FLUSH_AFTER_EACH_CHANNEL_CREATION = FLUSH_AFTER_EACH_CHANNEL_CREATION
        self.FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION = FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION
        self.FLUSH_AFTER_EACH_MESSAGE = FLUSH_AFTER_EACH_MESSAGE
        self.FLUSH_AFTER_EACH_GROUP_CREATION = FLUSH_AFTER_EACH_GROUP_CREATION
        self.FLUSH_DESIGNATED_TO_CLIENT = FLUSH_DESIGNATED_TO_CLIENT

        # ChannelGetCacheWaitPolicyKind:
        self.GET_CACHE_NO_CHECK = GET_CACHE_NO_CHECK
        self.GET_CACHE_NO_WAIT = GET_CACHE_NO_WAIT
        self.GET_CACHE_NOW = GET_CACHE_NOW
        self.GET_CACHE_WAIT = GET_CACHE_WAIT

        # ChannelGetActionWhenMonitorPolicyKind:
        self.GET_FROM_CACHE = GET_FROM_CACHE
        self.GET_FROM_IOC = GET_FROM_IOC

        # ChannelRequestDataTypePolicyKind:
        self.NATIVE_DATATYPE = NATIVE_DATATYPE
        # The smaller in byte size of type requested and native datatype
        self.LOWEST_DATATYPE = LOWEST_DATATYPE

        # ChannelWaitForResponsePolicyKind:
        self.BLOCKING = BLOCKING
        self.WAIT = WAIT
        self.NON_BLOCKING = NON_BLOCKING
        self.NO_WAIT = NO_WAIT

        # StatusMessageKind:
        self.NO_MESSAGE = NO_MESSAGE
        self.PRE_REQUEST = PRE_REQUEST
        self.FROM_REQUEST = FROM_REQUEST
        self.FROM_PEND = FROM_PEND
        self.FROM_CALLBACK = FROM_CALLBACK
        self.FROM_MESSAGE = FROM_MESSAGE

        # CallbackProgressKind:
        self.NOT_INITIATED = NOT_INITIATED
        self.PENDING = PENDING
        self.COMPLETE = COMPLETE

        # deines.h:
        self.DEFAULT_TIMEOUT_PEND_EVENT = DEFAULT_TIMEOUT_PEND_EVENT
        self.DEFAULT_TIMEOUT_PEND_IO = DEFAULT_TIMEOUT_PEND_IO
        self.INVALID_ENUM_RETURN_STRING = INVALID_ENUM_RETURN_STRING
        self.INVALID_ENUM_RETURN_VALUE = INVALID_ENUM_RETURN_VALUE
        self.BSREAD_ZEROMQ_HIGH_WATER_MARK = BSREAD_ZEROMQ_HIGH_WATER_MARK
        self.BSREAD_ZEROMQ_TIMEOUT_MS = BSREAD_ZEROMQ_TIMEOUT_MS



cdef class channelInfo:
    cdef readonly 	long  	  					channelID
    cdef readonly   bint                connectFlag
    cdef readonly   string			        hostName
    cdef readonly   unsigned int        nelem  # native
    cdef readonly   int		              dataType  # native
    cdef readonly   string              dataTypeAsString  # native
    cdef readonly   unsigned short      accessRead  # 0 or 1
    cdef readonly   unsigned short      accessWrite  # 0 or 1
    cdef readonly   string							className  # dbr_class_name_t
    cdef readonly   int                 connectionState  # as given by CA_OP_
    cdef readonly   int                 cafeConnectionState  # as given by cafe_cs_state
    cdef readonly   string              connectionStateAsString  # as given by CA_OP_
    # as given by cafe_cs_state
    cdef readonly   string              cafeConnectionStateAsString
    cdef CAFEDataTypeCode 		cdt
    cdef CAFEStatusCode   		csc
    cdef CAOPCodes   		       caop

    def __cinit__(self):
        self.channelID = 0
        self.connectFlag = False
        self.hostName = (< bytes > ("")).encode('UTF-8')	
        self.nelem = 0
        self.dataType = ICAFE_TYPENOTCONN
        self.dataTypeAsString = (< bytes > ("ICAFE_TYPENOTCONN")).encode('UTF-8')	
        self.accessRead = 0
        self.accessWrite = 0
        self.className = (< bytes > ("")).encode('UTF-8')	
        self.connectionState = ICAFE_CA_OP_CONN_DOWN
        self.cafeConnectionState = ICAFE_CS_NEVER_CONN
        self.connectionStateAsString = (< bytes > ('ICAFE_CA_OP_CONN_DOWN')).encode('UTF-8')	
        self.cafeConnectionStateAsString = (< bytes > ('ICAFE_CS_NEVER_CONN')).encode('UTF-8')	

    def show(self):
        print("channelID           = %s" % hex(self.channelID))
        print("connectFlag         = %d" % self.connectFlag)
        print("hostName            = %s" % self.hostName)
        print("nelem               = %d" % self.nelem)
        print("dataType            = %s (%d)" %
              (self.cdt.message(self.dataType), self.dataType))
        print("accessRead          = %d" % self.accessRead)
        print("accessWrite         = %d" % self.accessWrite)
        print("className           = %s" % self.className)
        #print ("connectionState     = %s (%d)" % (self.connectionStateAsString, self.connectionState))
        #print ("cafeConnectionState = %s (%d)" % (self.cafeConnectionStateAsString,self.cafeConnectionState))
        print("connectionState     = %s (%d)" %
              (self.caop.message(self.connectionState), self.connectionState))
        print("cafeConnectionState = %s (%d)" % (self.csc.message(
            self.cafeConnectionState), self.cafeConnectionState))
        return


##################################################################################
'''
cdef class _epicsTimeStampAsDate:	
	cdef epicsTime * _time	
	cdef local_tm_nano_sec local 
	cdef list  etsDate
	def __cinit__(self, epicsTimeStamp ts):	
	    		    
		self._time = new epicsTime(ts)
		#This gives	warning: dereferencing type-punned pointer will break strict-aliasing rules	
		self.local = <local_tm_nano_sec &> self._time
                ##April 2020 to avoid above warning
		##cdef local_tm_nano_sec * _local = <local_tm_nano_sec *>malloc(sizeof(epicsTime))
		##_local = <local_tm_nano_sec *> self._time
		##self.local = _local[0] #<local_tm_nano_sec &> self._time
		
		print ("ts===== ", ts.secPastEpoch, ts.nsec)		
		print ("year=== ", ( ( (&(&self.local).ansi_tm).tm_year) + 1900))
		
		self.etsDate=[]
		self.etsDate.append( (self.local.ansi_tm.tm_year + 1900))
		self.etsDate.append( (self.local.ansi_tm.tm_mon  + 1))
		self.etsDate.append( self.local.ansi_tm.tm_mday)
		self.etsDate.append( self.local.ansi_tm.tm_hour)
		self.etsDate.append( self.local.ansi_tm.tm_min)
		self.etsDate.append( self.local.ansi_tm.tm_sec)
		self.etsDate.append( <unsigned int> ts.nsec)
		self.etsDate.append( self.local.nSec)
		del self._time
		###free(_local)
	def get(self):
		return self.etsDate	
'''

##################################################################################

##################################################################################
cdef class pvctrl:
    cdef readonly unsigned int nelem
    cdef readonly int alarmStatus
    cdef readonly int alarmSeverity
    cdef readonly unsigned short precision
    cdef readonly str units
    cdef readonly unsigned short noEnumStrings
    cdef readonly list enumStrings
    cdef readonly upperDisplayLimit
    cdef readonly lowerDisplayLimit
    cdef readonly upperAlarmLimit
    cdef readonly upperWarningLimit
    cdef readonly lowerWarningLimit
    cdef readonly lowerAlarmLimit
    cdef readonly upperControlLimit
    cdef readonly lowerControlLimit
    cdef readonly list value
    cdef readonly int status
  
    def __cinit__(self):
        self.nelem = 1
        self.alarmStatus = -1
        self.alarmSeverity = -1
        self.precision = 0
        self.units = str("") #(< bytes > ("")).encode('UTF-8')	
        self.noEnumStrings = 0
        self.enumStrings = []
        self.upperDisplayLimit = 0
        self.lowerDisplayLimit = 0
        self.upperAlarmLimit = 0
        self.lowerAlarmLimit = 0
        self.upperControlLimit = 0
        self.lowerControlLimit = 0
        self.value = []
        self.status = 1
      

    def show(self):
        self.showPrint(self.nelem)
        return

    def showMax(self, nelem):
        if nelem > self.nelem:
            # print ("----------------------"
            # print ("Note: Max. Nelem =", self.nelem
            # print ("----------------------"
            nelem = self.nelem
        self.showPrint(nelem)
        return

    cdef showPrint(self, nelem):
        cdef unsigned int i
        # is self.value[0] a list
        if nelem == 1:
            if isinstance(self.value[0], list):             
                print("value         = ", self.value[0][0], end="")
            else:                
                print("value         =", self.value[0], end=""	)
        else:
            print("value	       = ", end="")
            
            if isinstance(self.value[0], list):
                for i in range(0, nelem):
                    print(self.value[0][i], "[%d]" % i, end=" ")
            else:
                for i in range(0, nelem):
                    print(self.value[i], "[%d]" % i, end=" ")

        print("")
        print("status        = %d" % self.status)
        print("alarmStatus   = %d" % self.alarmStatus)
        print("alarmSeverity = %d" % self.alarmSeverity)

        if self.noEnumStrings > 0:
            print("noEnumStrings = %d" % self.noEnumStrings)
            print("enumStrings   =",  end=" ")   # end for same line
            for i in range(0, self.noEnumStrings):
                print(self.enumStrings[i], "[%s]" % i, end=" ")
            print("")
        else:
            print("precision     = %d" % self.precision)
            print("units         = %s" % self.units)
            print("upperDisplayLimit = %f" % self.upperDisplayLimit)
            print("lowerDisplayLimit = %f" % self.lowerDisplayLimit)
            print("upperAlarmLimit   = %f" % self.upperAlarmLimit)
            print("lowerAlarmLimit   = %f" % self.lowerAlarmLimit)
            print("upperWarningLimit = %f" % self.upperWarningLimit)
            print("lowerWarningLimit = %f" % self.lowerWarningLimit)
            print("upperControlLimit = %f" % self.upperControlLimit)
            print("lowerControlLimit = %f" % self.lowerControlLimit)

      
##################################################################################

cdef class CafeException(Exception):
    cdef readonly str type
    cdef readonly str source
    cdef readonly unsigned int handle
    cdef readonly str name
    cdef readonly int error_code
    cdef readonly str error_text
    cdef readonly str error_info
   
    #Inheriting from base clas requires __init__; __cinit__ is ignored.
    def __init__(
    	self, _type="CafePVError",  _source=None,  _handle=0, 
       _pv_name=None, _error_code=-1, _error_text=None, _error_info=None):
        self.type = _type	
        self.source = _source
        self.handle = _handle
        self.name = _pv_name
        self.error_code = _error_code
        self.error_text = _error_text
        self.error_info = _error_info
    
    def show(self):
        print("------CafeException-----------------------------------------------------")
        if self.type is not None:
            print(self.type)
        print("Exception raised from CyCafe method:", self.source)
         
        if self.handle != 0:
            print("name =", self.name, "( handle =",
                  self.handle, ") reports the following error:")
            if (self.error_code != -1):
                print("error_code:", self.error_code, "error_text:", self.error_text)
            print("error_info:", self.error_info)
        elif self.type == 'CafePVError':
            print("name =", self.name, "( handle =",
                  self.handle, ") reports the following error:")
            if self.error_code != -1:  
                print("error_code:", self.error_code, "error_text:", self.error_text)
            print("error_info:", self.error_info)
        else:
            print("error_info:", self.error_info)
        print("------------------------------------------------------------------------")
        return

    def reveal(self):
        print("------CafeException-----------------------------------------------------")
        if self.type is not None:
            print(".type       =", self.type)
        print(".source     =", self.source)
        if self.handle != 0:
            print(".handle     =", self.handle)
        if self.name is not None:
            print(".name       =", self.name)
        if self.error_code != -1:
            print(".error_code =", self.error_code)
        if self.error_text is not None:
            print(".error_text =", self.error_text)
        print(".error_info =", self.error_info)
        print("------------------------------------------------------------------------")
        return


cdef class monitorpolicy:
    cdef readonly  chtype        dataType
    cdef readonly  chtype        dbrDataType
    cdef readonly  DBR_TYPE      cafeDbrType
    cdef readonly  unsigned int  nelem
    cdef readonly  unsigned int  mask
    # cdef readonly pCallback     handler
    cdef readonly  unsigned int  userArgs
    # cdef readonly  evid		          eventID
    cdef readonly  int           status
    cdef readonly  unsigned int  monid
    cdef readonly  bint maskHasDBE_PROPERTY
    cdef readonly  bint maskHasDBE_VALUE
    cdef readonly  bint maskHasDBE_LOG
    cdef readonly  bint maskHasDBE_ALARM

    def __cinit__(self):
        self.monid = 0
        # self.dataType=None
        # self.dbrDataType=None
        # self.cafeDbrType=None
        self.nelem = 1
        # self.mask=None
        # self.userArgs=None
        self.status = ICAFE_NORMAL
        self.maskHasDBE_PROPERTY = False
        self.maskHasDBE_VALUE = False
        self.maskHasDBE_LOG = False
        self.maskHasDBE_ALARM = False

    def show(self):
        print("monid      =", self.monid)
        print("datatype   =", self.dataType)
        print("dbrDataType=", self.dbrDataType)
        print("cafeDbrType=", self.cafeDbrType)
        print("nelem      =", self.nelem)
        print("mask       =", self.mask)
        print("userArgs   =", < long > self.userArgs)
        print("status     =", self.status)
        print("maskHasDBE_PROPERTY=", self.maskHasDBE_PROPERTY)
        print("maskHasDBE_VALUE   =", self.maskHasDBE_VALUE)
        print("maskHasDBE_LOG     =", self.maskHasDBE_LOG)
        print("maskHasDBE_ALARM   =", self.maskHasDBE_ALARM)
        return


##################################################################################
cdef class pvdata:
    cdef readonly unsigned int nelem
    cdef readonly int alarmStatus
    cdef readonly int alarmSeverity
    cdef readonly str alarmStatusAsString
    cdef readonly str alarmSeverityAsString
    cdef readonly list ts
    cdef readonly list tsDate
    cdef readonly str tsDateAsString
    cdef readonly str bsDateAsString
    cdef readonly list value
    cdef public int  status
    cdef public str  statusAsString
    cdef readonly unsigned long long pulseID
    cdef readonly int dataType
    cdef readonly str dataTypeAsString


    def __cinit__(self):
        self.nelem = 1
        self.alarmStatus = -1
        self.alarmSeverity = -1
        self.ts = []
        self.tsDate = []
        self.tsDateAsString = ''
        self.bsDateAsString = ''
        self.value = []
        self.status = 1
        self.statusAsString = 'ICAFE_NORMAL'
        self.pulseID = 0
        self.dataType = -1
        self.dataTypeAsString = ""
  
    def show(self):       
        self.showPrint(self.nelem)
        return

    def showMax(self, nelem):
        if nelem > self.nelem:
            # print ("----------------------"
            # print ("Note: Max. Nelem =", self.nelem
            # print ("----------------------"
            nelem = self.nelem

        self.showPrint(nelem)
        return

    cdef showPrint(self, nelem):
        cdef unsigned int i
        # is self.value[0] a list

        if nelem == 1:
            if isinstance(self.value[0], list):                
                print("value         = {} ({})".format(
                    self.value[0][0], self.dataTypeAsString), end="")
            else:                 
                print("value         = {} ({})".format(
                    self.value[0], self.dataTypeAsString), end="")
            print("")	    	    
        else:
            print("value	      = ", end="")

            if isinstance(self.value[0], list):
                for i in range(0, nelem):
                    print(self.value[0][i], "[%d]" % i, end=" ")
            else:
                for i in range(0, nelem):
                    print(self.value[i], "[%d]" % i, end=" ")             
            print("({0})".format(self.dataTypeAsString))

       
        print("status        = %d (%s)" % (self.status, self.statusAsString))
        if self.alarmStatus != -1:
            print("alarmStatus   = %d (%s)" %
                  (self.alarmStatus, self.alarmStatusAsString))
            print("alarmSeverity = %d (%s)" %
                  (self.alarmSeverity, self.alarmSeverityAsString))
        print("ts            =", self.ts[0], self.ts[1])
        print("tsDate        =", self.tsDate[0], self.tsDate[1],
              self.tsDate[2], self.tsDate[3],
              self.tsDate[4], self.tsDate[5], self.tsDate[6])
        if self.pulseID > 0:
            print("pulseID       =", self.pulseID)
        return

##################################################################################


##################################################################################
cdef class pvgroup:
    cdef public list pvdata
    cdef public unsigned int npv
    cdef public string name
    cdef readonly int  groupStatus
    cdef public unsigned int groupHandle
    cdef public bint rule
    cdef public bint hasAlarm
    cdef public bint hasTS

    def __cinit__(self):
        self.groupStatus = ICAFE_NORMAL
        self.rule = True
        self.hasAlarm = True
        self.hasTS = True
        
        return

    def show(self):
        self.showPrint(npv=self.npv, grouplist=None)
        return

    def showWithPV(self, glist):
        if len(glist) != self.npv:
            print("ERROR: GROUP MEMBER MISMATCH!!")
            print(glist, "has", len(glist),
                  "members while group has", self.npv, "!!")
            return
        self.showPrint(self.npv, grouplist=glist)
        return

    def showMax(self, _npv):
        if _npv > self.npv:
            # print ("----------------------"
            # print ("Note: Max. Npv =", self.npv
            # print ("----------------------"
            _npv = self.npv
        self.showPrint(npv=_npv, grouplist=None)
        return

    cdef showPrint(self, npv, grouplist):
        print("GROUP NAME   = ", self.name)
        print("GROUP HANDLE = ", self.groupHandle)
        print("GROUP STATUS = ", self.groupStatus)
        print("No. Members  = ", self.npv)  
        cdef unsigned int i, j
    
        for i in range(0, npv):
            print("Member: ", i, "---------------------------------")
            if grouplist:
                print("PV            =", grouplist[i])

            print("value         = ", end="")
            for j in range(0, len(self.pvdata[i].value)):
                print(self.pvdata[i].value[j], "[%d]" % j, end=" ")

            print("")
            print("status        = %d (%s)" %
                  (self.pvdata[i].status, self.pvdata[i].statusAsString))
            print("alarmStatus   = %d (%s)" % (
                self.pvdata[i].alarmStatus, self.pvdata[i].alarmStatusAsString))
            print("alarmSeverity = %d (%s)" % (
                self.pvdata[i].alarmSeverity, self.pvdata[i].alarmSeverityAsString))
            print("ts            =",
                  self.pvdata[i].ts[0], self.pvdata[i].ts[1])
            print("tsDate        =", self.pvdata[i].tsDate[0], self.pvdata[i].tsDate[1],
                  self.pvdata[i].tsDate[2], self.pvdata[i].tsDate[3],
                  self.pvdata[i].tsDate[4], self.pvdata[i].tsDate[5], self.pvdata[i].tsDate[6])
            print("--------------------------------------------")
        return

   
##################################################################################

cdef encodeString(bytes_input):
    bytesVal = bytes_input
    encoding = False		
    if not encoding:	
       try: 
           strVal = (bytesVal).decode('utf-8')
           encoding = True
           #print('utf-8 for umlauts CONFIRMED!')
       except UnicodeDecodeError:
           pass
    if not encoding:
        try: 
            strVal = (bytesVal).decode('utf-16')
            encoding = True
            #print('utf-16 for umlauts')
        except UnicodeDecodeError:
            pass
    if not encoding:
        try: 
            strVal= (bytesVal).decode('latin-1')
            encoding = True
            #print('latin1 for umlauts')		    
        except UnicodeDecodeError:
            pass
    if not encoding:
        strVal = bytes_input
    
    return strVal


##################################################################################
cdef getMatchedDataType(dt, dtn):
    # dt: input from user
    # dtn: native type
    # dtcheck: matching data type for pvd convert method

    cdef unsigned int dtcheck = dtn

    if dt in ['uchar', 'uint8']:
        dtcheck = CAFE_CHAR
    elif dt in ['np.uint8']:
        dtcheck = CAFE_CHAR
    elif dt in ['ushort', 'uint16']:
        dtcheck = CAFE_USHORT
    elif dt in ['np.ushort', 'np.uint16']:
        dtcheck = CAFE_USHORT
    elif dt in ['short', 'int16', 'int8', 'bool', 'byte']:
        dtcheck = CAFE_SHORT
    elif dt in ['np.short', 'np.int16', 'np.int8', 'np.bool_', 'np.byte']:
        dtcheck = CAFE_SHORT
    elif dt in ['int', 'int_', 'uint', 'ushort', 'int32', 'uint32']:
        dtcheck = CAFE_LONG
    elif dt in ['np.intc', 'np.int_', 'np.long', 'np.ushort', 'np.int32', 'np.uint32']:
        dtcheck = CAFE_LONG
    elif dt in ['double', 'float', 'float_', 'float64', 'int64', 'uint64']:
        dtcheck = CAFE_DOUBLE
    elif dt in ['np.float_', 'np.float64', 'np.long', 'np.ulong', 'np.int64', 'np.uint64']:
        dtcheck = CAFE_DOUBLE
    elif dt in ['float16', 'float32']:
        dtcheck = CAFE_FLOAT
    elif dt in ['np.float16', 'np.float32']:
        dtcheck = CAFE_FLOAT
    elif dt in ['string', 'str', 'unicode', 'string_', 'str_', 'unicode_']:
        dtcheck = CAFE_STRING
    elif dt in ['np.string', 'np.str', 'np.unicode', 'np.string_', 'np.str_', 'np.unicode_']:
        dtcheck = CAFE_STRING
    elif dt in ['native', '']:
        dtcheck = dtn  # need a line here
    else:
        print("Valid input parameters for data type are variations of: 'int', 'float', 'str', or 'native'")
        print("Data to be presented in native data type")

    return dtcheck
##################################################################################


#################################################################################

# cdef pvdata  p1 = pvdata()
# cdef pvctrl  c1 = pvctrl()


#################################################################################

cdef pvdata PVDataHolderToStruct(PVDataHolder pvd, dt=None):

    cdef CAFEDataTypeCode cdt
    #global p1
    cdef pvdata  p1 = pvdata()

    cdef unsigned int dtn = pvd.getDataType()
    cdef unsigned int dtcheck = dtn
    cdef localList = []

    cdef bytes bytesVal

    if dt:
        dtcheck = getMatchedDataType(dt, dtn)

    if dtcheck == CAFE_STRING:
        for i in range(0, pvd.getNelem()):
            bytesVal = <bytes> pvd.getAsString(i) 
            encoding = False  
            #if not encoding:
            #    try:                  
            #        strVal = (bVal).decode('latin-1')
            #        encoding = True    
            #    except UnicodeDecodeError:
            #        pass
            if not encoding:
                try: 
                    strVal = (bytesVal).decode('utf-8')
                    encoding = True
                    #print('utf-8 for umlauts CONFIRMED!')
                except UnicodeDecodeError:
                    pass
            if not encoding:
                try: 
                    strVal = (bytesVal).decode('utf-16')
                    encoding = True
                    #print('utf-16 for umlauts')
                except UnicodeDecodeError:
                    pass
            if not encoding:
                try: 
                    strVal= (bytesVal).decode('latin-1')
                    encoding = True
                    #print('latin1 for umlauts')		    
                except UnicodeDecodeError:
                    pass
            if not encoding:
                strVal =  pvd.getAsString(i)

            localList.append(strVal)

    elif dtcheck == CAFE_SHORT:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsLong(i))  # getAsShort(i)
    elif dtcheck == CAFE_FLOAT:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsDouble(i))  # getAsFloat(i)
    elif dtcheck == CAFE_ENUM:
        # if enum, string taken as native
        # if self._c_cafe.isEnum(handle)==1:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsString(i))
        # else:
        #	for i in range(0, pvd.getNelem()):
        #		localList.append(pvd.getAsLong(i)) #getAsUShort(i)
    elif dtcheck == CAFE_CHAR:
        for i in range(0, pvd.getNelem()):
            # <unsigned char> pvd.getAsChar(i)
            localList.append(pvd.getAsLong(i))
    elif dtcheck == CAFE_LONG:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsLong(i))
    elif dtcheck == CAFE_DOUBLE:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsDouble(i))
    else:
        localList.append(0)
        # print ("This line in PyCafe def getDataHolderToStruct should never appear!"
        # print ("No Data! Error. Is channel connected?"

    p1.value = localList
    p1.status = pvd.getStatus()
    p1.statusAsString = pvd.getStatusAsString()
    p1.nelem = pvd.getNelem()
    p1.alarmStatus = pvd.getAlarmStatus()
    p1.alarmSeverity = pvd.getAlarmSeverity()

    p1.alarmStatusAsString = pvd.getAlarmStatusAsString()
    p1.alarmSeverityAsString = pvd.getAlarmSeverityAsString()

    p1.pulseID = pvd.getPulseID()

    p1.dataType = pvd.getDataType()
    p1.dataTypeAsString = cdt.message(pvd.getDataType())

    p1.tsDateAsString = encodeString(<bytes> pvd.getEpicsTimeStampAsString())
    p1.bsDateAsString = encodeString(<bytes> pvd.getBSTimeStampAsString())

    #Mär 21, 2002 11:59:11.856568682
    #print( p1.tsDateAsString, "//",  p1.bsDateAsString)
    pvd._etsNorm = pvd.getEpicsTimeStampAsUInt32()

    cdef ll = []
    ll.append((pvd._etsNorm).secPastEpoch)
    ll.append((pvd._etsNorm).nsec)
    p1.ts = ll

    pvd._etsDate = pvd.getEpicsTimeStampAsDate()

    cdef ld = []
    ld.append((pvd._etsDate).year)
    ld.append((pvd._etsDate).mon)
    ld.append((pvd._etsDate).day)
    ld.append((pvd._etsDate).hour)
    ld.append((pvd._etsDate).min)
    ld.append((pvd._etsDate).sec)
    ld.append((pvd._etsDate).nsec)
    p1.tsDate = ld

    return p1
##################################################################################


#################################################################################

cdef pvctrl	PVCtrlHolderToStruct(PVCtrlHolder pvc, dt=None):

    #global c1
    cdef pvctrl c1 = pvctrl()

    cdef unsigned int dtn = pvc.getDataType()

    cdef unsigned int dtcheck = dtn
    cdef bytes bytesVal

    cdef localList = []

    if dt:
        dtcheck = getMatchedDataType(dt, dtn)

    if dtcheck == CAFE_STRING:
        for i in range(0, pvc.getNelem()):
            bytesVal = <bytes> pvc.getAsString(i)     
            try: 
                strVal = (bytesVal).decode('utf-8')
            except UnicodeDecodeError:
                pass
            try: 
                strVal = (bytesVal).decode('utf-16')
            except UnicodeDecodeError:
                pass
            try: 
                strVal= (bytesVal).decode('latin-1')
            except UnicodeDecodeError:
                strVal =  pvc.getAsString(i)     
            localList.append(strVal)
    elif dtcheck == CAFE_SHORT:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsLong(i))  # getAsShort(i)
    elif dtcheck == CAFE_FLOAT:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsDouble(i))  # getAsFloat(i)
    elif dtcheck == CAFE_ENUM:
        # if enum, string taken as native
        # if self._c_cafe.isEnum(handle)==1:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsString(i))
        # else:
        #	for i in range(0, pvd.getNelem()):
        #		localList.append(pvd.getAsLong(i)) #getAsUShort(i)
    elif dtcheck == CAFE_CHAR:
        for i in range(0, pvc.getNelem()):
            # <unsigned char> pvd.getAsChar(i)
            localList.append(pvc.getAsLong(i))
    elif dtcheck == CAFE_LONG:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsLong(i))
    elif dtcheck == CAFE_DOUBLE:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsDouble(i))
    else:
        localList.append(0)
        # print ("This line in PyCafe def getDataHolderToStruct should never appear!"
        # print ("No Data! Error. Is channel connected?"
    c1.value = localList
    c1.status = pvc.getStatus()
    c1.nelem = pvc.getNelem()
    c1.alarmStatus = pvc.getAlarmStatus()
    c1.alarmSeverity = pvc.getAlarmSeverity()

    c1.precision = pvc.getPrecision()
    c1.noEnumStrings = pvc.getNoEnumStrings()

    enumList = []
    for i in range(0, pvc.getNoEnumStrings()):
        enumList.append(pvc.getEnumStrings()[i])

    c1.enumStrings = enumList
    
    cdef bytes bVal
    try:
        c1.units = pvc.getUnits()
    except UnicodeDecodeError:
        #c1.units = str("")

        bVal = <bytes> pvc.getUnits()    
        c1.units = (bVal).decode('latin-1') 

    '''
    result = isinstance(pvc.getUnits(), (bytes,))

    if result:	
        c1.units = pvc.getUnits().decode('utf-8')
        print(str(c1.units + " is bytes"))
    else:
        c1.units = pvc.getUnits()  #AsString()
        print(str(c1.units + " has type " + str(type(pvc.getUnits()))))
    print(c1.units)
    '''

    c1.upperDisplayLimit = pvc.getUpperDispLimit_AsDouble()
    c1.lowerDisplayLimit = pvc.getLowerDispLimit_AsDouble()
    c1.upperAlarmLimit = pvc.getUpperAlarmLimit_AsDouble()
    c1.upperWarningLimit = pvc.getUpperWarningLimit_AsDouble()
    c1.lowerWarningLimit = pvc.getLowerWarningLimit_AsDouble()
    c1.lowerAlarmLimit = pvc.getLowerAlarmLimit_AsDouble()
    c1.upperControlLimit = pvc.getUpperCtrlLimit_AsDouble()
    c1.lowerControlLimit = pvc.getLowerCtrlLimit_AsDouble()

    return c1
##################################################################################


#################################################################################

cdef channelInfo channelRegaliaToStruct(ChannelRegalia cr):

    cdef channelInfo ci = channelInfo()
    cdef CAFEDataTypeCode cdt

    ci.channelID =  ( < long > cr.getChannelID())
    ci.connectFlag = cr.getConnectFlag()
    ci.hostName = cr.getHostNameAsString()
    ci.dataType = cr.getDataType()
    ci.dataTypeAsString = cdt.message(cr.getDataType())
    ci.className = cr.getClassNameAsString()
    ci.accessRead = cr.getAccessRead()
    ci.accessWrite = cr.getAccessWrite()
    ci.nelem = cr.getNelem()
    ci.connectionState = cr.getConnectionState()
    ci.cafeConnectionState = cr.getCafeConnectionState()
    ci.connectionStateAsString = cr.getConnectionStateAsString()
    ci.cafeConnectionStateAsString = cr.getCafeConnectionStateAsString()
    return ci
#################################################################################
