import os
import sys
from distutils.core import setup
from distutils.extension import Extension
from Cython.Compiler.Main import default_options
default_options['emit_linenums'] = True
from Cython.Build import cythonize
from numpy import get_include


_CAFE_VERSION='1.14.4'
_EPICS_VERSION='3.14.12'

setup(
    ext_modules = cythonize([Extension('PyCafe',['PyCafe.pyx'],
        language="c++",
        include_dirs=[  '/opt/gfa/python-3.8/latest/include/python3.8',
                        os.environ['EPICS'] + '/base/include',
                        os.environ['EPICS'] + '/base/include/os/Linux',                   
                        '/opt/gfa/cafe/boost/boost_1_61_0/include',
                        '/opt/gfa/cafe/boost/boost_1_61_0/include/boost',
                        '/opt/gfa/cafe/cpp/cafe-' + _CAFE_VERSION +
                        '-sf-py38-gcc-7.3.0/include',
                        '/opt/gfa/zmq/zeromq-4.2.3-gcc-6.3.0/include', 
                        '/opt/gfa/zmq/curl-7.54.1/include',
                        '/opt/gfa/zmsglog/json/jsoncpp-src-0.6.0-rc2/include',
                        '.', get_include()],
        library_dirs=[  os.environ['EPICS'] + '/base/lib/' + os.environ['EPICS_HOST_ARCH'],
                        '/opt/gfa/cafe/cpp/cafe-' + _CAFE_VERSION +
                        '-sf-py38-gcc-7.3.0/lib/' + os.environ['EPICS_HOST_ARCH'],	
                        '/opt/gfa/zmq/zeromq-4.2.3-gcc-6.3.0/lib',
                        '/opt/gfa/zmsglog/json/jsoncpp-src-0.6.0-rc2/libs/linux-gcc-6.3.0', 
                        '/opt/gfa/zmq/curl-7.54.1/lib',
                        '/opt/gfa/python-3.8/latest/lib',
                        os.environ['PSI_PREFIX'] + '/Programming/gcc/7.3.0/lib64',
                        os.environ['PSI_PREFIX'] + '/Programming/gcc/7.3.0/lib'
                                          ],
        runtime_library_dirs=[
                    os.environ['EPICS'] + '/base/lib/' + os.environ['EPICS_HOST_ARCH'],
                    '/opt/gfa/cafe/cpp/cafe-' + _CAFE_VERSION +
                    '-sf-py38-gcc-7.3.0/lib/' + os.environ['EPICS_HOST_ARCH'],
                    '/opt/gfa/zmq/zeromq-4.2.3-gcc-6.3.0/lib',
                    '/opt/gfa/zmsglog/json/jsoncpp-src-0.6.0-rc2/libs/linux-gcc-6.3.0', 
                    '/opt/gfa/zmq/curl-7.54.1/lib',
                    '/opt/gfa/python-3.8/latest/lib',
                    os.environ['PSI_PREFIX'] + '/Programming/gcc/7.3.0/lib64',
                    os.environ['PSI_PREFIX'] + '/Programming/gcc/7.3.0/lib'
                                          ],
        libraries=['ca','Com','dl','cafe'])
                        ], annotate=True, 
                           compiler_directives={'embedsignature': False, 'language_level': 3, 
                               'c_string_type': 'str', 'c_string_encoding' : 'ascii',  
                               'py2_import': False, 'warn.unreachable': False,
                               'remove_unreachable': False},
                           compile_time_env={'PY_VERSION_HEX':sys.hexversion, 
                                             'PY_EXT_C': True, 'BS_CAFE': True, 
                                             'CAFE_VERSION': _CAFE_VERSION,
                                             'EPICS_VERSION': _EPICS_VERSION}
                           )
     )
