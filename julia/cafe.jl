module MyModule

#First execute the following commands in your console window
#export PYTHONPATH=/opt/gfa/cafe/python/pycafe/pyzcafe-1.12.2-gcc-7.3.0

#Start Julia and change the python version from the default
#to Python 3.7.5 as follows:
#ENV["PYTHON"] = "/opt/gfa/python-3.7/latest/bin/python"
#using Pkg
#Pkg.build("PyCall")
#You may need to restart Julia

using PyCall
py"""
import sys
import time
import PyCafe 
cafe = PyCafe.CyCafe()
cyca = PyCafe.CyCa()

print(sys.version) #Should see 3.7.5
pvlist = ["SARFE10-PSSS059:SPECTRUM_X", "SATCL01-MQUA120:I-SET","SATCL01-MQUA120:I-READ", "PV-DOES-NOT:EXIST"]

#Quicker to open all channels with one call
cafe.openPrepare()
handle=cafe.open(pvlist)
cafe.openNowAndWait(0.4)


cafe.setNelem(handle[0], 10) #just read out 10 elements of wf

pvdata=cafe.getPV(handle[0])
print(pvlist[0])
pvdata.show() #shows struct
if pvdata.status == cyca.ICAFE_NORMAL:
   print("Value =", pvdata.value)  
   print("TimeStamp =", pvdata.tsDate)

print(pvlist[1])
pvdata=cafe.getPV(handle[1])
pvdata.show()
pvctrl=cafe.getCtrl(handle[1])
pvctrl.show()

cafe.monitor(handle[2])
for i in range (0, 10):
    val = cafe.getCache(handle[2])
    print (val)
    time.sleep(0.25)

value=cafe.get(handle[3])
print(pvlist[3])
if value is None:
   status=cafe.getStatus(handle[3])
   print("Status =", status, cafe.getStatusCodeAsText(status))  
 


"""


end