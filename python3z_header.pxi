cdef extern from "Python.h":
    ctypedef enum PyGILState_STATE:
        PyGILState_LOCKED = 0
        PyGILState_UNLOCKED = 1

    void PyEval_InitThreads()
    void Py_Initialize()
    PyGILState_STATE  PyGILState_Ensure()
    void PyGILState_Release(PyGILState_STATE)
    char* __FILE__
    int __LINE__


