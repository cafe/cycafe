module unload gcc
module load gcc/7.3.0
rm -f PyCafe_sf.cpp
rm -f PyCafe_sf.h
rm -f PyCafe.pxd
ln -s PyCafe_sf.pxd PyCafe.pxd
source /opt/gfa/python 3.5
python setup_py35_sf.py build_ext -b ./python3.5-sf/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.5-sf/lib/${EPICS_HOST_ARCH}
