
cdef object py_cb_handle = None
cdef object py_cb_handle_open = None

# ***********public or api***************

cdef api void py_cb_wrapper(PVDataHolder pvd, unsigned int handle, string pvname) with gil:
    #print(" py_cb_wrapper ====================================================+++++++++++++++PVDATA++++++=  Handle/PVNAME :", handle, pvname)
    cdef pvdata pv1 = PVDataHolderToStruct(pvd)

    for cbobjt, v in handleMonDictGlobal.items():
        if v == handle:
            cbobjt(pv1, handle, pvname)
    with nogil:
        return

cdef api void py_cb_ctrl_wrapper(PVCtrlHolder pvc, unsigned int handle, string pvname) with gil:
    #print(" py_cb_ctrl_wrapper ====================================================+++++++++++++++++CTRL+++++=  Handle/PVNAME :", handle, pvname)
    cdef pvctrl c1 = PVCtrlHolderToStruct(pvc)

    for cbobjt, v in handleMonDictGlobal.items():
        if v == handle:
            cbobjt(c1, handle, pvname)

    with nogil:
        return

cdef api void py_cb_handle_wrapper(unsigned int handle) with gil:
    #print(" py_cb_handle_wrapper ====================================================+++++++++++++++++HANDLE+++++=  Handle :", handle)
    py_cb_handle(handle)
    with nogil:
        return


# Default
cdef api void py_cb_handle_monid_wrapper(unsigned int handle, unsigned long monid) with gil:
    #print(" py_cb_handle_monid_wrapper ====================================================+++++++++++++MONID++++++++++=  Handle:", handle)
    # if monDictGlobal.has_key(monid):
    if monid in monDictGlobal.keys():
        cbobjt = monDictGlobal[monid]
        cbobjt(handle)
    else:
        print("CALLBACK NOT FOUND FOR handle/monid", handle, monid)
    with nogil:
        return


cdef api void py_cb_handle_get_wrapper(unsigned int handle) with gil:
    #print(" py_cb_handle_get_wrapper ====================================================+++++++++++++GET++++++++++=  Handle:", handle)
    # py_cb_handle_get(handle)
    if handle in getDictGlobal.keys():
        cbobjt = getDictGlobal[handle]
        cbobjt(handle)
    else:
        print("GET CALLBACK NOT FOUND FOR handle", handle)

    with nogil:
        return

cdef api void py_cb_handle_put_wrapper(unsigned int handle) with gil:
    #print(" py_cb_handle_put_wrapper ====================================================+++++++++++++PUT++++++++++=  Handle:", handle)
    # py_cb_handle_put(handle)
    if handle in putDictGlobal.keys():
        cbobjt = putDictGlobal[handle]
        cbobjt(handle)
    else:
        print("PUT CALLBACK NOT FOUND FOR handle", handle)

    with nogil:
        return


cdef api void py_cb_handle_open_wrapper(unsigned int handle, int status) with gil:
    # status reports ICAFE_CS_CONN (602) or ICAFE_CS_DISCONN (604)
    #print(" py_cb_handle_open_wrapper ====================================================+++++++++++++OPEN++++++++++=  Handle/Status:", handle, status)
    py_cb_handle_open(handle, status)
    with nogil:
        return

cdef api void py_cb_handle_connect_wrapper(unsigned int handle, string pvname, int status) with gil:
    print(" py_cb_handle_connect_wrapper ===========================+++CONNECT++++++++++=  Handle/PV/Status:", handle, pvname, status)
    #py_cb_handle_connect(handle, pvname, status)
   
    for k, y in openDictGlobal.items():
        print(k, y)
    # if openDictGlobal.has_key(pvname):
    if str(pvname) in openDictGlobal.keys():
        cbobjt = openDictGlobal[pvname]
        cbobjt(handle, pvname, status)
    with nogil:
        return


# ***********api***************
