module unload gcc
module load gcc/7.3.0
rm -f PyCafe_sf.cpp
rm -f PyCafe_sf.h
rm -f PyCafe.pxd
ln -s PyCafe_sf.pxd PyCafe.pxd
source /opt/gfa/python 3.7
python setup_py37_sf_local.py build_ext -b ./python3.7-sf/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.7-sf/lib/${EPICS_HOST_ARCH}
