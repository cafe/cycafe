# define threshold
cdef int PYCAFE_PRINT_HIGH = 9
cdef int PYCAFE_PRINT_MED = 5
cdef int PYCAFE_PRINT_LOW = 1
cdef int PYCAFE_PRINT_NONE = 0

cdef int PYCAFE_PRINT_LEVEL = PYCAFE_PRINT_NONE

#ctypedef shared_ptr[vector[double]] vdouble_ptr
#ctypedef shared_ptr[vector[float]] vfloat_ptr

IF PY_EXT_C:
    include "PyCafeDefs_pub.pxi"
ELSE:
    include "PyCafeDefs_api.pxi" 

'''

#Correct memory management a la KWSmith p.190
cdef class _finalizer:
	cdef void *_data
	def __dealloc__(self):		
		if self._data is not NULL:
			free(self._data)
		return
	
cdef void set_base(cnp.ndarray arr, void * carr):
		cdef _finalizer fin = _finalizer()
		fin._data = <void *> carr
		cnp.set_array_base(arr, fin)
		return
'''

cdef class CyCa:
    cpdef readonly unsigned int CY_DBE_VALUE
    cpdef readonly unsigned int CY_DBE_LOG
    cpdef readonly unsigned int CY_DBE_ARCHIVE
    cpdef readonly unsigned int CY_DBE_ALARM
    cpdef readonly unsigned int CY_DBE_PROPERTY
    cpdef readonly unsigned int CY_DBR_PRIMITIVE
    cpdef readonly unsigned int CY_DBR_PLAIN
    cpdef readonly unsigned int CY_DBR_STS
    cpdef readonly unsigned int CY_DBR_TIME
    cpdef readonly unsigned int CY_DBR_GR
    cpdef readonly unsigned int CY_DBR_CTRL
    cpdef readonly unsigned int CY_DBR_PUT
    cpdef readonly unsigned int CY_DBR_STSACK
    cpdef readonly unsigned int CY_DBR_CLASS
    cpdef readonly unsigned int CY_DBR_NONE
    cpdef readonly	unsigned int CY_DBR_STRING  # returns a NULL terminated string
    cpdef readonly	unsigned int CY_DBR_SHORT  # returns an unsigned short
    cpdef readonly	unsigned int CY_DBR_INT  # returns an unsigned short
    # returns an IEEE floating point value
    cpdef readonly	unsigned int CY_DBR_FLOAT
    # returns an unsigned short which is the enum item
    cpdef readonly	unsigned int CY_DBR_ENUM
    cpdef readonly	unsigned int CY_DBR_CHAR  # returns an unsigned char
    cpdef readonly	unsigned int CY_DBR_LONG  # returns an unsigned long
    # returns a double precision floating point number
    cpdef readonly	unsigned int CY_DBR_DOUBLE
    # returns a string status structure (dbr_sts_string)
    cpdef readonly	unsigned int CY_DBR_STS_STRING
    # returns a short status structure (dbr_sts_short)
    cpdef readonly	unsigned int CY_DBR_STS_SHORT
    # returns a short status structure (dbr_sts_int)
    cpdef readonly	unsigned int CY_DBR_STS_INT
    # returns a float status structure (dbr_sts_float)
    cpdef readonly	unsigned int CY_DBR_STS_FLOAT
    # returns an enum status structure (dbr_sts_enum)
    cpdef readonly	unsigned int CY_DBR_STS_ENUM
    # returns a char status structure (dbr_sts_char)
    cpdef readonly	unsigned int CY_DBR_STS_CHAR
    # returns a long status structure (dbr_sts_long)
    cpdef readonly	unsigned int CY_DBR_STS_LONG
    # returns a double status structure (dbr_sts_double)
    cpdef readonly	unsigned int CY_DBR_STS_DOUBLE
    # returns a string time structure (dbr_time_string)
    cpdef readonly	unsigned int CY_DBR_TIME_STRING
    # returns a short time structure (dbr_time_short)
    cpdef readonly	unsigned int CY_DBR_TIME_SHORT
    # returns a short time structure (dbr_time_short)
    cpdef readonly	unsigned int CY_DBR_TIME_INT
    # returns a float time structure (dbr_time_float)
    cpdef readonly	unsigned int CY_DBR_TIME_FLOAT
    # returns an enum time structure (dbr_time_enum)
    cpdef readonly	unsigned int CY_DBR_TIME_ENUM
    # returns a char time structure (dbr_time_char)
    cpdef readonly	unsigned int CY_DBR_TIME_CHAR
    # returns a long time structure (dbr_time_long)
    cpdef readonly	unsigned int CY_DBR_TIME_LONG
    # returns a double time structure (dbr_time_double)
    cpdef readonly	unsigned int CY_DBR_TIME_DOUBLE
    # returns a graphic string structure (dbr_gr_string)
    cpdef readonly	unsigned int CY_DBR_GR_STRING
    # returns a graphic short structure (dbr_gr_short)
    cpdef readonly	unsigned int CY_DBR_GR_SHORT
    # returns a graphic short structure (dbr_gr_int)
    cpdef readonly	unsigned int CY_DBR_GR_INT
    # returns a graphic float structure (dbr_gr_float)
    cpdef readonly	unsigned int CY_DBR_GR_FLOAT
    # returns a graphic enum structure (dbr_gr_enum)
    cpdef readonly	unsigned int CY_DBR_GR_ENUM
    # returns a graphic char structure (dbr_gr_char)
    cpdef readonly	unsigned int CY_DBR_GR_CHAR
    # returns a graphic long structure (dbr_gr_long)
    cpdef readonly	unsigned int CY_DBR_GR_LONG
    # returns a graphic double structure (dbr_gr_double)
    cpdef readonly	unsigned int CY_DBR_GR_DOUBLE
    # returns a control string structure (dbr_ctrl_int)
    cpdef readonly	unsigned int CY_DBR_CTRL_STRING
    # returns a control short structure (dbr_ctrl_short)
    cpdef readonly	unsigned int CY_DBR_CTRL_SHORT
    # returns a control short structure (dbr_ctrl_int)
    cpdef readonly	unsigned int CY_DBR_CTRL_INT
    # returns a control float structure (dbr_ctrl_float)
    cpdef readonly	unsigned int CY_DBR_CTRL_FLOAT
    # returns a control enum structure (dbr_ctrl_enum)
    cpdef readonly	unsigned int CY_DBR_CTRL_ENUM
    # returns a control char structure (dbr_ctrl_char)
    cpdef readonly	unsigned int CY_DBR_CTRL_CHAR
    # returns a control long structure (dbr_ctrl_long)
    cpdef readonly	unsigned int CY_DBR_CTRL_LONG
    # returns a control double structure (dbr_ctrl_double)
    cpdef readonly	unsigned int CY_DBR_CTRL_DOUBLE

    # cpdef readonly  int CY_ECA_TIMEOUT

    cpdef readonly  int CY_ECA_NORMAL  # 1
    cpdef readonly  int CY_ECA_ALLOCMEM  # 48
    cpdef readonly  int CY_ECA_TOLARGE  # 72
    cpdef readonly  int CY_ECA_TIMEOUT  # 80
    cpdef readonly  int CY_ECA_BADTYPE  # 114
    cpdef readonly  int CY_ECA_INTERNAL  # 142
    cpdef readonly  int CY_ECA_GETFAIL  # 152
    cpdef readonly  int CY_ECA_PUTFAIL  # 160
    cpdef readonly  int CY_ECA_BADCOUNT  # 176
    cpdef readonly  int CY_ECA_BADSTR  # 186
    cpdef readonly  int CY_ECA_DISCONN  # 192
    cpdef readonly  int CY_ECA_DBLCHNL  # 200
    cpdef readonly  int CY_ECA_EVDISALLOW  # 210
    cpdef readonly  int CY_ECA_BADMONID  # 242
    cpdef readonly  int CY_ECA_BADMASK  # 330
    cpdef readonly  int CY_ECA_IODONE  # 339
    cpdef readonly  int CY_ECA_IOINPROGRESS  # 347
    cpdef readonly  int CY_ECA_BADSYNCGRP  # 354
    cpdef readonly  int CY_ECA_PUTCBINPROG  # 362
    cpdef readonly  int CY_ECA_NORDACCESS  # 368
    cpdef readonly  int CY_ECA_NOWTACCESS  # 376
    cpdef readonly  int CY_ECA_ANACHRONISM  # 386
    cpdef readonly  int CY_ECA_NOSEARCHADDR  # 392
    cpdef readonly  int CY_ECA_NOCONVERT  # 400
    cpdef readonly  int CY_ECA_BADCHID  # 410
    cpdef readonly  int CY_ECA_BADFUNCPTR  # 418
    cpdef readonly  int CY_ECA_ISATTACHED  # 424
    cpdef readonly  int CY_ECA_UNAVAILINSERV  # 432
    cpdef readonly  int CY_ECA_CHANDESTROY  # 440
    cpdef readonly  int CY_ECA_BADPRIORITY  # 450
    cpdef readonly  int CY_ECA_NOTTHREADED  # 458
    cpdef readonly  int CY_ECA_16KARRAYCLIENT  # 464
    cpdef readonly  int CY_ECA_CONNSEQTMO  # 472
    cpdef readonly  int CY_ECA_UNRESPTMO  # 480

    cpdef readonly  int ICAFE_CS_NEVER_CONN
    cpdef readonly  int ICAFE_CS_PREV_CONN
    cpdef readonly  int ICAFE_CS_CONN
    cpdef readonly  int ICAFE_CS_CLOSED
    cpdef readonly  int ICAFE_CS_DISCONN
    cpdef readonly  int ICAFE_CS_UNKNOWN

    cpdef readonly  int ICAFE_TYPENOTCONN
    cpdef readonly  int ICAFE_RULE_FALSE
    cpdef readonly  int ICAFE_BADCOUNT
    cpdef readonly  int ICAFE_CALLBACK_NOT_YET_INVOKED
    cpdef readonly  int ICAFE_WAITING_FOR_PREV_CALLBACK
    cpdef readonly  int ICAFE_CACHE_EMPTY
    cpdef readonly  int ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT
    cpdef readonly  int ICAFE_MONITOR_DELAYED_AS_CONN_DOWN
    cpdef readonly  int ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE
    cpdef readonly  int ICAFE_SET_AND_GET_MISMATCH

    cpdef readonly  int ICAFE_CA_OP_GET
    cpdef readonly  int ICAFE_CA_OP_PUT
    cpdef readonly  int ICAFE_CA_OP_CREATE_CHANNEL
    cpdef readonly  int ICAFE_CA_OP_ADD_EVENT
    cpdef readonly  int ICAFE_CA_OP_CLEAR_EVENT
    cpdef readonly  int ICAFE_CA_OP_OTHER
    cpdef readonly  int ICAFE_CA_OP_CONN_DOWN
    cpdef readonly  int ICAFE_CA_OP_CONN_UP

    cpdef readonly  int ICAFE_DAQ_RUN
    cpdef readonly  int ICAFE_DAQ_PAUSED
    cpdef readonly  int ICAFE_DAQ_STOPPED

    cpdef readonly  int ECAFE_LOAD_COLLECTION
    cpdef readonly  int ECAFE_LOAD_GROUP

    cpdef readonly  int ICAFE_NORMAL
    cpdef readonly  int ICAFE_SUCCESS

    cpdef readonly  int ECAFE_NODATA
    cpdef readonly  int	ECAFE_INVALID_TYPE
    cpdef readonly  int ECAFE_BADCOUNT
    cpdef readonly  int	ECAFE_BADSTR
    cpdef readonly  int ECAFE_BADTYPE
    cpdef readonly  int	ECAFE_NO_CONVERT
    cpdef readonly  int ECAFE_NULLCONTEXT
    cpdef readonly  int	ECAFE_NULLCHID
    cpdef readonly  int ECAFE_NULLEVID
    cpdef readonly  int	ECAFE_UNKNOWN_COLLECTION
    cpdef readonly  int ECAFE_EMPTY_COLLECTION
    cpdef readonly  int	ECAFE_COLLECTION_PREV_DEF
    cpdef readonly  int ECAFE_COLLECTION_INVALID_MEMBER
    cpdef readonly  int	ECAFE_RULE_FALSE
    cpdef readonly  int ECAFE_UNKNOWN_GROUP
    cpdef readonly  int	ECAFE_EMPTY_GROUP
    cpdef readonly  int ECAFE_GROUP_PREV_DEF
    cpdef readonly  int	ECAFE_INVALID_HANDLE
    cpdef readonly  int ECAFE_INVALID_GROUP_HANDLE
    cpdef readonly  int	ECAFE_NORDACCESS
    cpdef readonly  int ECAFE_NOWTACCESS
    cpdef readonly  int	ECAFE_TIMEOUT
    cpdef readonly  int ECAFE_CANNOT_OPEN_FILE
    cpdef readonly  int	ECAFE_INVALID_SWITCH_CASE
    cpdef readonly  int	ECAFE_PVALIAS_PREV_DEF
    cpdef readonly  int ECAFE_PVALIAS_INVALID
    cpdef readonly  int ECAFE_PVNAME_PREV_DEF_AS_PVALIAS
    cpdef readonly  int ECAFE_DEVICE_ATTRIB_NOT_FOUND
    cpdef readonly  int ECAFE_HASH_UNIQUEID_EXISTS
    cpdef readonly  int ECAFE_WRONG_CA_CONTEXT
    cpdef readonly  int ECAFE_INVALID_CAFENUM_POLICY_TYPE
    cpdef readonly  int ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED
    cpdef readonly  int ECAFE_INVALID_ENUM_INDEX
    cpdef readonly  int ECAFE_PVGROUP_GROUPHANDLE_MISMATCH
    cpdef readonly  int ECAFE_TIMEOUT_SET_AND_MATCH
    cpdef readonly  int ECAFE_HANDLE_MISMATCH_SET_AND_MATCH
    cpdef readonly  int ECAFE_INCONSISTENT_CONTAINER_CORRECTED

    cpdef readonly  int ECAFE_BPM_DATA_IS_INVALID

    cpdef readonly  int ECAFE_BITSHUFF_DECOMPRESS
    cpdef readonly  int ECAFE_BITSHUFF_ALLOCMEM
    cpdef readonly  int ECAFE_BITSHUFF_REALLOCMEM
    cpdef readonly  int ECAFE_BITSHUFF_BADCOUNT

    cpdef readonly  int ECAFE_BSREAD_MULTIPART_MESS_NODATA
    cpdef readonly  int ECAFE_BSREAD_PARSEFAIL_MAINHEADER
    cpdef readonly  int ECAFE_BSREAD_PARSEFAIL_DATAHEADER
    cpdef readonly  int ECAFE_BSREAD_ZMQSTREAM_NULL

    cpdef readonly  int ERRNO_EINTR
    cpdef readonly  int ERRNO_EAGAIN
    cpdef readonly  int ERRNO_EFAULT
    cpdef readonly  int ERRNO_EINVAL
    cpdef readonly  int ERRNO_ENOTSOCK
    cpdef readonly  int ERRNO_EPROTONOSUPPORT

    cpdef readonly  int	SEV_NO_ALARM
    cpdef readonly  int	SEV_MINOR
    cpdef readonly  int	SEV_MAJOR
    cpdef readonly  int	SEV_INVALID

    cpdef readonly  int	STAT_NO_ALARM
    cpdef readonly  int	STAT_READ
    cpdef readonly  int	STAT_WRITE
    cpdef readonly  int	STAT_HIHI
    cpdef readonly  int	STAT_HIGH
    cpdef readonly  int	STAT_LOLO
    cpdef readonly  int	STAT_LOW
    cpdef readonly  int	STAT_STATE
    cpdef readonly  int	STAT_COS
    cpdef readonly  int	STAT_COMM
    cpdef readonly  int	STAT_TIMEOUT
    cpdef readonly  int	STAT_HWLIMIT
    cpdef readonly  int	STAT_CALC
    cpdef readonly  int	STAT_SCAN
    cpdef readonly  int	STAT_LINK
    cpdef readonly  int	STAT_SOFT
    cpdef readonly  int	STAT_BAD_SUB
    cpdef readonly  int	STAT_UDF
    cpdef readonly  int	STAT_DISABLE
    cpdef readonly  int	STAT_SIMM
    cpdef readonly  int	STAT_READ_ACCESS
    cpdef readonly  int	STAT_WRITE_ACCESS

    # ctypedef enum ChannelRequestPolicyKind:
    cpdef readonly unsigned int WITHOUT_CALLBACK
    cpdef readonly unsigned int WITH_CALLBACK_DEFAULT
    cpdef readonly unsigned int WITH_CALLBACK_USER_SUPPLIED

    # ChannelFlushSendBufferPolicyKind:
    cpdef readonly unsigned int WITH_FLUSH_IO
    cpdef readonly unsigned int WITH_PEND_IO
    cpdef readonly unsigned int WITH_PEND_EVENT
    cpdef readonly unsigned int WITH_POLL

    # ChannelWhenToFlushSendBufferPolicyKind:
    cpdef readonly unsigned int FLUSH_AUTOMATIC
    cpdef readonly unsigned int FLUSH_NOW
    cpdef readonly unsigned int FLUSH_AFTER_EACH_CHANNEL_CREATION
    cpdef readonly unsigned int FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION
    cpdef readonly unsigned int FLUSH_AFTER_EACH_MESSAGE
    cpdef readonly unsigned int FLUSH_AFTER_EACH_GROUP_CREATION
    cpdef readonly unsigned int FLUSH_DESIGNATED_TO_CLIENT

    # ChannelGetCacheWaitPolicyKind:
    cpdef readonly unsigned int GET_CACHE_NO_CHECK
    cpdef readonly unsigned int GET_CACHE_NO_WAIT
    cpdef readonly unsigned int GET_CACHE_NOW
    cpdef readonly unsigned int GET_CACHE_WAIT

    # ChannelGetActionWhenMonitorPolicyKind:
    cpdef readonly unsigned int	GET_FROM_CACHE
    cpdef readonly unsigned int	GET_FROM_IOC

    # ChannelRequestDataTypePolicyKind:
    cpdef readonly unsigned int NATIVE_DATATYPE
    # The smaller in byte size of type requested and native datatype
    cpdef readonly unsigned int LOWEST_DATATYPE

    # ChannelWaitForResponsePolicyKind:
    cpdef readonly unsigned int BLOCKING
    cpdef readonly unsigned int WAIT
    cpdef readonly unsigned int NON_BLOCKING
    cpdef readonly unsigned int NO_WAIT

    # StatusMessageKind:
    cpdef readonly unsigned int NO_MESSAGE
    cpdef readonly unsigned int PRE_REQUEST
    cpdef readonly unsigned int FROM_REQUEST
    cpdef readonly unsigned int FROM_PEND
    cpdef readonly unsigned int FROM_CALLBACK
    cpdef readonly unsigned int FROM_MESSAGE

    # CallbackProgressKind:
    cpdef readonly unsigned int NOT_INITIATED
    cpdef readonly unsigned int PENDING
    cpdef readonly unsigned int COMPLETE

    # deines.h:
    cpdef readonly double DEFAULT_TIMEOUT_PEND_EVENT
    cpdef readonly double DEFAULT_TIMEOUT_PEND_IO

    cpdef readonly string INVALID_ENUM_RETURN_STRING
    cpdef readonly short INVALID_ENUM_RETURN_VALUE

    cpdef readonly unsigned short BSREAD_ZEROMQ_HIGH_WATER_MARK
    cpdef readonly short          BSREAD_ZEROMQ_TIMEOUT_MS

    def __cinit__(self):

        #self.CY_ECA_TIMEOUT  		 =ECA_TIMEOUT
        self.CY_ECA_NORMAL = ECA_NORMAL
        self.CY_ECA_ALLOCMEM = ECA_ALLOCMEM
        self.CY_ECA_TOLARGE = ECA_TOLARGE
        self.CY_ECA_TIMEOUT = ECA_TIMEOUT
        self.CY_ECA_BADTYPE = ECA_BADTYPE
        self.CY_ECA_INTERNAL = ECA_INTERNAL
        self.CY_ECA_GETFAIL = ECA_GETFAIL
        self.CY_ECA_PUTFAIL = ECA_PUTFAIL
        self.CY_ECA_BADCOUNT = ECA_BADCOUNT
        self.CY_ECA_BADSTR = ECA_BADSTR
        self.CY_ECA_DISCONN = ECA_DISCONN
        self.CY_ECA_DBLCHNL = ECA_DBLCHNL
        self.CY_ECA_EVDISALLOW = ECA_EVDISALLOW
        self.CY_ECA_BADMONID = ECA_BADMONID
        self.CY_ECA_BADMASK = ECA_BADMASK
        self.CY_ECA_IODONE = ECA_IODONE
        self.CY_ECA_IOINPROGRESS = ECA_IOINPROGRESS
        self.CY_ECA_BADSYNCGRP = ECA_BADSYNCGRP
        self.CY_ECA_PUTCBINPROG = ECA_PUTCBINPROG
        self.CY_ECA_NORDACCESS = ECA_NORDACCESS
        self.CY_ECA_NOWTACCESS = ECA_NOWTACCESS
        self.CY_ECA_ANACHRONISM = ECA_ANACHRONISM
        self.CY_ECA_NOSEARCHADDR = ECA_NOSEARCHADDR
        self.CY_ECA_NOCONVERT = ECA_NOCONVERT
        self.CY_ECA_BADCHID = ECA_BADCHID
        self.CY_ECA_BADFUNCPTR = ECA_BADFUNCPTR
        self.CY_ECA_ISATTACHED = ECA_ISATTACHED
        self.CY_ECA_UNAVAILINSERV = ECA_UNAVAILINSERV
        self.CY_ECA_CHANDESTROY = ECA_CHANDESTROY
        self.CY_ECA_BADPRIORITY = ECA_BADPRIORITY
        self.CY_ECA_NOTTHREADED = ECA_NOTTHREADED
        self.CY_ECA_16KARRAYCLIENT = ECA_16KARRAYCLIENT
        self.CY_ECA_CONNSEQTMO = ECA_CONNSEQTMO
        self.CY_ECA_UNRESPTMO = ECA_UNRESPTMO

        self.ICAFE_NORMAL = ICAFE_NORMAL
        self.ICAFE_SUCCESS = ICAFE_SUCCESS

        self.ICAFE_CS_NEVER_CONN = ICAFE_CS_NEVER_CONN
        self.ICAFE_CS_PREV_CONN = ICAFE_CS_PREV_CONN
        self.ICAFE_CS_CONN = ICAFE_CS_CONN
        self.ICAFE_CS_CLOSED = ICAFE_CS_CLOSED
        self.ICAFE_CS_DISCONN = ICAFE_CS_DISCONN
        self.ICAFE_CS_UNKNOWN = ICAFE_CS_UNKNOWN

        self.ICAFE_TYPENOTCONN = ICAFE_TYPENOTCONN
        self.ICAFE_RULE_FALSE = ICAFE_RULE_FALSE
        self.ICAFE_BADCOUNT = ICAFE_BADCOUNT
        self.ICAFE_CALLBACK_NOT_YET_INVOKED = ICAFE_CALLBACK_NOT_YET_INVOKED
        self.ICAFE_WAITING_FOR_PREV_CALLBACK = ICAFE_WAITING_FOR_PREV_CALLBACK
        self.ICAFE_CACHE_EMPTY = ICAFE_CACHE_EMPTY
        self.ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT = ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT
        self.ICAFE_MONITOR_DELAYED_AS_CONN_DOWN = ICAFE_MONITOR_DELAYED_AS_CONN_DOWN
        self.ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE = ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE
        self.ICAFE_SET_AND_GET_MISMATCH = ICAFE_SET_AND_GET_MISMATCH

        self.ICAFE_DAQ_RUN = ICAFE_DAQ_RUN
        self.ICAFE_DAQ_PAUSED = ICAFE_DAQ_PAUSED
        self.ICAFE_DAQ_STOPPED = ICAFE_DAQ_STOPPED

        self.ICAFE_CA_OP_GET = ICAFE_CA_OP_GET
        self.ICAFE_CA_OP_PUT = ICAFE_CA_OP_PUT
        self.ICAFE_CA_OP_CREATE_CHANNEL = ICAFE_CA_OP_CREATE_CHANNEL
        self.ICAFE_CA_OP_ADD_EVENT = ICAFE_CA_OP_ADD_EVENT
        self.ICAFE_CA_OP_CLEAR_EVENT = ICAFE_CA_OP_CLEAR_EVENT
        self.ICAFE_CA_OP_OTHER = ICAFE_CA_OP_OTHER
        self.ICAFE_CA_OP_CONN_DOWN = ICAFE_CA_OP_CONN_DOWN
        self.ICAFE_CA_OP_CONN_UP = ICAFE_CA_OP_CONN_UP

        self.ECAFE_LOAD_COLLECTION = ECAFE_LOAD_COLLECTION
        self.ECAFE_LOAD_GROUP = ECAFE_LOAD_GROUP

        self.ECAFE_NODATA = ECAFE_NODATA
        self.ECAFE_INVALID_TYPE = ECAFE_INVALID_TYPE
        self.ECAFE_BADCOUNT = ECAFE_BADCOUNT
        self.ECAFE_BADSTR = ECAFE_BADSTR
        self.ECAFE_BADTYPE = ECAFE_BADTYPE
        self.ECAFE_NO_CONVERT = ECAFE_NO_CONVERT
        self.ECAFE_NULLCONTEXT = ECAFE_NULLCONTEXT
        self.ECAFE_NULLCHID = ECAFE_NULLCHID
        self.ECAFE_NULLEVID = ECAFE_NULLEVID
        self.ECAFE_UNKNOWN_COLLECTION = ECAFE_UNKNOWN_COLLECTION
        self.ECAFE_EMPTY_COLLECTION = ECAFE_EMPTY_COLLECTION
        self.ECAFE_COLLECTION_PREV_DEF = ECAFE_COLLECTION_PREV_DEF
        self.ECAFE_COLLECTION_INVALID_MEMBER = ECAFE_COLLECTION_INVALID_MEMBER
        self.ECAFE_RULE_FALSE = ECAFE_RULE_FALSE
        self.ECAFE_UNKNOWN_GROUP = ECAFE_UNKNOWN_GROUP
        self.ECAFE_EMPTY_GROUP = ECAFE_EMPTY_GROUP
        self.ECAFE_GROUP_PREV_DEF = ECAFE_GROUP_PREV_DEF
        self.ECAFE_INVALID_HANDLE = ECAFE_INVALID_HANDLE
        self.ECAFE_INVALID_GROUP_HANDLE = ECAFE_INVALID_GROUP_HANDLE
        self.ECAFE_NORDACCESS = ECAFE_NORDACCESS
        self.ECAFE_NOWTACCESS = ECAFE_NOWTACCESS
        self.ECAFE_TIMEOUT = ECAFE_TIMEOUT
        self.ECAFE_CANNOT_OPEN_FILE = ECAFE_CANNOT_OPEN_FILE
        self.ECAFE_INVALID_SWITCH_CASE = ECAFE_INVALID_SWITCH_CASE
        self.ECAFE_PVALIAS_PREV_DEF = ECAFE_PVALIAS_PREV_DEF
        self.ECAFE_PVALIAS_INVALID = ECAFE_PVALIAS_INVALID
        self.ECAFE_PVNAME_PREV_DEF_AS_PVALIAS = ECAFE_PVNAME_PREV_DEF_AS_PVALIAS
        self.ECAFE_DEVICE_ATTRIB_NOT_FOUND = ECAFE_DEVICE_ATTRIB_NOT_FOUND
        self.ECAFE_HASH_UNIQUEID_EXISTS = ECAFE_HASH_UNIQUEID_EXISTS
        self.ECAFE_WRONG_CA_CONTEXT = ECAFE_WRONG_CA_CONTEXT
        self.ECAFE_INVALID_CAFENUM_POLICY_TYPE = ECAFE_INVALID_CAFENUM_POLICY_TYPE
        self.ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED = ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED
        self.ECAFE_INVALID_ENUM_INDEX = ECAFE_INVALID_ENUM_INDEX
        self.ECAFE_PVGROUP_GROUPHANDLE_MISMATCH = ECAFE_PVGROUP_GROUPHANDLE_MISMATCH
        self.ECAFE_TIMEOUT_SET_AND_MATCH = ECAFE_TIMEOUT_SET_AND_MATCH
        self.ECAFE_HANDLE_MISMATCH_SET_AND_MATCH = ECAFE_HANDLE_MISMATCH_SET_AND_MATCH
        self.ECAFE_INCONSISTENT_CONTAINER_CORRECTED = ECAFE_INCONSISTENT_CONTAINER_CORRECTED

        self.ECAFE_BPM_DATA_IS_INVALID = ECAFE_BPM_DATA_IS_INVALID
        self.ECAFE_BITSHUFF_DECOMPRESS = ECAFE_BITSHUFF_DECOMPRESS
        self.ECAFE_BITSHUFF_ALLOCMEM = ECAFE_BITSHUFF_ALLOCMEM
        self.ECAFE_BITSHUFF_REALLOCMEM = ECAFE_BITSHUFF_REALLOCMEM
        self.ECAFE_BITSHUFF_BADCOUNT = ECAFE_BITSHUFF_BADCOUNT

        self.ECAFE_BSREAD_MULTIPART_MESS_NODATA = ECAFE_BSREAD_MULTIPART_MESS_NODATA
        self.ECAFE_BSREAD_PARSEFAIL_MAINHEADER = ECAFE_BSREAD_PARSEFAIL_MAINHEADER
        self.ECAFE_BSREAD_PARSEFAIL_DATAHEADER = ECAFE_BSREAD_PARSEFAIL_DATAHEADER
        self.ECAFE_BSREAD_ZMQSTREAM_NULL = ECAFE_BSREAD_ZMQSTREAM_NULL

        self.ERRNO_EINTR = ERRNO_EINTR
        self.ERRNO_EAGAIN = ERRNO_EAGAIN
        self.ERRNO_EFAULT = ERRNO_EFAULT
        self.ERRNO_EINVAL = ERRNO_EINVAL
        self.ERRNO_ENOTSOCK = ERRNO_ENOTSOCK
        self.ERRNO_EPROTONOSUPPORT = ERRNO_EPROTONOSUPPORT

        self.SEV_NO_ALARM = SEV_NO_ALARM
        self.SEV_MINOR = SEV_MINOR
        self.SEV_MAJOR = SEV_MAJOR
        self.SEV_INVALID = SEV_INVALID

        self.STAT_NO_ALARM = STAT_NO_ALARM
        self.STAT_READ = STAT_READ
        self.STAT_WRITE = STAT_WRITE
        self.STAT_HIHI = STAT_HIHI
        self.STAT_HIGH = STAT_HIGH
        self.STAT_LOLO = STAT_LOLO
        self.STAT_LOW = STAT_LOW
        self.STAT_STATE = STAT_STATE
        self.STAT_COS = STAT_COS
        self.STAT_COMM = STAT_COMM
        self.STAT_TIMEOUT = STAT_TIMEOUT
        self.STAT_HWLIMIT = STAT_HWLIMIT
        self.STAT_CALC = STAT_CALC
        self.STAT_SCAN = STAT_SCAN
        self.STAT_LINK = STAT_LINK
        self.STAT_SOFT = STAT_SOFT
        self.STAT_BAD_SUB = STAT_BAD_SUB
        self.STAT_UDF = STAT_UDF
        self.STAT_DISABLE = STAT_DISABLE
        self.STAT_SIMM = STAT_SIMM
        self.STAT_READ_ACCESS = STAT_READ_ACCESS
        self.STAT_WRITE_ACCESS = STAT_WRITE_ACCESS

        self.CY_DBE_VALUE = DBE_VALUE
        self.CY_DBE_LOG = DBE_LOG
        self.CY_DBE_ARCHIVE = DBE_ARCHIVE
        self.CY_DBE_ALARM = DBE_ALARM
        self.CY_DBE_PROPERTY = DBE_PROPERTY
        self.CY_DBR_PRIMITIVE = DBR_PRIMITIVE
        self.CY_DBR_PLAIN = DBR_PLAIN
        self.CY_DBR_STS = DBR_STS
        self.CY_DBR_TIME = DBR_TIME
        self.CY_DBR_GR = DBR_GR
        self.CY_DBR_CTRL = DBR_CTRL
        # DBR_PUT_ACKT and DBR_PUT_ACKS Write only - used from global alarm acknowledge.
        self.CY_DBR_PUT = DBR_PUT
        self.CY_DBR_STSACK = DBR_STSACK  # is DBR_STSACK_STRING
        self.CY_DBR_CLASS = DBR_CLASS   # is DBR_CLASS_NAME,
        self.CY_DBR_NONE = DBR_NONE
        self.CY_DBR_STRING = DBR_STRING
        self.CY_DBR_SHORT = DBR_SHORT  # returns an unsigned short
        self.CY_DBR_INT = DBR_INT  # returns an unsigned short
        self.CY_DBR_FLOAT = DBR_FLOAT  # returns an IEEE floating point value
        self.CY_DBR_ENUM = DBR_ENUM  # returns an unsigned short which is the enum item
        self.CY_DBR_CHAR = DBR_CHAR  # returns an unsigned char
        self.CY_DBR_LONG = DBR_LONG  # returns an unsigned long
        self.CY_DBR_DOUBLE = DBR_DOUBLE  # returns a double precision floating point number
        # returns a string status structure (dbr_sts_string)
        self.CY_DBR_STS_STRING = DBR_STS_STRING
        # returns a short status structure (dbr_sts_short)
        self.CY_DBR_STS_SHORT = DBR_STS_SHORT
        # returns a short status structure (dbr_sts_int)
        self.CY_DBR_STS_INT = DBR_STS_INT
        # returns a float status structure (dbr_sts_float)
        self.CY_DBR_STS_FLOAT = DBR_STS_FLOAT
        # returns an enum status structure (dbr_sts_enum)
        self.CY_DBR_STS_ENUM = DBR_STS_ENUM
        # returns a char status structure (dbr_sts_char)
        self.CY_DBR_STS_CHAR = DBR_STS_CHAR
        # returns a long status structure (dbr_sts_long)
        self.CY_DBR_STS_LONG = DBR_STS_LONG
        # returns a double status structure (dbr_sts_double)
        self.CY_DBR_STS_DOUBLE = DBR_STS_DOUBLE
        # returns a string time structure (dbr_time_string)
        self.CY_DBR_TIME_STRING = DBR_TIME_STRING
        # returns a short time structure (dbr_time_short)
        self.CY_DBR_TIME_SHORT = DBR_TIME_SHORT
        # returns a short time structure (dbr_time_short)
        self.CY_DBR_TIME_INT = DBR_TIME_INT
        # returns a float time structure (dbr_time_float)
        self.CY_DBR_TIME_FLOAT = DBR_TIME_FLOAT
        # returns an enum time structure (dbr_time_enum)
        self.CY_DBR_TIME_ENUM = DBR_TIME_ENUM
        # returns a char time structure (dbr_time_char)
        self.CY_DBR_TIME_CHAR = DBR_TIME_CHAR
        # returns a long time structure (dbr_time_long)
        self.CY_DBR_TIME_LONG = DBR_TIME_LONG
        # returns a double time structure (dbr_time_double)
        self.CY_DBR_TIME_DOUBLE = DBR_TIME_DOUBLE
        # returns a graphic string structure (dbr_gr_string)
        self.CY_DBR_GR_STRING = DBR_GR_STRING
        # returns a graphic short structure (dbr_gr_short)
        self.CY_DBR_GR_SHORT = DBR_GR_SHORT
        # returns a graphic short structure (dbr_gr_int)
        self.CY_DBR_GR_INT = DBR_GR_INT
        # returns a graphic float structure (dbr_gr_float)
        self.CY_DBR_GR_FLOAT = DBR_GR_FLOAT
        # returns a graphic enum structure (dbr_gr_enum)
        self.CY_DBR_GR_ENUM = DBR_GR_ENUM
        # returns a graphic char structure (dbr_gr_char)
        self.CY_DBR_GR_CHAR = DBR_GR_CHAR
        # returns a graphic long structure (dbr_gr_long)
        self.CY_DBR_GR_LONG = DBR_GR_LONG
        # returns a graphic double structure (dbr_gr_double)
        self.CY_DBR_GR_DOUBLE = DBR_GR_DOUBLE
        # returns a control string structure (dbr_ctrl_int)
        self.CY_DBR_CTRL_STRING = DBR_CTRL_STRING
        # returns a control short structure (dbr_ctrl_short)
        self.CY_DBR_CTRL_SHORT = DBR_CTRL_SHORT
        # returns a control short structure (dbr_ctrl_int)
        self.CY_DBR_CTRL_INT = DBR_CTRL_INT
        # returns a control float structure (dbr_ctrl_float)
        self.CY_DBR_CTRL_FLOAT = DBR_CTRL_FLOAT
        # returns a control enum structure (dbr_ctrl_enum)
        self.CY_DBR_CTRL_ENUM = DBR_CTRL_ENUM
        # returns a control char structure (dbr_ctrl_char)
        self.CY_DBR_CTRL_CHAR = DBR_CTRL_CHAR
        # returns a control long structure (dbr_ctrl_long)
        self.CY_DBR_CTRL_LONG = DBR_CTRL_LONG
        # returns a control double structure (dbr_ctrl_double)
        self.CY_DBR_CTRL_DOUBLE = DBR_CTRL_DOUBLE

        # ctypedef enum ChannelRequestPolicyKind:
        self.WITHOUT_CALLBACK = WITHOUT_CALLBACK
        self.WITH_CALLBACK_DEFAULT = WITH_CALLBACK_DEFAULT
        self.WITH_CALLBACK_USER_SUPPLIED = WITH_CALLBACK_USER_SUPPLIED

        # ChannelFlushSendBufferPolicyKind:
        self.WITH_FLUSH_IO = WITH_FLUSH_IO
        self.WITH_PEND_IO = WITH_PEND_IO
        self.WITH_PEND_EVENT = WITH_PEND_EVENT
        self.WITH_POLL = WITH_POLL

        # ChannelWhenToFlushSendBufferPolicyKind:
        self.FLUSH_AUTOMATIC = FLUSH_AUTOMATIC
        self.FLUSH_NOW = FLUSH_NOW
        self.FLUSH_AFTER_EACH_CHANNEL_CREATION = FLUSH_AFTER_EACH_CHANNEL_CREATION
        self.FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION = FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION
        self.FLUSH_AFTER_EACH_MESSAGE = FLUSH_AFTER_EACH_MESSAGE
        self.FLUSH_AFTER_EACH_GROUP_CREATION = FLUSH_AFTER_EACH_GROUP_CREATION
        self.FLUSH_DESIGNATED_TO_CLIENT = FLUSH_DESIGNATED_TO_CLIENT

        # ChannelGetCacheWaitPolicyKind:
        self.GET_CACHE_NO_CHECK = GET_CACHE_NO_CHECK
        self.GET_CACHE_NO_WAIT = GET_CACHE_NO_WAIT
        self.GET_CACHE_NOW = GET_CACHE_NOW
        self.GET_CACHE_WAIT = GET_CACHE_WAIT

        # ChannelGetActionWhenMonitorPolicyKind:
        self.GET_FROM_CACHE = GET_FROM_CACHE
        self.GET_FROM_IOC = GET_FROM_IOC

        # ChannelRequestDataTypePolicyKind:
        self.NATIVE_DATATYPE = NATIVE_DATATYPE
        # The smaller in byte size of type requested and native datatype
        self.LOWEST_DATATYPE = LOWEST_DATATYPE

        # ChannelWaitForResponsePolicyKind:
        self.BLOCKING = BLOCKING
        self.WAIT = WAIT
        self.NON_BLOCKING = NON_BLOCKING
        self.NO_WAIT = NO_WAIT

        # StatusMessageKind:
        self.NO_MESSAGE = NO_MESSAGE
        self.PRE_REQUEST = PRE_REQUEST
        self.FROM_REQUEST = FROM_REQUEST
        self.FROM_PEND = FROM_PEND
        self.FROM_CALLBACK = FROM_CALLBACK
        self.FROM_MESSAGE = FROM_MESSAGE

        # CallbackProgressKind:
        self.NOT_INITIATED = NOT_INITIATED
        self.PENDING = PENDING
        self.COMPLETE = COMPLETE

        # deines.h:
        self.DEFAULT_TIMEOUT_PEND_EVENT = DEFAULT_TIMEOUT_PEND_EVENT
        self.DEFAULT_TIMEOUT_PEND_IO = DEFAULT_TIMEOUT_PEND_IO
        self.INVALID_ENUM_RETURN_STRING = INVALID_ENUM_RETURN_STRING
        self.INVALID_ENUM_RETURN_VALUE = INVALID_ENUM_RETURN_VALUE
        self.BSREAD_ZEROMQ_HIGH_WATER_MARK = BSREAD_ZEROMQ_HIGH_WATER_MARK
        self.BSREAD_ZEROMQ_TIMEOUT_MS = BSREAD_ZEROMQ_TIMEOUT_MS



cdef class channelInfo:
    cdef readonly 	long  	  					channelID
    cdef readonly   bint                connectFlag
    cdef readonly   string			        hostName
    cdef readonly   unsigned int        nelem  # native
    cdef readonly   int		              dataType  # native
    cdef readonly   string              dataTypeAsString  # native
    cdef readonly   unsigned short      accessRead  # 0 or 1
    cdef readonly   unsigned short      accessWrite  # 0 or 1
    cdef readonly   string							className  # dbr_class_name_t
    cdef readonly   int                 connectionState  # as given by CA_OP_
    cdef readonly   int                 cafeConnectionState  # as given by cafe_cs_state
    cdef readonly   string              connectionStateAsString  # as given by CA_OP_
    # as given by cafe_cs_state
    cdef readonly   string              cafeConnectionStateAsString
    cdef CAFEDataTypeCode 		cdt
    cdef CAFEStatusCode   		csc
    cdef CAOPCodes   		       caop

    def __cinit__(self):
        self.channelID = 0
        self.connectFlag = False
        self.hostName = (< bytes > ("")).encode('UTF-8')	
        self.nelem = 0
        self.dataType = ICAFE_TYPENOTCONN
        self.dataTypeAsString = (< bytes > ("ICAFE_TYPENOTCONN")).encode('UTF-8')	
        self.accessRead = 0
        self.accessWrite = 0
        self.className = (< bytes > ("")).encode('UTF-8')	
        self.connectionState = ICAFE_CA_OP_CONN_DOWN
        self.cafeConnectionState = ICAFE_CS_NEVER_CONN
        self.connectionStateAsString = (< bytes > ('ICAFE_CA_OP_CONN_DOWN')).encode('UTF-8')	
        self.cafeConnectionStateAsString = (< bytes > ('ICAFE_CS_NEVER_CONN')).encode('UTF-8')	

    def show(self):
        print("channelID           = %s" % hex(self.channelID))
        print("connectFlag         = %d" % self.connectFlag)
        print("hostName            = %s" % self.hostName)
        print("nelem               = %d" % self.nelem)
        print("dataType            = %s (%d)" %
              (self.cdt.message(self.dataType), self.dataType))
        print("accessRead          = %d" % self.accessRead)
        print("accessWrite         = %d" % self.accessWrite)
        print("className           = %s" % self.className)
        #print ("connectionState     = %s (%d)" % (self.connectionStateAsString, self.connectionState))
        #print ("cafeConnectionState = %s (%d)" % (self.cafeConnectionStateAsString,self.cafeConnectionState))
        print("connectionState     = %s (%d)" %
              (self.caop.message(self.connectionState), self.connectionState))
        print("cafeConnectionState = %s (%d)" % (self.csc.message(
            self.cafeConnectionState), self.cafeConnectionState))
        return


##################################################################################
'''
cdef class _epicsTimeStampAsDate:	
	cdef epicsTime * _time	
	cdef local_tm_nano_sec local 
	cdef list  etsDate
	def __cinit__(self, epicsTimeStamp ts):	
	    		    
		self._time = new epicsTime(ts)
		#This gives	warning: dereferencing type-punned pointer will break strict-aliasing rules	
		self.local = <local_tm_nano_sec &> self._time
                ##April 2020 to avoid above warning
		##cdef local_tm_nano_sec * _local = <local_tm_nano_sec *>malloc(sizeof(epicsTime))
		##_local = <local_tm_nano_sec *> self._time
		##self.local = _local[0] #<local_tm_nano_sec &> self._time
		
		print ("ts===== ", ts.secPastEpoch, ts.nsec)		
		print ("year=== ", ( ( (&(&self.local).ansi_tm).tm_year) + 1900))
		
		self.etsDate=[]
		self.etsDate.append( (self.local.ansi_tm.tm_year + 1900))
		self.etsDate.append( (self.local.ansi_tm.tm_mon  + 1))
		self.etsDate.append( self.local.ansi_tm.tm_mday)
		self.etsDate.append( self.local.ansi_tm.tm_hour)
		self.etsDate.append( self.local.ansi_tm.tm_min)
		self.etsDate.append( self.local.ansi_tm.tm_sec)
		self.etsDate.append( <unsigned int> ts.nsec)
		self.etsDate.append( self.local.nSec)
		del self._time
		###free(_local)
	def get(self):
		return self.etsDate	
'''

##################################################################################

##################################################################################
cdef class pvctrl:
    cdef readonly unsigned int nelem
    cdef readonly int alarmStatus
    cdef readonly int alarmSeverity
    cdef readonly unsigned short precision
    cdef readonly str units
    cdef readonly unsigned short noEnumStrings
    cdef readonly list enumStrings
    cdef readonly upperDisplayLimit
    cdef readonly lowerDisplayLimit
    cdef readonly upperAlarmLimit
    cdef readonly upperWarningLimit
    cdef readonly lowerWarningLimit
    cdef readonly lowerAlarmLimit
    cdef readonly upperControlLimit
    cdef readonly lowerControlLimit
    cdef readonly list value
    cdef readonly int status
  
    def __cinit__(self):
        self.nelem = 1
        self.alarmStatus = -1
        self.alarmSeverity = -1
        self.precision = 0
        self.units = str("") #(< bytes > ("")).encode('UTF-8')	
        self.noEnumStrings = 0
        self.enumStrings = []
        self.upperDisplayLimit = 0
        self.lowerDisplayLimit = 0
        self.upperAlarmLimit = 0
        self.lowerAlarmLimit = 0
        self.upperControlLimit = 0
        self.lowerControlLimit = 0
        self.value = []
        self.status = 1
      

    def show(self):
        self.showPrint(self.nelem)
        return

    def showMax(self, nelem):
        if nelem > self.nelem:
            # print ("----------------------"
            # print ("Note: Max. Nelem =", self.nelem
            # print ("----------------------"
            nelem = self.nelem
        self.showPrint(nelem)
        return

    cdef showPrint(self, nelem):
        cdef unsigned int i
        # is self.value[0] a list
        if nelem == 1:
            if isinstance(self.value[0], list):             
                print("value         = ", self.value[0][0], end="")
            else:                
                print("value         =", self.value[0], end=""	)
        else:
            print("value	       = ", end="")
            
            if isinstance(self.value[0], list):
                for i in range(0, nelem):
                    print(self.value[0][i], "[%d]" % i, end=" ")
            else:
                for i in range(0, nelem):
                    print(self.value[i], "[%d]" % i, end=" ")

        print("")
        print("status        = %d" % self.status)
        print("alarmStatus   = %d" % self.alarmStatus)
        print("alarmSeverity = %d" % self.alarmSeverity)

        if self.noEnumStrings > 0:
            print("noEnumStrings = %d" % self.noEnumStrings)
            print("enumStrings   =",  end=" ")   # end for same line
            for i in range(0, self.noEnumStrings):
                print(self.enumStrings[i], "[%s]" % i, end=" ")
            print("")
        else:
            print("precision     = %d" % self.precision)
            print("units         = %s" % self.units)
            print("upperDisplayLimit = %f" % self.upperDisplayLimit)
            print("lowerDisplayLimit = %f" % self.lowerDisplayLimit)
            print("upperAlarmLimit   = %f" % self.upperAlarmLimit)
            print("lowerAlarmLimit   = %f" % self.lowerAlarmLimit)
            print("upperWarningLimit = %f" % self.upperWarningLimit)
            print("lowerWarningLimit = %f" % self.lowerWarningLimit)
            print("upperControlLimit = %f" % self.upperControlLimit)
            print("lowerControlLimit = %f" % self.lowerControlLimit)

      
##################################################################################

cdef class CafeException(Exception):
    cdef readonly str type
    cdef readonly str source
    cdef readonly unsigned int handle
    cdef readonly str name
    cdef readonly int error_code
    cdef readonly str error_text
    cdef readonly str error_info
   
    #Inheriting from base clas requires __init__; __cinit__ is ignored.
    def __init__(
    	self, _type="CafePVError",  _source=None,  _handle=0, 
       _pv_name=None, _error_code=-1, _error_text=None, _error_info=None):
        self.type = _type	
        self.source = _source
        self.handle = _handle
        self.name = _pv_name
        self.error_code = _error_code
        self.error_text = _error_text
        self.error_info = _error_info
    
    def show(self):
        print("------CafeException-----------------------------------------------------")
        if self.type is not None:
            print(self.type)
        print("Exception raised from CyCafe method:", self.source)
         
        if self.handle != 0:
            print("name =", self.name, "( handle =",
                  self.handle, ") reports the following error:")
            if (self.error_code != -1):
                print("error_code:", self.error_code, "error_text:", self.error_text)
            print("error_info:", self.error_info)
        elif self.type == 'CafePVError':
            print("name =", self.name, "( handle =",
                  self.handle, ") reports the following error:")
            if self.error_code != -1:  
                print("error_code:", self.error_code, "error_text:", self.error_text)
            print("error_info:", self.error_info)
        else:
            print("error_info:", self.error_info)
        print("------------------------------------------------------------------------")
        return

    def reveal(self):
        print("------CafeException-----------------------------------------------------")
        if self.type is not None:
            print(".type       =", self.type)
        print(".source     =", self.source)
        if self.handle != 0:
            print(".handle     =", self.handle)
        if self.name is not None:
            print(".name       =", self.name)
        if self.error_code != -1:
            print(".error_code =", self.error_code)
        if self.error_text is not None:
            print(".error_text =", self.error_text)
        print(".error_info =", self.error_info)
        print("------------------------------------------------------------------------")
        return


cdef class monitorpolicy:
    cdef readonly  chtype        dataType
    cdef readonly  chtype        dbrDataType
    cdef readonly  DBR_TYPE      cafeDbrType
    cdef readonly  unsigned int  nelem
    cdef readonly  unsigned int  mask
    # cdef readonly pCallback     handler
    cdef readonly  unsigned int  userArgs
    # cdef readonly  evid		          eventID
    cdef readonly  int           status
    cdef readonly  unsigned int  monid
    cdef readonly  bint maskHasDBE_PROPERTY
    cdef readonly  bint maskHasDBE_VALUE
    cdef readonly  bint maskHasDBE_LOG
    cdef readonly  bint maskHasDBE_ALARM

    def __cinit__(self):
        self.monid = 0
        # self.dataType=None
        # self.dbrDataType=None
        # self.cafeDbrType=None
        self.nelem = 1
        # self.mask=None
        # self.userArgs=None
        self.status = ICAFE_NORMAL
        self.maskHasDBE_PROPERTY = False
        self.maskHasDBE_VALUE = False
        self.maskHasDBE_LOG = False
        self.maskHasDBE_ALARM = False

    def show(self):
        print("monid      =", self.monid)
        print("datatype   =", self.dataType)
        print("dbrDataType=", self.dbrDataType)
        print("cafeDbrType=", self.cafeDbrType)
        print("nelem      =", self.nelem)
        print("mask       =", self.mask)
        print("userArgs   =", < long > self.userArgs)
        print("status     =", self.status)
        print("maskHasDBE_PROPERTY=", self.maskHasDBE_PROPERTY)
        print("maskHasDBE_VALUE   =", self.maskHasDBE_VALUE)
        print("maskHasDBE_LOG     =", self.maskHasDBE_LOG)
        print("maskHasDBE_ALARM   =", self.maskHasDBE_ALARM)
        return


##################################################################################
cdef class pvdata:
    cdef readonly unsigned int nelem
    cdef readonly int alarmStatus
    cdef readonly int alarmSeverity
    cdef readonly str alarmStatusAsString
    cdef readonly str alarmSeverityAsString
    cdef readonly list ts
    cdef readonly list tsDate
    cdef readonly str tsDateAsString
    cdef readonly str bsDateAsString
    cdef readonly list value
    cdef public int  status
    cdef public str  statusAsString
    cdef readonly unsigned long long pulseID
    cdef readonly int dataType
    cdef readonly str dataTypeAsString


    def __cinit__(self):
        self.nelem = 1
        self.alarmStatus = -1
        self.alarmSeverity = -1
        self.ts = []
        self.tsDate = []
        self.tsDateAsString = ''
        self.bsDateAsString = ''
        self.value = []
        self.status = 1
        self.statusAsString = 'ICAFE_NORMAL'
        self.pulseID = 0
        self.dataType = -1
        self.dataTypeAsString = ""
  
    def show(self):       
        self.showPrint(self.nelem)
        return

    def showMax(self, nelem):
        if nelem > self.nelem:
            # print ("----------------------"
            # print ("Note: Max. Nelem =", self.nelem
            # print ("----------------------"
            nelem = self.nelem

        self.showPrint(nelem)
        return

    cdef showPrint(self, nelem):
        cdef unsigned int i
        # is self.value[0] a list

        if nelem == 1:
            if isinstance(self.value[0], list):                
                print("value         = {} ({})".format(
                    self.value[0][0], self.dataTypeAsString), end="")
            else:                 
                print("value         = {} ({})".format(
                    self.value[0], self.dataTypeAsString), end="")
            print("")	    	    
        else:
            print("value	      = ", end="")

            if isinstance(self.value[0], list):
                for i in range(0, nelem):
                    print(self.value[0][i], "[%d]" % i, end=" ")
            else:
                for i in range(0, nelem):
                    print(self.value[i], "[%d]" % i, end=" ")             
            print("({0})".format(self.dataTypeAsString))

       
        print("status        = %d (%s)" % (self.status, self.statusAsString))
        if self.alarmStatus != -1:
            print("alarmStatus   = %d (%s)" %
                  (self.alarmStatus, self.alarmStatusAsString))
            print("alarmSeverity = %d (%s)" %
                  (self.alarmSeverity, self.alarmSeverityAsString))
        print("ts            =", self.ts[0], self.ts[1])
        print("tsDate        =", self.tsDate[0], self.tsDate[1],
              self.tsDate[2], self.tsDate[3],
              self.tsDate[4], self.tsDate[5], self.tsDate[6])
        if self.pulseID > 0:
            print("pulseID       =", self.pulseID)
        return

##################################################################################


##################################################################################
cdef class pvgroup:
    cdef public list pvdata
    cdef public unsigned int npv
    cdef public string name
    cdef readonly int  groupStatus
    cdef public unsigned int groupHandle
    cpdef public bint rule
    cpdef public bint hasAlarm
    cpdef public bint hasTS

    def __cinit__(self):
        self.groupStatus = ICAFE_NORMAL
        self.rule = True
        self.hasAlarm = True
        self.hasTS = True
        
        return

    def show(self):
        self.showPrint(npv=self.npv, grouplist=None)
        return

    def showWithPV(self, glist):
        if len(glist) != self.npv:
            print("ERROR: GROUP MEMBER MISMATCH!!")
            print(glist, "has", len(glist),
                  "members while group has", self.npv, "!!")
            return
        self.showPrint(self.npv, grouplist=glist)
        return

    def showMax(self, _npv):
        if _npv > self.npv:
            # print ("----------------------"
            # print ("Note: Max. Npv =", self.npv
            # print ("----------------------"
            _npv = self.npv
        self.showPrint(npv=_npv, grouplist=None)
        return

    cdef showPrint(self, npv, grouplist):
        print("GROUP NAME   = ", self.name)
        print("GROUP HANDLE = ", self.groupHandle)
        print("GROUP STATUS = ", self.groupStatus)
        print("No. Members  = ", self.npv)  
        cdef unsigned int i, j
    
        for i in range(0, npv):
            print("Member: ", i, "---------------------------------")
            if grouplist:
                print("PV            =", grouplist[i])

            print("value         = ", end="")
            for j in range(0, len(self.pvdata[i].value)):
                print(self.pvdata[i].value[j], "[%d]" % j, end=" ")

            print("")
            print("status        = %d (%s)" %
                  (self.pvdata[i].status, self.pvdata[i].statusAsString))
            print("alarmStatus   = %d (%s)" % (
                self.pvdata[i].alarmStatus, self.pvdata[i].alarmStatusAsString))
            print("alarmSeverity = %d (%s)" % (
                self.pvdata[i].alarmSeverity, self.pvdata[i].alarmSeverityAsString))
            print("ts            =",
                  self.pvdata[i].ts[0], self.pvdata[i].ts[1])
            print("tsDate        =", self.pvdata[i].tsDate[0], self.pvdata[i].tsDate[1],
                  self.pvdata[i].tsDate[2], self.pvdata[i].tsDate[3],
                  self.pvdata[i].tsDate[4], self.pvdata[i].tsDate[5], self.pvdata[i].tsDate[6])
            print("--------------------------------------------")
        return

   
##################################################################################


##################################################################################
cdef getMatchedDataType(dt, dtn):
    # dt: input from user
    # dtn: native type
    # dtcheck: matching data type for pvd convert method

    cdef unsigned int dtcheck = dtn

    if dt in ['uchar', 'uint8']:
        dtcheck = CAFE_CHAR
    elif dt in ['np.uint8']:
        dtcheck = CAFE_CHAR
    elif dt in ['ushort', 'uint16']:
        dtcheck = CAFE_USHORT
    elif dt in ['np.ushort', 'np.uint16']:
        dtcheck = CAFE_USHORT
    elif dt in ['short', 'int16', 'int8', 'bool', 'byte']:
        dtcheck = CAFE_SHORT
    elif dt in ['np.short', 'np.int16', 'np.int8', 'np.bool_', 'np.byte']:
        dtcheck = CAFE_SHORT
    elif dt in ['int', 'int_', 'uint', 'ushort', 'int32', 'uint32']:
        dtcheck = CAFE_LONG
    elif dt in ['np.intc', 'np.int_', 'np.long', 'np.ushort', 'np.int32', 'np.uint32']:
        dtcheck = CAFE_LONG
    elif dt in ['double', 'float', 'float_', 'float64', 'int64', 'uint64']:
        dtcheck = CAFE_DOUBLE
    elif dt in ['np.float_', 'np.float64', 'np.long', 'np.ulong', 'np.int64', 'np.uint64']:
        dtcheck = CAFE_DOUBLE
    elif dt in ['float16', 'float32']:
        dtcheck = CAFE_FLOAT
    elif dt in ['np.float16', 'np.float32']:
        dtcheck = CAFE_FLOAT
    elif dt in ['string', 'str', 'unicode', 'string_', 'str_', 'unicode_']:
        dtcheck = CAFE_STRING
    elif dt in ['np.string', 'np.str', 'np.unicode', 'np.string_', 'np.str_', 'np.unicode_']:
        dtcheck = CAFE_STRING
    elif dt in ['native', '']:
        dtcheck = dtn  # need a line here
    else:
        print("Valid input parameters for data type are variations of: 'int', 'float', 'str', or 'native'")
        print("Data to be presented in native data type")

    return dtcheck
##################################################################################


#################################################################################

# cdef pvdata  p1 = pvdata()
# cdef pvctrl  c1 = pvctrl()


#################################################################################

cdef pvdata PVDataHolderToStruct(PVDataHolder pvd, dt=None):

    cdef CAFEDataTypeCode cdt
    #global p1
    cdef pvdata  p1 = pvdata()

    cdef unsigned int dtn = pvd.getDataType()
    cdef unsigned int dtcheck = dtn
    cdef localList = []

    if dt:
        dtcheck = getMatchedDataType(dt, dtn)

    if dtcheck == CAFE_STRING:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsString(i))
    elif dtcheck == CAFE_SHORT:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsLong(i))  # getAsShort(i)
    elif dtcheck == CAFE_FLOAT:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsDouble(i))  # getAsFloat(i)
    elif dtcheck == CAFE_ENUM:
        # if enum, string taken as native
        # if self._c_cafe.isEnum(handle)==1:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsString(i))
        # else:
        #	for i in range(0, pvd.getNelem()):
        #		localList.append(pvd.getAsLong(i)) #getAsUShort(i)
    elif dtcheck == CAFE_CHAR:
        for i in range(0, pvd.getNelem()):
            # <unsigned char> pvd.getAsChar(i)
            localList.append(pvd.getAsLong(i))
    elif dtcheck == CAFE_LONG:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsLong(i))
    elif dtcheck == CAFE_DOUBLE:
        for i in range(0, pvd.getNelem()):
            localList.append(pvd.getAsDouble(i))
    else:
        localList.append(0)
        # print ("This line in PyCafe def getDataHolderToStruct should never appear!"
        # print ("No Data! Error. Is channel connected?"

    p1.value = localList
    p1.status = pvd.getStatus()
    p1.statusAsString = pvd.getStatusAsString()
    p1.nelem = pvd.getNelem()
    p1.alarmStatus = pvd.getAlarmStatus()
    p1.alarmSeverity = pvd.getAlarmSeverity()

    p1.alarmStatusAsString = pvd.getAlarmStatusAsString()
    p1.alarmSeverityAsString = pvd.getAlarmSeverityAsString()

    p1.pulseID = pvd.getPulseID()

    p1.dataType = pvd.getDataType()
    p1.dataTypeAsString = cdt.message(pvd.getDataType())

    p1.tsDateAsString = pvd.getEpicsTimeStampAsString()
    p1.bsDateAsString = pvd.getBSTimeStampAsString()

    pvd._etsNorm = pvd.getEpicsTimeStampAsUInt32()

    cpdef ll = []
    ll.append((pvd._etsNorm).secPastEpoch)
    ll.append((pvd._etsNorm).nsec)
    p1.ts = ll

    pvd._etsDate = pvd.getEpicsTimeStampAsDate()

    cpdef ld = []
    ld.append((pvd._etsDate).year)
    ld.append((pvd._etsDate).mon)
    ld.append((pvd._etsDate).day)
    ld.append((pvd._etsDate).hour)
    ld.append((pvd._etsDate).min)
    ld.append((pvd._etsDate).sec)
    ld.append((pvd._etsDate).nsec)
    p1.tsDate = ld

    return p1
##################################################################################


#################################################################################

cdef pvctrl	PVCtrlHolderToStruct(PVCtrlHolder pvc, dt=None):

    #global c1
    cdef pvctrl c1 = pvctrl()

   

    cdef unsigned int dtn = pvc.getDataType()

    cdef unsigned int dtcheck = dtn

    cdef localList = []

    if dt:
        dtcheck = getMatchedDataType(dt, dtn)

    if dtcheck == CAFE_STRING:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsString(i))
    elif dtcheck == CAFE_SHORT:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsLong(i))  # getAsShort(i)
    elif dtcheck == CAFE_FLOAT:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsDouble(i))  # getAsFloat(i)
    elif dtcheck == CAFE_ENUM:
        # if enum, string taken as native
        # if self._c_cafe.isEnum(handle)==1:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsString(i))
        # else:
        #	for i in range(0, pvd.getNelem()):
        #		localList.append(pvd.getAsLong(i)) #getAsUShort(i)
    elif dtcheck == CAFE_CHAR:
        for i in range(0, pvc.getNelem()):
            # <unsigned char> pvd.getAsChar(i)
            localList.append(pvc.getAsLong(i))
    elif dtcheck == CAFE_LONG:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsLong(i))
    elif dtcheck == CAFE_DOUBLE:
        for i in range(0, pvc.getNelem()):
            localList.append(pvc.getAsDouble(i))
    else:
        localList.append(0)
        # print ("This line in PyCafe def getDataHolderToStruct should never appear!"
        # print ("No Data! Error. Is channel connected?"
    c1.value = localList
    c1.status = pvc.getStatus()
    c1.nelem = pvc.getNelem()
    c1.alarmStatus = pvc.getAlarmStatus()
    c1.alarmSeverity = pvc.getAlarmSeverity()

    c1.precision = pvc.getPrecision()
    c1.noEnumStrings = pvc.getNoEnumStrings()

    enumList = []
    for i in range(0, pvc.getNoEnumStrings()):
        enumList.append(pvc.getEnumStrings()[i])

    c1.enumStrings = enumList
    
    try:
        c1.units = pvc.getUnits()
    except UnicodeDecodeError:
        c1.units = str("")

    '''
    result = isinstance(pvc.getUnits(), (bytes,))

    if result:	
        c1.units = pvc.getUnits().decode('utf-8')
        print(str(c1.units + " is bytes"))
    else:
        c1.units = pvc.getUnits()  #AsString()
        print(str(c1.units + " has type " + str(type(pvc.getUnits()))))
    print(c1.units)
    '''

    c1.upperDisplayLimit = pvc.getUpperDispLimit_AsDouble()
    c1.lowerDisplayLimit = pvc.getLowerDispLimit_AsDouble()
    c1.upperAlarmLimit = pvc.getUpperAlarmLimit_AsDouble()
    c1.upperWarningLimit = pvc.getUpperWarningLimit_AsDouble()
    c1.lowerWarningLimit = pvc.getLowerWarningLimit_AsDouble()
    c1.lowerAlarmLimit = pvc.getLowerAlarmLimit_AsDouble()
    c1.upperControlLimit = pvc.getUpperCtrlLimit_AsDouble()
    c1.lowerControlLimit = pvc.getLowerCtrlLimit_AsDouble()

    return c1
##################################################################################


#################################################################################

cdef channelInfo channelRegaliaToStruct(ChannelRegalia cr):

    cdef channelInfo ci = channelInfo()
    cdef CAFEDataTypeCode cdt

    ci.channelID =  ( < long > cr.getChannelID())
    ci.connectFlag = cr.getConnectFlag()
    ci.hostName = cr.getHostNameAsString()
    ci.dataType = cr.getDataType()
    ci.dataTypeAsString = cdt.message(cr.getDataType())
    ci.className = cr.getClassNameAsString()
    ci.accessRead = cr.getAccessRead()
    ci.accessWrite = cr.getAccessWrite()
    ci.nelem = cr.getNelem()
    ci.connectionState = cr.getConnectionState()
    ci.cafeConnectionState = cr.getCafeConnectionState()
    ci.connectionStateAsString = cr.getConnectionStateAsString()
    ci.cafeConnectionStateAsString = cr.getCafeConnectionStateAsString()
    return ci
#################################################################################


cdef class bschannel:
    cdef readonly string name
    cdef readonly string type
    cdef readonly int    modulo
    cdef readonly int    offset
    cdef readonly unsigned int nelem
    cdef readonly bint   BSEnabled

    def __cinit__(self):
        self.name = (< bytes > ("")).encode('UTF-8')
        self.type = (< bytes > ("")).encode('UTF-8')
        self.modulo = 1
        self.offset = 0
        self.nelem = 1
        self.BSEnabled = False

cdef class BSData:
    cdef readonly string htype
    cdef readonly unsigned long long pulse_id
    cdef readonly list BSChannel
    cdef readonly list pv
    cdef readonly dict pvIdx
    cdef readonly list handle
    cdef readonly list global_timestamp
    cdef readonly list gtsDate
    cdef readonly bint isBS
    cdef readonly unsigned int nPV
    cdef readonly unsigned int nBSEnabled
    cdef readonly unsigned short nhwm
    cdef readonly int timeoutMS
    cdef readonly unsigned int nChannels
    cdef readonly unsigned int nNullData
    cdef readonly float pGoodData
    cdef readonly int status
    cdef readonly statusAsString
    cdef public bint daqStop

    cdef readonly list hasNewData
    cdef readonly list pvdata

    def __cinit__(self):
        self.pulse_id = 0
        self.htype = (< bytes > ("")).encode('UTF-8')
        self.BSChannel = []
        self.pv = []
        self.handle = []
        self.global_timestamp = []
        self.gtsDate = []
        self.isBS = False
        self.nPV = 0
        self.nBSEnabled = 0
        self.nhwm = BSREAD_ZEROMQ_HIGH_WATER_MARK
        self.timeoutMS = BSREAD_ZEROMQ_TIMEOUT_MS
        self.nChannels = 0  # Diagnostics
        self.nNullData = 0
        self.pGoodData = 0
        self.status = ICAFE_NORMAL
        self.statusAsString = 'ICAFE_NORMAL'
        self.daqStop = False
        self.hasNewData = []

    def __copy__(self):
        return self

    def __deepcopy__(self, memo):
        cdef BSData res = BSData()
        cpdef p1List = []
        cpdef bscList = []

        res.htype = self.htype
        res.pulse_id = self.pulse_id
        res.pv = deepcopy(self.pv)
        res.handle = deepcopy(self.handle)
        res.isBS = self.isBS

        #res.pvIdx = dict([(self.pv[i], i) for i in range(0,len(self.pv))] )

        res.pvIdx = deepcopy(self.pvIdx)

        # print(res.pvIdx.items())

        res.global_timestamp = deepcopy(self.global_timestamp)
        res.gtsDate = deepcopy(self.gtsDate)

        res.hasNewData = deepcopy(self.hasNewData)

        res.nPV = self.nPV
        res.timeoutMS = self.timeoutMS
        res.nhwm = self.nhwm
        res.status = self.status
        res.statusAsString = self.statusAsString

        res.nChannels = self.nChannels
        res.nNullData = self.nNullData
        res.pGoodData = self.pGoodData

        res.daqStop = self.daqStop

        lenPV = len(self.pvdata)

        # Should never happen
        if lenPV != len(self.BSChannel):
            print("Anomoly in BSData deepcopy. Unequal lengths. Take minimum value")
            if lenPV > len(self.BSChannel):
                lenPV = len(self.BSChannel)

        for i in range(0, lenPV):

            # for i in range(0,len(self.BSChannel)):
            bsc = bschannel()
            bsc.name = self.BSChannel[i].name
            bsc.type = self.BSChannel[i].type
            bsc.modulo = self.BSChannel[i].modulo
            bsc.offset = self.BSChannel[i].offset
            bsc.nelem = self.BSChannel[i].nelem
            bsc.BSEnabled = self.BSChannel[i].BSEnabled

            bscList.append(bsc)

        # res.BSChannel=bscList

        # for i in range(0,len(self.pvdata)):

            p1 = pvdata()
            # value=[]
            # value.append(self.pvdata[i].value[0])
            # p1.value.append(value)
            p1.value = deepcopy(self.pvdata[i].value)
            p1.status = self.pvdata[i].status
            p1.statusAsString = self.pvdata[i].statusAsString
            p1.nelem = self.pvdata[i].nelem
            p1.alarmStatus = self.pvdata[i].alarmStatus
            p1.alarmSeverity = self.pvdata[i].alarmSeverity
            p1.alarmStatusAsString = self.pvdata[i].alarmStatusAsString
            p1.alarmSeverityAsString = self.pvdata[i].alarmSeverityAsString
            p1.pulseID = self.pvdata[i].pulseID
            p1.dataType = self.pvdata[i].dataType
            p1.dataTypeAsString = self.pvdata[i].dataTypeAsString
            p1.tsDateAsString = self.pvdata[i].tsDateAsString
            p1.bsDateAsString = self.pvdata[i].bsDateAsString
            p1.ts = deepcopy(self.pvdata[i].ts)
            p1.tsDate = deepcopy(self.pvdata[i].tsDate)

            p1List.append(p1)

        res.pvdata = p1List
        res.BSChannel = bscList

        # for i in range(0,len(res.pvdata)):
        # res.pvdata[i].show()

        return res

    def compare(self,  list _pvdata):
        self.hasNewData = [0]*len(self.pv)
        cdef unsigned short newDataFlag = 0

        for i in range(0, len(self.pv)):
            if self.pvdata[i].ts[1] != _pvdata[i].ts[1]:
                self.hasNewData[i] = 1
                newDataFlag = 1
            elif self.pvdata[i].ts[0] != _pvdata[i].ts[0]:
                self.hasNewData[i] = 1
                newDataFlag = 1
            # Catch disconnect events(!!) and set newDataFlag only
            elif self.pvdata[i].status != _pvdata[i].status:
                newDataFlag = 1
        return newDataFlag

    def getIdxFromPVName(self, str _name):
        for i in range(0, len(self.pv)):
            if _name == self.pv[i]:
                return i
        return -1

    def getDaqStop(self):
        return self.daqStop

    def setDaqStop(self, bint ds):
        self.daqStop = ds

    def getVal(self, iElem=None):

        cdef int idx = -1

        if isinstance(iElem, (str)):
            # iElem=self.getIdxFromPVName(iElem)
            iElem = self.pvIdx.get(iElem)
            if iElem is None:
                return None
            idx = iElem
        elif isinstance(iElem, (int, long)):
            if iElem < 0:
                print("Invalid input:", iElem, "Idx must be non-negative!")
                return None
            idx = iElem

        if iElem is not None:
            if len(self.pvdata[idx].value) == 1:
                return self.pvdata[idx].value[0]
            else:
                return self.pvdata[idx].value
        vall = []
        for i in range(0, self.nPV):
            if len(self.pvdata[i].value) == 1:
                vall.append(self.pvdata[i].value[0])
            else:
                vall.append(self.pvdata[i].value)
        return vall

    def getPV(self, iElem=None):

        cdef int idx = -1

        if isinstance(iElem, (str)):
            iElem = self.pvIdx.get(iElem)
            if iElem is None:
                return None
            idx = iElem
        elif isinstance(iElem, (int, long)):
            if iElem < 0:
                print("Invalid input:", iElem, " Idx must be non-negative!")
                return None
            idx = iElem

        if iElem is not None:
            return self.pvdata[idx]
        vall = []
        for i in range(0, self.nPV):
            vall.append(self.pvdata[i])
        return vall

    def show(self, str BSCA=None, int wf=1):

        for i in range(0, self.nPV):

            if self.BSChannel[i].BSEnabled and self.isBS:
                if BSCA in (None, 'BS'):
                    print("---------------------------------")
                    print("pv= %s [%d] BS" % (self.pv[i], i))
                    if wf > 1:
                        self.pvdata[i].showMax(wf)
                    else:
                        self.pvdata[i].show()
            else:
                if BSCA in (None, 'CA'):
                    print("---------------------------------")
                    print("pv= %s [%d] CA" % (self.pv[i], i))
                    if wf > 1:
                        self.pvdata[i].showMax(wf)
                    else:
                        self.pvdata[i].show()
        print("---------------------------------")
	
        print("overallStatus    = %d (%s)" %
              (self.status, self.statusAsString))
        if self.pulse_id > 0:      
            print("pulse_id         = %d" % self.pulse_id)
            print("global_timestamp = %d sec. %d nsec" %
              (self.global_timestamp[0], self.global_timestamp[1]))
            print("gtsDate          = %d %d %d %d %d %d %d" % (self.gtsDate[0], \
	    self.gtsDate[1], self.gtsDate[2], self.gtsDate[3], self.gtsDate[4], \
	    self.gtsDate[5], self.gtsDate[6]))
        print("nPV              = %d" % self.nPV)
        if self.isBS:
            print("of which BS      = %d  (Percentage with data = %f)" %
                  (self.nChannels, float(self.pGoodData)))
            print("of which CA      = %d" % (self.nPV - self.nChannels))
        else:
            print("of which CA      = %d (i.e., all)" % (self.nPV)	)
        print("---------------------------------")
        return

    def showAll(self, int wf=1):

        for i in range(0, self.nPV):
            print("---------------------------------")
            if self.BSChannel[i].BSEnabled and self.isBS:
                print("pv= %s [%d] BS" % (self.pv[i], i))
            else:
                print("pv= %s [%d] CA" % (self.pv[i], i))
            if wf > 1:
                self.pvdata[i].showMax(wf)
            else:
                self.pvdata[i].show()
        print("---------------------------------")
        print("overallStatus    = %d (%s)" %
              (self.status, self.statusAsString))
        print("pulse_id         = %d" % self.pulse_id)
        print("global_timestamp = %d sec. %d nsec" %
              (self.global_timestamp[0], self.global_timestamp[1]))
        print("gtsDate          = %d %d %d %d %d %d %d" % (self.gtsDate[0], self.gtsDate[1], self.gtsDate[2], self.gtsDate[3], self.gtsDate[4], self.gtsDate[5],
                                                           self.gtsDate[6]))
        print("nPV              = %d" % self.nPV)
        if self.isBS:
            print("of which BS      = %d  (Percentage with data = %f)" %
                  (self.nChannels, float(self.pGoodData)))
            print("of which CA      = %d" % (self.nPV-self.nChannels))
        else:
            print("of which CA      = %d (i.e., all)" % (self.nPV)	)
        print("pvIdx            = ", self.pvIdx)
        print("pv               = ", self.pv)
        print("handle           = ", self.handle)
        print("isBS             = %d" % self.isBS)
        #print ("nBSEnabled       = %d" % self.nBSEnabled)
        print("nhwm             = %d" % self.nhwm)
        print("timeoutMS        = %d" % self.timeoutMS)
        print("nChannels        = %d" % self.nChannels)
        print("nNullData        = %d" % self.nNullData)
        print("pGoodData        = %f" % float(self.pGoodData))
        print("---------------------------------")
        return


cdef class SFDBPMData:
    cdef readonly   list  x
    cdef readonly   list  y
    cdef readonly   list  q
    cdef readonly   list  Energy
    cdef readonly   list  xStatus
    cdef readonly   list  yStatus
    cdef readonly   list  qStatus
    cdef readonly   list  EnergyStatus

    # cdef readonly   list    offs_x
    # cdef readonly   list    offs_y

    cdef readonly   bint      isAllXOK
    cdef readonly   bint      isAllYOK
    cdef readonly   bint      isAllQOK
    cdef readonly   bint      isAllOK
    cdef readonly   list      device
    cdef readonly   int       status

    def __cinit__(self):
        self.isAllOK = False
        self.isAllXOK = False
        self.isAllYOK = False
        self.isAllQOK = False
        self.x = []
        self.y = []
        self.q = []
        self.Energy = []
        self.xStatus = []
        self.yStatus = []
        self.qStatus = []
        self.EnergyStatus = []
        self.device = []

        # self.offs_x=[]
        # self.offs_y=[]


#################################################################################

cdef SFDBPMData dbpmHolderToStruct(DBPMKeeper d):
    cdef SFDBPMData dbpm = SFDBPMData()

    cdef unsigned int i

    # double [::1] mvDouble
    #mvDouble  = np.empty(d.getX().size(), dtype=np.float64)
    # for ij in range(0, d.getX().size()):
    #					mvDouble[ij]=<double>d.getX()[i]
    # dbpm.x=np.array(mvDouble)

    for i in range(0, d.getX().size()):

        dbpm.x.append(d.getX()[i].getValue())
        dbpm.y.append(d.getY()[i].getValue())
        dbpm.q.append(d.getQ()[i].getValue())
        dbpm.Energy.append(d.getEnergy()[i].getValue())
        dbpm.xStatus.append(d.getX()[i].getStatus())
        dbpm.yStatus.append(d.getY()[i].getStatus())
        dbpm.qStatus.append(d.getQ()[i].getStatus())
        dbpm.EnergyStatus.append(d.getEnergy()[i].getStatus())

    dbpm.isAllXOK = d.getIsAllXOK()
    dbpm.isAllYOK = d.getIsAllYOK()
    dbpm.isAllQOK = d.getIsAllQOK()
    dbpm.isAllOK = d.getIsAllOK()
    dbpm.status = d.getStatus()
    return dbpm

#################################################################################


#################################################################################

cdef  BSDataHolderToStruct(BSDataHolder b):

    cdef BSData bsd = BSData()
    cdef vector[PVDataHolder] pvd
    cdef unsigned int i
    cdef etsNorm _etsNorm
    cdef etsDate _etsDate
    cdef HandleHelper hh
    cdef CAFEStatusCode csc
    localListToStruct = []
    localBSCToStruct = []

    bsd.pulse_id = b.getPulse_id()
    bsd.pv = b.getPV()
    bsd.handle = b.getHandles()
    bsd.isBS = b.getIsBS()

    bsd.pvIdx = dict([(bsd.pv[i], i) for i in range(0, len(bsd.pv))])

    # print(bsd.pvIdx.items())

    _etsNorm = b.getGlobal_timestamp()

    #print ("pulse_id " , b.getPulse_id())
    #print ("gto ", b.getGlobal_timestamp())

    cpdef ll = []
    ll.append(_etsNorm.secPastEpoch)
    ll.append(_etsNorm.nsec)
    bsd.global_timestamp = ll

    _etsDate = hh.unixTimeToDate(_etsNorm)
    cpdef ld = []
    ld.append(_etsDate.year)
    ld.append(_etsDate.mon)
    ld.append(_etsDate.day)
    ld.append(_etsDate.hour)
    ld.append(_etsDate.min)
    ld.append(_etsDate.sec)
    ld.append(_etsDate.nsec)

    bsd.gtsDate = ld

    #bsd.isBS = b.getIsBS()
    bsd.nPV = b.getNPV()
    bsd.timeoutMS = b.getTimeout()
    bsd.nhwm = b.getHWM()

    bsd.status = b.getStatus()
    bsd.statusAsString = csc.msgIDAsString(bsd.status)

    pvd = b.getPVDataV()
    cpdef lt = []
    cpdef bscList = []

    # Would be quicker to have p1 = pvdata() outside loop - check this
    for i in range(0, pvd.size()):
        p1 = pvdata()
        p1 = PVDataHolderToStruct(pvd[i], dt='native')

        #print ("a: ", p1.tsDate)
        lt = []
        bsc = bschannel()

        if b.getBSChannel(i).isBSEnabled() and bsd.isBS:
            bsc.BSEnabled = True
            _etsNorm = pvd[i].getEpicsTimeStampAsUInt32()
            _etsDate = pvd[i].getUnixTimeAsDate(_etsNorm)
        else:
            _etsDate = pvd[i].getEpicsTimeStampAsDate()
        lt.append(_etsDate.year)
        lt.append(_etsDate.mon)
        lt.append(_etsDate.day)
        lt.append(_etsDate.hour)
        lt.append(_etsDate.min)
        lt.append(_etsDate.sec)
        lt.append(_etsDate.nsec)
        p1.tsDate = lt
        #print ("b: ", p1.tsDate)
        localListToStruct.append(p1)
        localBSCToStruct.append(bsc)

    bsd.pvdata = localListToStruct
    bsd.BSChannel = localBSCToStruct

    bsd.nChannels = b.getNChannels()
    bsd.nNullData = b.getNNullData()
    bsd.pGoodData = b.getPGoodData()

    bsd.daqStop = False
    return bsd

#################################################################################
