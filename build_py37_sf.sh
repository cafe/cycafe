module unload gcc
module load gcc/7.3.0
rm -f PyCafe.cpp
rm -f PyCafe.h
rm -f PyCafe.pxd
ln -s PyCafe_sf.pxd PyCafe.pxd
rm -f PyCafe.pyx
ln -s PyCafe_sf.pyx PyCafe.pyx
_EPICS_HOST_ARCH=${RHREL}-x86_64 
source /opt/gfa/python 3.7
python setup_py37_sf.py build_ext -b ./python3.7-sf/lib/${_EPICS_HOST_ARCH} 
cp examples.py ./python3.7-sf/lib/${_EPICS_HOST_ARCH}
