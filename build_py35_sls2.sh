module unload gcc
module load gcc/7.3.0
#rm -f PyCafeDefs.pxi
#ln -s PyCafeDefs_py35.pxi PyCafeDefs.pxi
#rm -f PyCafe.cpp
#rm -f PyCafe.h
#rm -f PyCafe.pxd
#ln -s PyCafe_sls.pxd PyCafe.pxd
#rm -f PyCafe.pyx
#ln -s PyCafe_sls_py35.pyx PyCafe.pyx
_EPICS_HOST_ARCH=${RHREL}-x86_64 
source /opt/gfa/python 3.5
python setup_py35_sls2.py build_ext -b ./python3.5-sls2/lib/${_EPICS_HOST_ARCH} 
cp examples.py ./python3.5-sls2/lib/${_EPICS_HOST_ARCH}

