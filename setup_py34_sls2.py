import os
import sys
from distutils.core import setup
from distutils.extension import Extension
from Cython.Compiler.Main import default_options
default_options['emit_linenums'] = True
from Cython.Build import cythonize
from numpy import get_include

_GCC_VERSION='7.3.0'
_CAFE_VERSION='1.15.0-py34-gcc-' + _GCC_VERSION
_EPICS_VERSION='7.0.6'


setup(
    ext_modules = cythonize([Extension('PyCafe',['PyCafe.pyx'],
        language="c++",
        include_dirs=[  '/opt/psi/Programming/psi-python34/2.1.0/include/python3.4m',
                        os.environ['EPICS'] + '/base-' + _EPICS_VERSION + '/include',
                        os.environ['EPICS'] + '/base-' + _EPICS_VERSION + '/include/os/Linux',
                        os.environ['EPICS'] + '/base-' + _EPICS_VERSION + '/include/compiler/gcc',
                        '/opt/gfa/cafe/boost/boost_1_61_0/include',
                        '/opt/gfa/cafe/boost/boost_1_61_0/include/boost',
                        '/opt/gfa/cafe/cpp/cafe-' + _CAFE_VERSION +
                        '/include', 
                        '.', get_include()],
        library_dirs=[  os.environ['EPICS'] + '/base-' + _EPICS_VERSION + '/lib/' + 
                        os.environ['EPICS_HOST_ARCH'],
                        '/opt/gfa/cafe/cpp/cafe-' + _CAFE_VERSION +
                        '/lib/' + os.environ['EPICS_HOST_ARCH'],
                        os.environ['PSI_PREFIX'] + '/Programming/gcc/' + _GCC_VERSION + '/lib64',
                        os.environ['PSI_PREFIX'] + '/Programming/gcc/' + _GCC_VERSION + '/lib',
                        '/opt/psi/Programming/psi-python34/2.1.0/lib'
                                          ],
        runtime_library_dirs=[ 
                    os.environ['EPICS']  + '/base-' + _EPICS_VERSION + '/lib/' + 
                    os.environ['EPICS_HOST_ARCH'],
                    '/opt/gfa/cafe/cpp/cafe-' + _CAFE_VERSION +
                    '/lib/' + os.environ['EPICS_HOST_ARCH'],
                    os.environ['PSI_PREFIX'] + '/Programming/gcc/' + _GCC_VERSION + '/lib64',
                    os.environ['PSI_PREFIX'] + '/Programming/gcc/' + _GCC_VERSION + '/lib',
                    '/opt/psi/Programming/psi-python34/2.1.0/lib'
                                          ],
                                       libraries=['ca','Com','dl','cafe', 'xml2'])
                        ], annotate=True,
                           compile_time_env={'PY_VERSION_HEX': sys.hexversion, 'PY_EXT_C': True, 'BS_CAFE': False, 
                                             'CAFE_VERSION': _CAFE_VERSION, 'EPICS_VERSION': _EPICS_VERSION  }, 
                           compiler_directives={'embedsignature': False, 'language_level': 3, 
                                'c_string_type': 'str', 'c_string_encoding' : 'ascii', 
                                'py2_import': False, 'warn.unreachable': False, 'remove_unreachable': False}
                           )
    )
