module unload gcc
module load gcc/7.3.0
rm -f PyCafeDefs.pxi
ln -s PyCafeDefs_py37.pxi PyCafeDefs.pxi
rm -f PyCafe.cpp
rm -f PyCafe.h
rm -f PyCafe.pxd
ln -s PyCafe_sls.pxd PyCafe.pxd
rm -f PyCafe.pyx
ln -s PyCafe_sls.pyx PyCafe.pyx
_EPICS_HOST_ARCH=${RHREL}-x86_64 #os.environ['EPICS_HOST_ARCH']
source /opt/gfa/python 3.7
python setup_py37_sls2.py build_ext -b ./python3.7-sls2/lib/${_EPICS_HOST_ARCH}
cp examples.py ./python3.7-sls2/lib/${_EPICS_HOST_ARCH}
