module unload gcc
module load gcc/7.3.0
rm -f PyCafe.cpp
rm -f PyCafe.h
rm -f PyCafe.pxd
ln -s PyCafe_sls.pxd PyCafe.pxd

source /opt/gfa/python 3.7
python setup_py37.py build_ext -b ./python3.7/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.7/lib/${EPICS_HOST_ARCH}
