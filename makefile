#
# Jan Chrin
# 
#
##### CHANGE AS APPROPRIATE #################
#

_EPICS_HOST_ARCH=${RHREL}-x86_64
CAFE_CYCAFE_BASE=/opt/gfa/cafe/python/pycafe
INSTALL_PATH_PY= $(CAFE_CYCAFE_BASE)/cafe-1.19.3-gcc-7.3.0/lib/${_EPICS_HOST_ARCH}
INSTALL_PATH_SF= $(CAFE_CYCAFE_BASE)/cafe-1.19.3-sf-gcc-7.3.0/lib/${_EPICS_HOST_ARCH}
INSTALL_PATH_SLS2= $(CAFE_CYCAFE_BASE)/cafe-1.19.3-gcc-7.3.0/lib/${_EPICS_HOST_ARCH}
INSTALL_PATH_SLS2_PY38= $(CAFE_CYCAFE_BASE)/cafe-1.19.3-gcc-7.5.0/lib/${_EPICS_HOST_ARCH}
INSTALL_PATH_SLS2_PY310= $(CAFE_CYCAFE_BASE)/cafe-1.19.3-gcc-7.5.0/lib/${_EPICS_HOST_ARCH}
#############################################

.DEFAULT_GOAL := help
.PHONY: clean

help:
	@echo "Options for make: install_py37 install_py37_sf install_py37_sls2 install_py35 install_py35_sf"

install_py37: python3.7/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-37m-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_PY)
	cp python3.7/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-37m-x86_64-linux-gnu.so  $(INSTALL_PATH_PY)	
	cp python3.7/lib/${_EPICS_HOST_ARCH}/examples.py $(INSTALL_PATH_PY)

install_py35: python3.5/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-35m-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_PY)
	cp python3.5/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-35m-x86_64-linux-gnu.so  $(INSTALL_PATH_PY)	

install_py37_sf: python3.7-sf/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-37m-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_SF)
	cp python3.7-sf/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-37m-x86_64-linux-gnu.so  $(INSTALL_PATH_SF)
	cp python3.7-sf/lib/${_EPICS_HOST_ARCH}/examples.py $(INSTALL_PATH_SF)

install_py35_sf: python3.5-sf/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-35m-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_SF)
	cp python3.5-sf/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-35m-x86_64-linux-gnu.so  $(INSTALL_PATH_SF)


install_py310_sls2: python3.10-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-310-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_SLS2_PY310)
	cp python3.10-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-310-x86_64-linux-gnu.so  $(INSTALL_PATH_SLS2_PY310)	
	cp python3.10-sls2/lib/${_EPICS_HOST_ARCH}/examples.py $(INSTALL_PATH_SLS2_PY310)

install_py38_sls2: python3.8-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-38-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_SLS2_PY38)
	cp python3.8-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-38-x86_64-linux-gnu.so  $(INSTALL_PATH_SLS2_PY38)	
	cp python3.8-sls2/lib/${_EPICS_HOST_ARCH}/examples.py $(INSTALL_PATH_SLS2_PY38)

install_py37_sls2: python3.7-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-37m-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_SLS2)
	cp python3.7-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-37m-x86_64-linux-gnu.so  $(INSTALL_PATH_SLS2)	
	cp python3.7-sls2/lib/${_EPICS_HOST_ARCH}/examples.py $(INSTALL_PATH_SLS2)

install_py35_sls2: python3.5-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-35m-x86_64-linux-gnu.so
	mkdir -p $(INSTALL_PATH_SLS2)
	cp python3.5-sls2/lib/${_EPICS_HOST_ARCH}/PyCafe.cpython-35m-x86_64-linux-gnu.so  $(INSTALL_PATH_SLS2)	
	cp python3.5-sls2/lib/${_EPICS_HOST_ARCH}/examples.py $(INSTALL_PATH_SLS2)
clean:
	rm -f *.o

