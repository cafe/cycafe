
cdef object py_cb_handle = None
cdef object py_cb_handle_open = None

cdef object cbobjt = None

# ***********public or api***************
# For when with pvdata


cdef public void cy_monitor_handler_wrapper(void * callback) with gil:
    cbobjt=<object> (<void *> callback)   
    cbobjt()
    with nogil:
        return
   

cdef public void cy_data_event_handler_wrapper(void * callback,  
                                               unsigned int handle, 
                                               string pvname, 
                                               PVDataHolder pvd) with gil:
    
    global py_cb   
    #print(" cy_data_event_handler_wrapper=====+++++++++++++++PVDATA++++++(1)=  Handle/PVNAME :", handle, pvname)   
    cdef pvdata data = PVDataHolderToStruct(pvd)
    #data.show()      
    py_cb = <object>(<void *> callback)
    #print(" cy_data_event_handler_wrapper=====+++++++++++++++PVDATA++++++(2)=  Handle/PVNAME :", handle, pvname)
    cdef HandleHelper hh
    mpid = hh.getUsrArgsAsUInt(handle)
    #print("monitorId", mpid)
    #monDictGlobal[mpid](handle, pvname, data)
    <object>py_cb(handle, pvname, data)

    #if monDictGlobal[mpid] !=  <object>py_cb:
    #    print( monDictGlobal[mpid])
    #    print( <object>py_cb)
   
    with nogil:
        return


cdef public void cy_ctrl_event_handler_wrapper(void * callback,  
                                               unsigned int handle, 
                                               string pvname, 
                                               PVCtrlHolder pvc) with gil:
   
    #print(" cy_data_event_handler_wrapper=====+++++++++++++++PVCTRL++++++=  Handle/PVNAME :", handle, pvname)
    cdef pvctrl data = PVCtrlHolderToStruct(pvc)
    cbobjt=<object> (<void *> callback)   
    cbobjt(handle, pvname, data) 
    with nogil:
        return


cdef public void cy_event_handler_wrapper(void * callback, 
                                          unsigned int handle, 
                                          string pvname) with gil:
    #print(" cy_event_handler_wrapper=====+++++++++++++++HANDLEPV++++++=  Handle/PVNAME :", handle, pvname)
    #(<object>args[0])(<int>args[1], <string>args[2]) 
    cbobjt=<object> callback   
    #cbobjt=<object>args[0]
    cbobjt(handle, pvname) 
    #cbobjt=<object>(<int>args[0], pvname)
        
    with nogil:
        return

cdef public void cy_handle_handler_wrapper(void * callback, 
     	    	 	                    unsigned int handle) with gil:
    #print(" cy_handle_handler_wrapper=====+++++++++++++++HANDLEPV++++++=  Handle:", handle)
     
    cbobjt=<object> callback   
    cbobjt(handle) 
    with nogil:
        return

cdef public void cy_connect_handler_wrapper(void * callback,
                                            unsigned int handle, 
                                            string pvname, 
                                            int status) with gil:
    #print(" cy_connect_handler_wrapper ====+++CONNECT++++++++++=  Handle/PV/Status:", handle, pvname, status)
    #print(" callback is ", <object> callback)
   
    if callback: 
        cbobjt=<object> callback   
        cbobjt(handle, pvname, status) 
     
    with nogil:
        return


cdef public void cy_cb_handle_get_wrapper(void * callback, unsigned int handle) with gil:
    print(" py_cb_handle_get_wrapper+++++++++++++GET++++++++++=  Handle:", handle)
    cbobjt=<object> (<void *> callback)   
    cbobjt(handle)
    with nogil:
        return


cdef public void cy_cb_handle_put_wrapper(void * callback, unsigned int handle) with gil:
    #print(" py_cb_handle_put_wrapper+++++++++++++PUT++++++++++=  Handle:", handle)
    cbobjt=<object> (<void *> callback)   
    cbobjt(handle)
    with nogil:
        return


# ***********public***************
