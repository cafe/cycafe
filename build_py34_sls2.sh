module unload gcc
module load gcc/7.3.0
#rm -f PyCafe.cpp
#rm -f PyCafe.h
#rm -f PyCafe.pxd
#ln -s PyCafe_sls.pxd PyCafe.pxd
#rm -f PyCafe.pyx
#ln -s PyCafe_sls.pyx PyCafe.pyx
module unload psi-python34
module unload psi-python36
module unload Python
module load psi-python34/2.1.0

python setup_py34_sls2.py build_ext -b ./python3.4-sls2/lib/${EPICS_HOST_ARCH} 
cp examples.py ./python3.4-sls2/lib/${EPICS_HOST_ARCH}


#/opt/psi/Programming/psi-python34/2.1.0/bin/python
