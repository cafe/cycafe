import time
import PyCafe
cafe = PyCafe.CyCafe()
cyca = PyCafe.CyCa()

PV1 = "ARIDI-BPM-01LE:X-AVG"
PV2 = "ARIDI-BPM-01LE:Y-AVG"
PV3 = "ARIDI-BPM-01SB:X-AVG"

def py_cb_single(handle, pv, pvdata):
    print(handle, pv, pvdata.value[0])

def py_cb_group(handle, pv, pvdata):
    print(handle, pv, pvdata.value[0])
    #pvdata.show()
    val, s, sl = cafe.getGroupCache('grp1')
    print(val, s, sl)


GROUP_MONITOR_ALL = False

#Note that in this example PV1 is connected
#twice, once outside the group and once within
#the group
h1 = cafe.open(PV1)
monid = cafe.monitorStart(h1, py_cb_single)

cafe.defineGroup('grp1', [PV1, PV2, PV3])
gh1 = cafe.groupOpen('grp1')

if GROUP_MONITOR_ALL:
    cafe.groupMonitorStart('grp1', py_cb_group)
else:
    #Indices match [P1, PV2, PV3]
    #So cbs provided for PV2 and PV3 only
    cbList = [None, py_cb_group, py_cb_group]
    s, sl = cafe.groupMonitorStartWithCBList('grp1', cbList)
    print("status", s, sl)

for i in range(0, 5):
    time.sleep(0.2)

cafe.printHandles()
cafe.terminate()

