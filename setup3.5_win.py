from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from numpy import get_include

#runtime_library_dirs do not override LD_LIBRARY_PATH!
#,
#					'C:/local/boost_1_62_0/lib64-msvc-10.0/libboost_thread-vc100-mt-s-1_62.lib',
#					'C:/local/boost_1_62_0/lib64-msvc-10.0/libboost_system-vc100-mt-s-1_62.lib',
#					'C:/local/boost_1_62_0/lib64-msvc-10.0/libboost_date_time-vc100-mt-s-1_62.lib',
#					'C:/local/boost_1_62_0/lib64-msvc-10.0/libboost_chrono-vc100-mt-s-1_62.lib'


setup(
	ext_modules = cythonize([Extension('PyCafe',['PyCafe.pyx'], 				 
	    language="c++",					
			include_dirs=[  
					'C:/Users/chrin/AppData/Local/Continuum/Anaconda3/env/py345/include',
                           	        'C:/epics/base-3.14.12.5/include',
                            	        'C:/epics/base-3.14.12.5/include/os/WIN32',
				        'C:/local/boost_1_62_0',
					'C:/local/boost_1_62_0/boost',
                           		'C:/CAFE/CAFE/cpp/include', 'C:/CAFE/CAFE/cpp','C:/Qt/qt-4.8.6-x64-msvc2010/include'                    		
								'.', get_include()],
			extra_compile_args = ["-MT", "-EHsc"], 					
             		library_dirs=[	
             				'C:/epics/base-3.14.12.5/lib/windows-x64',
                          		'C:/CAFE/CAFE/cpp',	
 					'C:/Users/chrin/AppData/Local/Continuum/Anaconda3/envs/py345/libs',
 					'C:/local/boost_1_62_0/lib64-msvc-10.0',					
 					'C:/Qt/qt-4.8.6-x64-msvc2010/lib'
                          	     ],  
                        extra_link_args=["-Release","-Manifest","-NoLogo"],
              	        #runtime_library_dirs=['C:/epics/base-3.14.12.5/lib/windows-x64',
                        #           	      'C:/CAFE/CAFE/cpp/lib',	                                   	      
                        #            	      'C:/Users/chrin/AppData/Local/Continuum/Anaconda3/envs/py345/libs'                                   	      
                        #                    ],
 			libraries=['ca','Com','cafe','python3','libboost_thread-vc100-mt-s-1_62',
 			'libboost_system-vc100-mt-s-1_62','libboost_date_time-vc100-mt-s-1_62',
 			'libboost_chrono-vc100-mt-s-1_62',
  			'QtCore4','QtXml4'
 			]
                        )			
			], annotate=False,	
			compiler_directives={'embedsignature': False, 'language_level': 3, 'c_string_type': 'str',
			'c_string_encoding' : 'ascii', 'warning_errors' : False, 'py2_import': False, 'warn.unreachable': False,
			'remove_unreachable': False})
)
