# file: PyCafe.pxd

# from cpython.ref cimport PyObject
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp.map cimport map
from libcpp.pair cimport pair

cimport cython

#import numpy as np
cimport numpy as cnp

IF  PY_VERSION_HEX <=  0x30501f0:
    include "python35_header.pxi"
      
cdef extern from "boost/multi_index_container.hpp" namespace "boost":
    cdef cppclass multi_index_container:
        pass

cdef extern from "boost/smart_ptr/shared_ptr.hpp" namespace "boost":
    cdef cppclass shared_ptr[T]:
        shared_ptr()
        shared_ptr(T * p)
        shared_ptr(const shared_ptr & )
        long use_count()
        T operator*()

cdef extern from "<map>" namespace "std":
    cdef cppclass multimap[T, U]:
        cppclass iterator:
            pair[T, U] & operator*() nogil
            iterator operator++() nogil
            iterator operator--() nogil
            bint operator == (iterator) nogil
            bint operator != (iterator) nogil
        multimap() nogil except +
        U & operator[](T &) nogil
        iterator begin() nogil
        iterator end() nogil
        pair[iterator, bint] insert(pair[T, U]) nogil
        iterator find(T & ) nogil


cdef extern from "epicsMutex.h":
    ctypedef LIBCOM_API epicsMutex  
    cdef cppclass  LIBCOM_API:
        pass
 

cdef extern from "epicsTime.h": 
    ctypedef void throwError
   

cdef extern from "cadef.h":

    ctypedef long chtype
    ctypedef oldSubscription * evid

    cdef struct ca_client_context:
        pass

    cdef struct oldSubscription:
        pass

    cdef struct oldChannelNotify:
        pass

    cdef struct event_handler_args:
        oldChannelNotify chid
        long type
        int count
        const void * dbr
        int status
        void * usr
        pass

    cdef:
        int ca_flush_io()
        int ca_poll()


cdef extern from "defines.h":
    double DEFAULT_TIMEOUT_PEND_EVENT
    double DEFAULT_TIMEOUT_PEND_IO
    const string INVALID_ENUM_RETURN_STRING
    const short INVALID_ENUM_RETURN_VALUE

    unsigned short  BSREAD_ZEROMQ_HIGH_WATER_MARK
    short           BSREAD_ZEROMQ_TIMEOUT_MS
    const short PVNAME_SIZE
    ctypedef struct etsDate:
        int year
        int mon
        int day
        int hour
        int min
        int sec
        unsigned long nsec
        int wday
        int yday
        int isdst
        pass

    ctypedef struct etsNorm:
        unsigned int secPastEpoch
        unsigned int nsec
        pass


cdef extern from "caeventmask.h":
    # ctypedef enum DBE_TYPE:
    #	DBE_VALUE=1
    #	DBE_LOG=2
    #	DBE_ARCHIVE=DBE_LOG
    #	DBE_ALARM=4
    #	DBE_PROPERTY=8
    unsigned int DBE_VALUE
    unsigned int DBE_LOG
    unsigned int DBE_ARCHIVE
    unsigned int DBE_ALARM
    unsigned int DBE_PROPERTY


cdef extern from "time.h":
    cdef struct tm:
        int tm_sec  # seconds after the minute	0-61*
        int tm_min  # minutes after the hour	0-59
        int tm_hour  # hours since midnight	0-23
        int tm_mday  # day of the month	1-31
        int tm_mon  # months since January	0-11
        int tm_year  # years since 1900
        int tm_wday  # days since Sunday	0-6
        int tm_yday  # days since January 1	0-365
        int tm_isdst  # Daylight Saving Time flag

cdef extern from "epicsTime.h":
    cdef struct epicsTimeStamp:
        unsigned int secPastEpoch   # seconds since 0000 Jan 1, 1990
        unsigned int nsec
    cdef cppclass epicsTime:
        epicsTime (const epicsTimeStamp &)
        epicsTime & operator = (const epicsTimeStamp & )
    cdef struct local_tm_nano_sec:
        tm ansi_tm
        unsigned long nSec

cdef extern from "db_access.h":
    unsigned int DBR_STRING  # returns a NULL terminated string
    unsigned int DBR_SHORT  # returns an unsigned short
    unsigned int DBR_INT  # returns an unsigned short
    unsigned int DBR_FLOAT  # returns an IEEE floating point value
    unsigned int DBR_ENUM  # returns an unsigned short which is the enum item
    unsigned int DBR_CHAR  # returns an unsigned char
    unsigned int DBR_LONG  # returns an unsigned long
    unsigned int DBR_DOUBLE  # returns a double precision floating point number
    # returns a string status structure (dbr_sts_string)
    unsigned int DBR_STS_STRING
    # returns a short status structure (dbr_sts_short)
    unsigned int DBR_STS_SHORT
    unsigned int DBR_STS_INT  # returns a short status structure (dbr_sts_int)
    # returns a float status structure (dbr_sts_float)
    unsigned int DBR_STS_FLOAT
    # returns an enum status structure (dbr_sts_enum)
    unsigned int DBR_STS_ENUM
    unsigned int DBR_STS_CHAR  # returns a char status structure (dbr_sts_char)
    unsigned int DBR_STS_LONG  # returns a long status structure (dbr_sts_long)
    # returns a double status structure (dbr_sts_double)
    unsigned int DBR_STS_DOUBLE
    # returns a string time structure (dbr_time_string)
    unsigned int DBR_TIME_STRING
    # returns a short time structure (dbr_time_short)
    unsigned int DBR_TIME_SHORT
    # returns a short time structure (dbr_time_short)
    unsigned int DBR_TIME_INT
    # returns a float time structure (dbr_time_float)
    unsigned int DBR_TIME_FLOAT
    # returns an enum time structure (dbr_time_enum)
    unsigned int DBR_TIME_ENUM
    unsigned int DBR_TIME_CHAR  # returns a char time structure (dbr_time_char)
    unsigned int DBR_TIME_LONG  # returns a long time structure (dbr_time_long)
    # returns a double time structure (dbr_time_double)
    unsigned int DBR_TIME_DOUBLE
    # returns a graphic string structure (dbr_gr_string)
    unsigned int DBR_GR_STRING
    # returns a graphic short structure (dbr_gr_short)
    unsigned int DBR_GR_SHORT
    unsigned int DBR_GR_INT  # returns a graphic short structure (dbr_gr_int)
    # returns a graphic float structure (dbr_gr_float)
    unsigned int DBR_GR_FLOAT
    unsigned int DBR_GR_ENUM  # returns a graphic enum structure (dbr_gr_enum)
    unsigned int DBR_GR_CHAR  # returns a graphic char structure (dbr_gr_char)
    unsigned int DBR_GR_LONG  # returns a graphic long structure (dbr_gr_long)
    # returns a graphic double structure (dbr_gr_double)
    unsigned int DBR_GR_DOUBLE
    # returns a control string structure (dbr_ctrl_int)
    unsigned int DBR_CTRL_STRING
    # returns a control short structure (dbr_ctrl_short)
    unsigned int DBR_CTRL_SHORT
    # returns a control short structure (dbr_ctrl_int)
    unsigned int DBR_CTRL_INT
    # returns a control float structure (dbr_ctrl_float)
    unsigned int DBR_CTRL_FLOAT
    # returns a control enum structure (dbr_ctrl_enum)
    unsigned int DBR_CTRL_ENUM
    # returns a control char structure (dbr_ctrl_char)
    unsigned int DBR_CTRL_CHAR
    # returns a control long structure (dbr_ctrl_long)
    unsigned int DBR_CTRL_LONG
    # returns a control double structure (dbr_ctrl_double)
    unsigned int DBR_CTRL_DOUBLE

    ctypedef	char[40]  		  dbr_string_t
    ctypedef	short	  		    dbr_short_t
    ctypedef	float	  		    dbr_float_t
    ctypedef	unsigned short 	dbr_enum_t
    ctypedef	unsigned char 	dbr_char_t
    ctypedef	int				      dbr_long_t
    ctypedef	double			    dbr_double_t

    cdef struct dbr_sts_string:
        pass
    cdef struct dbr_sts_short:
        pass
    cdef struct dbr_sts_int:
        pass
    cdef struct dbr_sts_float:
        dbr_float_t value
        dbr_short_t status
        pass
    cdef struct dbr_sts_enum:
        pass
    cdef struct dbr_sts_char:
        pass
    cdef struct dbr_sts_long:
        pass
    cdef struct dbr_sts_double:
        pass

    cdef struct dbr_time_string:
        pass
    cdef struct dbr_time_short:
        pass
    cdef struct dbr_time_int:
        pass
    cdef struct dbr_time_float:
        dbr_float_t value
        dbr_short_t status
        dbr_short_t severity
        epicsTimeStamp stamp
        pass
    cdef struct dbr_time_enum:
        pass
    cdef struct dbr_time_char:
        pass
    cdef struct dbr_time_long:
        pass
    cdef struct dbr_time_double:
        pass

    cdef struct dbr_gr_string:
        pass
    cdef struct dbr_gr_short:
        pass
    cdef struct dbr_gr_int:
        pass
    cdef struct dbr_gr_float:
        dbr_float_t value
        dbr_short_t status
        pass
    cdef struct dbr_gr_enum:
        pass
    cdef struct dbr_gr_char:
        pass
    cdef struct dbr_gr_long:
        pass
    cdef struct dbr_gr_double:
        pass

    cdef struct dbr_ctrl_string:
        pass
    cdef struct dbr_ctrl_short:
        pass
    cdef struct dbr_ctrl_int:
        pass
    cdef struct dbr_ctrl_float:
        dbr_float_t value
        dbr_short_t status
        pass
    cdef struct dbr_ctrl_enum:
        pass
    cdef struct dbr_ctrl_char:
        pass
    cdef struct dbr_ctrl_long:
        pass
    cdef struct dbr_ctrl_double:
        pass

cdef extern from "caerr.h":
    unsigned int ECA_NORMAL
    unsigned int ECA_ALLOCMEM
    unsigned int ECA_TOLARGE
    unsigned int ECA_TIMEOUT
    unsigned int ECA_BADTYPE
    unsigned int ECA_INTERNAL
    unsigned int ECA_GETFAIL
    unsigned int ECA_PUTFAIL
    unsigned int ECA_BADCOUNT
    unsigned int ECA_BADSTR
    unsigned int ECA_DISCONN
    unsigned int ECA_DBLCHNL
    unsigned int ECA_EVDISALLOW
    unsigned int ECA_BADMONID
    unsigned int ECA_BADMASK
    unsigned int ECA_IODONE
    unsigned int ECA_IOINPROGRESS
    unsigned int ECA_BADSYNCGRP
    unsigned int ECA_PUTCBINPROG
    unsigned int ECA_NORDACCESS
    unsigned int ECA_NOWTACCESS
    unsigned int ECA_ANACHRONISM
    unsigned int ECA_NOSEARCHADDR
    unsigned int ECA_NOCONVERT
    unsigned int ECA_BADCHID
    unsigned int ECA_BADFUNCPTR
    unsigned int ECA_ISATTACHED
    unsigned int ECA_UNAVAILINSERV
    unsigned int ECA_CHANDESTROY
    unsigned int ECA_BADPRIORITY
    unsigned int ECA_NOTTHREADED
    unsigned int ECA_16KARRAYCLIENT
    unsigned int ECA_CONNSEQTMO
    unsigned int ECA_UNRESPTMO

cdef extern from "caopCodes.h":
    cdef cppclass CAOPCodes:
        pass
        string message(int)
        int enumIs(string)
        show()


cdef extern from "tmDateMap.h":
    ctypedef enum TM_WDAY:
        TM_SUNDAY = 0
        TM_MONDAY = 1
        TM_TUESDAY = 2
        TM_WEDNESDAY = 3
        TM_THURSDAY = 4
        TM_FRIDAY = 5
        TM_SATURDAY = 6

    ctypedef enum TM_MONTHP:
        TM_JAN = 1
        TM_FEB = 2
        TM_MAR = 3
        TM_APR = 4
        TM_MAY = 5
        TM_JUN = 6
        TM_JUL = 7
        TM_AUG = 8
        TM_SEP = 9
        TM_OCT = 10
        TM_NOV = 11
        TM_DEC = 12

    cdef cppclass TMwdayText:
        string message(int)
        string asString(int)
        int enumIs(string)
        int asEnum(string)
        show()

    cdef cppclass TMmonthpText:
        string message(int)
        string asString(int)
        int enumIs(string)
        int asEnum(string)
        show()

cdef extern from "cafeDataType.h":
    ctypedef enum CAFE_DATATYPE:
        CAFE_TYPENOTCONN = -1
        CAFE_STRING = 0
        CAFE_SHORT = 1
        CAFE_INT = CAFE_SHORT
        CAFE_FLOAT = 2
        CAFE_ENUM = 3
        CAFE_USHORT = CAFE_ENUM
        CAFE_CHAR = 4
        CAFE_LONG = 5
        CAFE_DOUBLE = 6
        CAFE_NO_ACCESS = 7
        CAFE_INVALID_DATATYPE = 8
        CAFE_NOT_REQUESTED = 100
        CAFE_NOT_SHOWN = 101

    ctypedef union CAFE_DATATYPE_UNION:
        dbr_string_t str
        dbr_short_t  s
        dbr_float_t  f
        dbr_enum_t  us  # unsigned short us;
        dbr_char_t  ch  # unsigned char  ch;
        dbr_long_t   l  # int l;
        dbr_double_t d

    ctypedef CAFE_DATATYPE_UNION * CAFE_DATATYPE_UNION_SEQ

    cdef cppclass CAFEDataTypeCode:
        pass
        string message(int)
        string asString(int)
        int enumIs(string)
        int asEnum(string)
        show()

cdef extern from "cafeEnum.h":
    ctypedef enum DBR_TYPE:
        DBR_PRIMITIVE
        DBR_PLAIN = DBR_PRIMITIVE
        DBR_STS
        DBR_TIME
        DBR_GR
        DBR_CTRL
        # DBR_PUT_ACKT and DBR_PUT_ACKS Write only - used from global alarm acknowledge.
        DBR_PUT
        DBR_STSACK   # is DBR_STSACK_STRING
        DBR_CLASS    # is DBR_CLASS_NAME,
        DBR_NONE     # should not occur, but used internally within cafeVectors.h

    ctypedef enum ChannelFlushSendBufferPolicyKind:
        WITH_FLUSH_IO
        WITH_PEND_IO
        WITH_PEND_EVENT
        WITH_POLL

    ctypedef enum ChannelWhenToFlushSendBufferPolicyKind:
        FLUSH_AUTOMATIC = 0
        FLUSH_NOW = FLUSH_AUTOMATIC
        FLUSH_AFTER_EACH_CHANNEL_CREATION = FLUSH_NOW
        FLUSH_AFTER_EACH_CHANNEL_SUBSCRIPTION = FLUSH_NOW
        FLUSH_AFTER_EACH_MESSAGE = FLUSH_NOW
        FLUSH_AFTER_EACH_GROUP_CREATION = FLUSH_NOW
        FLUSH_DESIGNATED_TO_CLIENT

    ctypedef enum ChannelGetCacheWaitPolicyKind:
        GET_CACHE_NO_CHECK = 0
        GET_CACHE_NO_WAIT
        GET_CACHE_NOW = GET_CACHE_NO_WAIT
        GET_CACHE_WAIT

    ctypedef enum ChannelGetActionWhenMonitorPolicyKind:
        GET_FROM_CACHE
        GET_FROM_IOC

    ctypedef enum ChannelRequestPolicyKind:
        WITHOUT_CALLBACK
        WITH_CALLBACK_DEFAULT
        WITH_CALLBACK_USER_SUPPLIED

    ctypedef enum ChannelRequestDataTypePolicyKind:
        NATIVE_DATATYPE
        LOWEST_DATATYPE  # The smaller in byte size of type requested and native datatype

    ctypedef enum ChannelWaitForResponsePolicyKind:
        BLOCKING = 0
        WAIT = BLOCKING
        NON_BLOCKING = 1
        NO_WAIT = NON_BLOCKING

    ctypedef enum StatusMessageKind:
        NO_MESSAGE
        PRE_REQUEST
        FROM_REQUEST
        FROM_PEND
        FROM_CALLBACK
        FROM_MESSAGE

    ctypedef enum CallbackProgressKind:
        NOT_INITIATED
        PENDING
        COMPLETE


cdef extern from "exceptions.h":
    ctypedef struct CAFEException_pv:
        char            pv[60]  # PVNAME_SIZE
        char            pvAlias[60]  # PVNAME_SIZE
        unsigned int    handle
        CAFE_DATATYPE   dataTypeNative
        const  char * dataTypeNativeText
        int             statusCode
        const  char * statusCodeText
        const  char * statusMessage
        const  char * source
        unsigned int    ln
        pass

    ctypedef struct CAFEException_group:
        char            groupName[60]  # [PVNAME_SIZE];
        unsigned int    groupHandle
        int             statusCode
        const  char * statusCodeText
        const  char * statusMessage
        const  char * source
        unsigned int    ln
        pass

    cdef cppclass CAFEException_open:
        CAFEException_pv pvEx
        pass
        const char * what()

    cdef cppclass CAFEException_groupOpen:
        CAFEException_group groupEx
        pass
        const char * what()

cdef extern from "channelRegalia.h":
    cdef cppclass ChannelRegalia:
        pass
        oldChannelNotify * getChannelID()
        bint                getConnectFlag()
        const char * getHostName()
        string              getHostNameAsString()
        int		              getDataType()
        const char * getClassName()
        string              getClassNameAsString()
        unsigned short      getAccessRead()
        unsigned short      getAccessWrite()
        unsigned short      getReadAccess()
        unsigned short      getWriteAccess()
        unsigned int        getNelem()
        int                 getConnectionState()
        int                 getCafeConnectionState()
        string              getConnectionStateAsString()
        string              getCafeConnectionStateAsString()

    cdef cppclass ChannelDataStore:
        string description
        ChannelRegalia info
        PVDataHolder   pvd
        PVCtrlHolder   pvc
        pass

cdef extern from "handleHelper.h":

    cdef cppclass HandleHelper:
        etsDate _etsDate
        etsNorm _etsNorm

        HandleHelper()

        int addWidget(unsigned int, void * )
        int removeWidget(unsigned int, void *)
        int getWidgets(unsigned int, vector[void *])

        int setPyConnectCallbackFn(unsigned int, void *)

        int checkConsistency(unsigned int)

        ca_client_context getContextFromPV(const char *)
        ca_client_context getContextFromHandle(unsigned int)

        vector[string] getEnumStrings(unsigned int)
        string getStringFromEnum(unsigned int, unsigned short)
        short  getEnumFromString(unsigned int, string)

        const char * getPVFromHandle(unsigned int)
        unsigned int  getHandleFromPV(const char *)
        unsigned int  getGroupHandleFromGroupName(const char *)
        unsigned int  getHandleFromPVWithinGroup(const char *, unsigned int)
        vector[unsigned int] getHandlesFromWithinGroupV(unsigned int)
        int getChannelRegalia(unsigned int, ChannelRegalia)

        unsigned int getUsrArgsAsUInt(unsigned int)
        chtype getDataTypeCB(unsigned int)
        chtype getDbrDataTypeCB(unsigned int)
        DBR_TYPE getCafeDbrTypeCB(unsigned int)

        bint hasDescription(unsigned int)
        int getDescription(unsigned int, string &)
        int getUnits      (unsigned int, string &)
        int getPrecision  (unsigned int, short &)

        int getChannelDevice   (unsigned int, string &)
        int getChannelAttribute(unsigned int, string &)

        int getStatus(unsigned int)
        bint hasAlarmStatusSeverity(unsigned int)
        int getAlarmStatusSeverity(unsigned int, short[2])
        int[] getTimeStamp(unsigned int, epicsTimeStamp & ts)

        unsigned int   setCafeDbrTypeV(vector[unsigned int], DBR_TYPE)
        unsigned int   setCafeDbrType(unsigned int, DBR_TYPE)
        unsigned int   getCafeDbrType(unsigned int, DBR_TYPE &)
        unsigned int   setNelem(unsigned int, unsigned int)
        unsigned int   setNelemToNative(unsigned int)
        unsigned int   setNelemToRetrieveFromCache(unsigned int, unsigned int)
        unsigned int   setNelemToRetrieveFromCtrlCache(unsigned int, unsigned int)
        unsigned int   getNelemToRetrieveFromCache(unsigned int)
        unsigned int   getNelemToRetrieveFromCtrlCache(unsigned int)

        unsigned int   setNelemCtrl(unsigned int, unsigned int)
        unsigned int   getNelemRequestCtrl(unsigned)
        unsigned int   getNelemClientCtrl(unsigned)

        unsigned int   getNelemClient(unsigned int)
        unsigned int   getNelemNative(unsigned int)
        unsigned int   getNelemRequest(unsigned int)

        unsigned int   getNmonitor(unsigned int)
        vector[unsigned int] getMonitorIDs(unsigned int)
        vector[unsigned int] getMonitorIDsInWaiting(unsigned int)	
        int updateMonitorPolicyDeltaMS(unsigned int, unsigned int, unsigned short)

        int	getDataTypeNative (unsigned int, long &)
        int	getDataTypeRequest(unsigned int, long &)

        etsNorm getEpicsTimeStampAsUInt32(unsigned int)
        etsDate getEpicsTimeStampAsDate(unsigned int)
        etsDate epicsTimeStampToDate(epicsTimeStamp)
        etsDate etsNormToDate(etsNorm)
        etsDate unixTimeToDate(etsNorm)
        string  etsDateAsString(etsNorm)

        int printHandlesV(vector[unsigned int])

        unsigned int getDisconnectedHandles (vector[unsigned int] & , vector[string] & )
        unsigned int getHandles  (vector[unsigned int] & , vector[string] & )
        unsigned int getHandleStates  (vector[unsigned int] &, vector[string] & , vector[unsigned short] & )

        int getMonitorPolicyVector(unsigned int, vector[MonitorPolicy] & )


cdef extern from "policies.h":

    ctypedef void(*pCallback)(event_handler_args)

    cdef cppclass MonitorPolicy:

        MonitorPolicy()
        unsigned int getMonitorID()
        void setMask(unsigned int)
        void setNelem(unsigned int)
        void setDataType(chtype)
        void setCafeDbrType(DBR_TYPE)
        void setHandler(pCallback)
        void setDefaultHandler()
        void setNotifyDeltaMilliSeconds(unsigned short)
        void setNoCyCallbackParameters(unsigned short)
        void setCyCallback(void *)
        void setPyHandler()
        void setPyHandlerData()
        void setPyCyHandler(void *) 
        void setPyCyHandlerData(void *)
        void setPyCyHandlerHandle(void *)
        void setUserArgs(void *)
        unsigned int getMask()
        unsigned int getNelem()
        unsigned short getNotifyDeltaMilliSeconds()
        unsigned int getUserArgs()
        DBR_TYPE getCafeDbrType()
        unsigned int getDataType()
        unsigned int getDbrDataType()
        bint maskHasDBE_PROPERTY()
        bint maskHasDBE_VALUE()
        bint maskHasDBE_LOG()
        bint maskHasDBE_ALARM()

    cdef cppclass ChannelCreatePolicy:
        ChannelCreatePolicy()
        void setPyCallbackFlag(bint)
        bint getPyCallbackFlag()
        void setPyConnectHandler(void *)
        void * getPyConnectHandler()

    cdef cppclass ChannelOpenPolicy:
        ChannelOpenPolicy()
        void setWhenToFlushSendBuffer(ChannelWhenToFlushSendBufferPolicyKind)
        ChannelWhenToFlushSendBufferPolicyKind getWhenToFlushSendBuffer()
        void setFlushSendBufferKind(ChannelFlushSendBufferPolicyKind)
        void flushSendBufferNow()
        double setTimeout(double)
        double getTimeout()
        double setDefaultTimeout(double)
        double getDefaultTimeout()
        double setTimeoutToDefault()

    cdef cppclass ChannelRequestPolicy:
        ChannelRequestPolicy()
        ChannelWhenToFlushSendBufferPolicyKind  getWhenToFlushSendBuffer()
        ChannelWaitForResponsePolicyKind getWaitKind()
        ChannelRequestPolicyKind getMethodKind()
        pCallback getHandler()
        int  getCallbackStatus()
        void setHandler(pCallback h)
        void setPyHandlerGet(void *)
        void setPyHandlerPut(void *)
        void setMethodKind(ChannelRequestPolicyKind)
        void setWhenToFlushSendBuffer(ChannelWhenToFlushSendBufferPolicyKind)
        void setWaitKind(ChannelWaitForResponsePolicyKind)
        void setCallbackStatus(int)
        void setPolicy(ChannelWhenToFlushSendBufferPolicyKind, ChannelWaitForResponsePolicyKind, ChannelRequestPolicyKind)

    cdef cppclass ChannelRequestDataTypePolicy:
        ChannelRequestDataTypePolicy()
        ChannelRequestDataTypePolicy(ChannelRequestDataTypePolicyKind)
        ChannelRequestDataTypePolicyKind requestKind
        void setRequestKind(ChannelRequestDataTypePolicyKind)
        ChannelRequestDataTypePolicyKind getRequestKind()

    cdef cppclass ChannelTimeoutPolicy:
        ChannelTimeoutPolicy()
        bint   getSelfGoverningTimeout()
        void   setSelfGoverningTimeout(bool)
        double getTimeout()
        double getDeltaTimeout()
        unsigned short getNtries()
        double setTimeout(double)
        double setDeltaTimeout(double)
        unsigned short setNtries(unsigned short)

    cdef cppclass ChannelGetCacheWaitPolicy:
        ChannelGetCacheWaitPolicy()
        ChannelGetCacheWaitPolicy(ChannelGetCacheWaitPolicyKind)
        ChannelGetCacheWaitPolicyKind whenKind
        void setWaitKind(ChannelGetCacheWaitPolicyKind)
        ChannelGetCacheWaitPolicyKind getWhenKind()

    cdef cppclass ChannelGetActionWhenMonitorPolicy:
        ChannelGetActionWhenMonitorPolicy()
        ChannelGetActionWhenMonitorPolicy(
            ChannelGetActionWhenMonitorPolicyKind)
        ChannelGetActionWhenMonitorPolicyKind actionKind
        void setActionKind(ChannelGetActionWhenMonitorPolicyKind)
        ChannelGetActionWhenMonitorPolicyKind getActionKind()


cdef extern from "policyHelper.h":
    cdef cppclass PolicyHelper:

        int getChannelGetCacheWaitPolicy        (unsigned int, ChannelGetCacheWaitPolicy  &)
        int setChannelGetCacheWaitPolicy(unsigned int, ChannelGetCacheWaitPolicy)
        int setChannelGetCacheWaitPolicyAllHandles(ChannelGetCacheWaitPolicy)
        int getChannelGetActionWhenMonitorPolicy(unsigned int, ChannelGetActionWhenMonitorPolicy &)
        int setChannelGetActionWhenMonitorPolicy(unsigned int, ChannelGetActionWhenMonitorPolicy)
        int setChannelGetActionWhenMonitorPolicyAllHandles(ChannelGetActionWhenMonitorPolicy)

        int getChannelRequestDataTypePolicy(unsigned int, ChannelRequestDataTypePolicy &)
        int setChannelRequestDataTypePolicy(unsigned int, ChannelRequestDataTypePolicy)
        int setChannelRequestDataTypePolicyAllHandles(ChannelRequestDataTypePolicy)

        int getChannelRequestPolicyGet (unsigned int, ChannelRequestPolicy & )
        int setChannelRequestPolicyGet(unsigned int, ChannelRequestPolicy)
        int setChannelRequestPolicyGetAllHandles(ChannelRequestPolicy)

        int getChannelRequestPolicyPut (unsigned int, ChannelRequestPolicy &)
        int setChannelRequestPolicyPut(unsigned int, ChannelRequestPolicy)
        int setChannelRequestPolicyPutAllHandles(ChannelRequestPolicy)

        int getChannelTimeoutPolicyGet (unsigned int, ChannelTimeoutPolicy &)
        int setChannelTimeoutPolicyGet(unsigned int, ChannelTimeoutPolicy)

        int getChannelTimeoutPolicyPut (unsigned int, ChannelTimeoutPolicy &)
        int setChannelTimeoutPolicyPut(unsigned int, ChannelTimeoutPolicy)

        int getTimeout(unsigned int, double &)
        int getTimeout(unsigned int, double & , double & )
        int getTimeoutMin(double & , double & )
        int getTimeoutMax(double & , double & )
        int setTimeout(unsigned int, double)
        int setTimeout(unsigned int, double, double)
        int setTimeout(double)
        int setTimeout(double, double)
        int setSelfGoverningTimeout(bint)
        int getSGTimeout (unsigned int, double &, double & )
        int getSGTimeout (unsigned int, double &)
        int getSGTimeoutMin (double, double &)
        int getSGTimeoutMax (double, double & )
        int setSGTimeout(unsigned int, double, double)
        int setSGTimeout(unsigned int, double)
        int setSGTimeout(double, double)
        int setSGTimeout(double)
        int setSGSelfGoverningTimeout(bint)

# cdef extern from "hashConduit.h":
#	ctypedef multi_index_container<> cafeConduit_set
#	ctypedef cafeConduit_set cafeConduit_set_by_handle
#	ctypedef multi_index_container.iterator it_handle


cdef extern from "statusCodes.h":
    cdef int ICAFE_STATUS_BASE = 600
    cdef int ICAFE_STATUS_CS = ICAFE_STATUS_BASE
    cdef int ICAFE_STATUS_CFT = 700
    cdef int ICAFE_STATUS_CA_OP = 800
    cdef int ICAFE_STATUS_DAQ = 900
    cdef int ICAFE_STATUS_ERROR = 1000
    cdef int ICAFE_FILE_ERROR = 1100
    cdef int ICAFE_SERVICE_ERROR = 1200
    cdef int ICAFE_BITSHUFFLE_ERROR = 1300
    cdef int ICAFE_BSREAD_BASE = 1500
    cdef int ICAFE_ERRNO_BASE = 5000
    cdef int ICAFE_SUCCESS = ECA_NORMAL
    cdef int ICAFE_NORMAL = ECA_NORMAL

    ctypedef enum CAFE_CS_STATE:
        ICAFE_CS_NEVER_CONN = 600
        ICAFE_CS_PREV_CONN
        ICAFE_CS_CONN
        ICAFE_CS_CLOSED
        ICAFE_CS_DISCONN
        ICAFE_CS_UNKNOWN

    ctypedef enum CAFE_CFT_STATE:
        ICAFE_TYPENOTCONN = 700
        ICAFE_RULE_FALSE
        ICAFE_BADCOUNT
        ICAFE_CALLBACK_NOT_YET_INVOKED
        ICAFE_WAITING_FOR_PREV_CALLBACK
        ICAFE_CACHE_EMPTY
        ICAFE_CHANNEL_BLOCKING_POLICY_CONFLICT
        ICAFE_MONITOR_DELAYED_AS_CONN_DOWN
        ICAFE_HAS_MONITOR_GET_DONE_FROM_CACHE
        ICAFE_SET_AND_GET_MISMATCH

    ctypedef enum CAFE_CC_STATE:
        ICAFE_CA_OP_GET = 800
        ICAFE_CA_OP_PUT
        ICAFE_CA_OP_CREATE_CHANNEL
        ICAFE_CA_OP_ADD_EVENT
        ICAFE_CA_OP_CLEAR_EVENT
        ICAFE_CA_OP_OTHER
        ICAFE_CA_OP_CONN_UP
        ICAFE_CA_OP_CONN_DOWN

    ctypedef enum CAFE_STATUS_DAQ:
        ICAFE_DAQ_RUN = 900
        ICAFE_DAQ_PAUSED
        ICAFE_DAQ_STOPPED

    ctypedef enum CAFE_FILE_ERROR:
        ECAFE_LOAD_COLLECTION = 1100
        ECAFE_LOAD_GROUP

    ctypedef enum CAFE_ERROR_STATE:
        ECAFE_NODATA = 1000
        ECAFE_INVALID_TYPE
        ECAFE_BADCOUNT
        ECAFE_BADSTR
        ECAFE_BADTYPE
        ECAFE_NO_CONVERT
        ECAFE_NULLCONTEXT
        ECAFE_NULLCHID
        ECAFE_NULLEVID
        ECAFE_UNKNOWN_COLLECTION
        ECAFE_EMPTY_COLLECTION
        ECAFE_COLLECTION_PREV_DEF
        ECAFE_COLLECTION_INVALID_MEMBER
        ECAFE_RULE_FALSE
        ECAFE_UNKNOWN_GROUP
        ECAFE_EMPTY_GROUP
        ECAFE_GROUP_PREV_DEF
        ECAFE_INVALID_HANDLE
        ECAFE_INVALID_GROUP_HANDLE
        ECAFE_NORDACCESS
        ECAFE_NOWTACCESS
        ECAFE_TIMEOUT
        ECAFE_CANNOT_OPEN_FILE
        ECAFE_INVALID_SWITCH_CASE
        ECAFE_PVALIAS_PREV_DEF
        ECAFE_PVALIAS_INVALID
        ECAFE_PVNAME_PREV_DEF_AS_PVALIAS
        ECAFE_DEVICE_ATTRIB_NOT_FOUND
        ECAFE_HASH_UNIQUEID_EXISTS
        ECAFE_WRONG_CA_CONTEXT
        ECAFE_INVALID_CAFENUM_POLICY_TYPE
        ECAFE_MAX_MONITORS_PER_CHAN_EXCEEDED
        ECAFE_INVALID_ENUM_INDEX
        ECAFE_PVGROUP_GROUPHANDLE_MISMATCH
        ECAFE_TIMEOUT_SET_AND_MATCH
        ECAFE_HANDLE_MISMATCH_SET_AND_MATCH
        ECAFE_INCONSISTENT_CONTAINER_CORRECTED

    ctypedef enum CAFE_SERVICE_ERROR:
        ECAFE_BPM_DATA_IS_INVALID = 1200

    ctypedef enum CAFE_BITSHUFFLE_ERROR:
        ECAFE_BITSHUFF_DECOMPRESS = 1300
        ECAFE_BITSHUFF_ALLOCMEM
        ECAFE_BITSHUFF_REALLOCMEM
        ECAFE_BITSHUFF_BADCOUNT

    ctypedef enum CAFE_BSREAD_ERROR:
        ECAFE_BSREAD_MULTIPART_MESS_NODATA = 1500
        ECAFE_BSREAD_PARSEFAIL_MAINHEADER
        ECAFE_BSREAD_PARSEFAIL_DATAHEADER
        ECAFE_BSREAD_ZMQSTREAM_NULL

    ctypedef enum CAFE_ERRNO_ERROR:
        ERRNO_EINTR = 5004
        ERRNO_EAGAIN = 5011
        ERRNO_EFAULT = 5014
        ERRNO_EINVAL = 5022
        ERRNO_ENOTSOCK = 5088
        ERRNO_EPROTONOSUPPORT = 5093

    ctypedef enum EPICS_GLOBAL_ALARM_SEVERITY:
        SEV_NO_ALARM = 0
        SEV_MINOR
        SEV_MAJOR
        SEV_INVALID

    ctypedef enum EPICS_GLOBAL_ALARM_CONDITION:
        STAT_NO_ALARM = 0
        STAT_READ
        STAT_WRITE
        STAT_HIHI
        STAT_HIGH
        STAT_LOLO
        STAT_LOW
        STAT_STATE
        STAT_COS
        STAT_COMM
        STAT_TIMEOUT
        STAT_HWLIMIT
        STAT_CALC
        STAT_SCAN
        STAT_LINK
        STAT_SOFT
        STAT_BAD_SUB
        STAT_UDF
        STAT_DISABLE
        STAT_SIMM
        STAT_READ_ACCESS
        STAT_WRITE_ACCESS

    cdef cppclass CAFEStatusSeverity:
        CAFEStatusSeverity()
        string message(int)

    cdef cppclass CAFEStatusInfo:
        CAFEStatusInfo()
        string message(int)

    cdef cppclass CAFEStatusCode:
        CAFEStatusCode()
        void getStates(vector[int], vector[string])
        string message(int)
        string msgIDAsString(int)
        int enumIs(string)
        bint isTimeout(int)
        void printAll()

    cdef cppclass CAFEStatus:
        CAFEStatus()
        string severity(int)
        string asString(int)
        string msgID(int)
        string code(int)
        string info(int)
        string message(int)
        bint isTimeout(int)
        void report(int)


cdef extern from "PVDataHolder.h":

    cdef cppclass PVDataHolder:
        etsDate _etsDate
        etsNorm _etsNorm

        PVDataHolder() except +
        PVDataHolder(unsigned int) except +
        # shared_ptr [CAFE_DATATYPE_UNION []] val;
        # shared_ptr[double []]  getDouble()
        # shared_ptr[float  []]  getFloat()
        # shared_ptr[int    []]  getInt()
        # shared_ptr[string []]  getString()

        # shared_ptr[vector [double]]  getAsVDouble()
        # shared_ptr[vector [float]]   getAsVFloat()
        # shared_ptr[vector [int]]     getAsVInt()
        # shared_ptr[vector [string]]  getAsVString()

        char * getAsDbr_string_t()
        char * getAsDbr_string_t(unsigned int)
        string getAsString()
        string getAsString(unsigned int)
        double getAsDouble()
        double getAsDouble(unsigned int)
        float  getAsFloat()
        float  getAsFloat(unsigned int)
        short  getAsShort()
        short  getAsShort(unsigned int)
        unsigned short  getAsUShort()
        unsigned short  getAsUShort(unsigned int)
        unsigned char   getAsChar()
        unsigned char   getAsChar(unsigned int)
        int   getAsLong()
        int   getAsLong(unsigned int)
        int   getAsLongLong()
        int   getAsLongLong(unsigned int)

        string getWFAsString()
        string concatToString()

        unsigned long long getPulseID()

        unsigned int getUserNo()

        unsigned int getNelem()
        int setNelem(unsigned int)
        void setRule(bint)
        bint getRule()
        void setHasAlarm(bint)
        void setHasTS(bint)
        bint getHasAlarm()
        bint getHasTS()

        int setString(string)
        int setDouble(double)
        int setInt(int)
        int setVString(vector[string])
        int setVDouble(vector[double])
        int setVInt(vector[int])

        int getAlarmStatus()
        int getAlarmSeverity()
        string getAlarmStatusAsString()
        string getAlarmSeverityAsString()
        int getStatus()
        string getStatusAsString()

        etsNorm getEpicsTimeStampAsUInt32()
        etsDate getEpicsTimeStampAsDate()
        etsDate getUnixTimeAsDate(etsNorm)
        string  getEpicsTimeStampAsString()
        string  getBSTimeStampAsString()

        unsigned int getDataType()
        char * getPV()
        char * getPVName()
        void valReset()


cdef extern from "PVCtrlHolder.h":

    cdef cppclass PVCtrlHolder:

        PVCtrlHolder() except +
        PVCtrlHolder(unsigned int) except +

        short getPrecision()
        const char * getUnits()
        string getUnitsAsString()
        short getNoEnumStrings()
        vector[string] getEnumStrings()
        double getUpperDispLimit_AsDouble()
        double getLowerDispLimit_AsDouble()
        double getUpperAlarmLimit_AsDouble()
        double getUpperWarningLimit_AsDouble()
        double getLowerWarningLimit_AsDouble()
        double getLowerAlarmLimit_AsDouble()
        double getUpperCtrlLimit_AsDouble()
        double getLowerCtrlLimit_AsDouble()

        float getFloat(unsigned int)

        string getAsString()
        string getAsString(unsigned int)

        char * getAsDbr_string_t()
        char * getAsDbr_string_t(unsigned int)
        double getAsDouble()
        double getAsDouble(unsigned int)
        float getAsFloat()
        float getAsFloat(unsigned int)
        short   getAsShort()
        short   getAsShort(unsigned int)
        unsigned short   getAsUShort()
        unsigned short   getAsUShort(unsigned int)
        unsigned char   getAsChar()
        unsigned char   getAsChar(unsigned int)
        int   getAsLong()
        int   getAsLong(unsigned int)
        int   getAsLongLong()
        int   getAsLongLong(unsigned int)

        unsigned int getNelem()
        int setNelem(unsigned int)

        int getAlarmStatus()
        int getAlarmSeverity()
        int getStatus()

        unsigned int getDataType()


cdef extern from "PVGroup.h":

    cdef cppclass PVGroup:
        PVGroup()
        PVDataHolder * getPVData()
        unsigned int getNPV()
        string getNameAsString()
        unsigned int getGroupHandle()
        int getStatusGroup()
        void setPVData(PVDataHolder * )
        void show()
        void showMax(unsigned int)
        void showMaxMax(unsigned int, unsigned int)
    pass

# cdef extern from "instant.h":
#	template <class CTYPE> class Instant


cdef extern from "cafe.h":

    cdef cppclass CAFE:
        ChannelCreatePolicy channelCreatePolicy
        ChannelOpenPolicy channelOpenPolicy
        ChannelOpenPolicy channelOpenGroupPolicy
        ChannelOpenPolicy channelMonitorPolicy
        # ChannelRequestPolicy channelRequestPolicyGet
        # ChannelRequestPolicy channelRequestPolicyPut

        PolicyHelper getPolicyHelper()
        HandleHelper getHandleHelper()

        string _epics_version() nogil
        string _ca_version() nogil
        int _ca_pend_io(double)  nogil
        int _ca_pend_event(double)  nogil
        int _ca_flush_io() nogil
        int _ca_poll() nogil

        int addWidget(unsigned int, void * )
        int removeWidget(unsigned int, void *)
        int getWidgets(unsigned int, vector[void *])

        int init()
        int open(char * , unsigned int & ) nogil except +
        int openV(vector[string], vector[unsigned int] &) nogil except +

        int openChannelsWithHandles(vector[unsigned int]) nogil
        int closeChannelKeepHandle(unsigned int) nogil
        int closeChannelsKeepHandles(vector[unsigned int]) nogil
        int reconnect(vector[unsigned int]) nogil

        void openNoWait() nogil
        void openPrepare() nogil
        void openGroupPrepare() nogil

        bint initCallbackCompleteV(vector[unsigned int])
        bint initCallbackCompleteHandle(unsigned int)

        void openNowAndWaitHandleV(vector[unsigned int], double) nogil

        void openNow() nogil
        void openNowAndWait(double) nogil
        void openGroupNowAndWait(double) nogil

        void openGroupNowAndWaitForInput(double, vector[unsigned int]) nogil
        void groupOpenNowAndWait(double, vector[unsigned int]) nogil

        void openMonitorPrepare() nogil
        void openMonitorNow() nogil
        void openMonitorNowAndWait(double) nogil

        int  closeHandlesV(vector[unsigned int]) nogil
        int  close(unsigned int) nogil
        int  closeChannels() nogil

       
        vector[unsigned int] closeDisconnectedChannelsFromWithinGroupV(unsigned int) nogil

        int supplementHandles()	nogil
        int supplementHandlesV(vector[unsigned int]) nogil
        int supplementHandle(unsigned int) nogil

        bint allChannelsConnected()
        bint isChannelConnected(unsigned int)
        int  printDisconnectedHandles()
        int  printHandle(unsigned int)
        int  printHandles()

        bint isEnum(unsigned int)
        bint isValid(unsigned int)

        void printStatusMessage(int)
        void printStatus(unsigned int, int)

        int  attachContextByPVName(const char *)
        int  attachContextByHandle(unsigned int)

        unsigned int getNelemClient(unsigned int)
        unsigned int getNelemNative(unsigned int)
        unsigned int getNelemRequest(unsigned int)

        const char * getPVFromHandle(unsigned int)
        unsigned int getHandleFromPV  (const char *)

        void loadSFGroups(string)

        int getChannelDataStore(unsigned int, ChannelDataStore) nogil
        int getChannelInfo(unsigned int, ChannelRegalia &)

        CAFEStatus  getCafeStatus()
        CAFEStatusSeverity getCafeStatusSeverity()

        PVDataHolder * getPVData(vector[unsigned int])

        vector[string] getFromGlobalChannelList(vector[string])

        string getChannelIDAsString(chid)

        int wfExpress(const unsigned int handle, const unsigned int, dbr_char_t *) 

        int getWFAsString(unsigned int, string &) nogil
        int getWFAsStringCache(unsigned int, string &)

        int getNonBlocking(unsigned int) nogil
        int getCacheFloatArray(unsigned int, float *)
        int getCacheDoubleArray(unsigned int, double *)
        int getCacheShortArray(unsigned int, short * )
        int getCacheCharArray(unsigned int, dbr_char_t * )
        int getCacheLongArray(unsigned int, int * )
        int getCacheStringArray(unsigned int, string * )
        int getCacheDbrStringArray(unsigned int, dbr_string_t[])
        int getCacheDouble(unsigned int, double &)
        int getCacheLong(unsigned int, int & )
        int getCacheString(unsigned int, string & )

        int getDouble(unsigned int, double &) nogil
        int getLong(unsigned int, int &) nogil
        int getString(unsigned int, string &) nogil
        int getStringTS(unsigned int, string & , short & , short&, epicsTimeStamp&) nogil

        int getCharArray(unsigned int, dbr_char_t[]) nogil
        int getShortArray(unsigned int, short[]) nogil
        int getUShortArray(unsigned int, unsigned short[]) nogil
        int getDoubleArray(unsigned int, double[]) nogil
        int getFloatArray(unsigned int, float[]) nogil
        int getLongArray(unsigned int, int[]) nogil
        int getStringArray(unsigned int, string[]) nogil
        int getDbrStringArray(unsigned int, dbr_string_t[]) nogil

        int getStringVector(const unsigned int, vector[string]) nogil
        int getShortVector(const unsigned int, vector[short]) nogil
        int getFloatVector(const unsigned int, vector[float]) nogil
        int getUShortVector(const unsigned int, vector[unsigned short]) nogil
        int getUCharVector(const unsigned int, vector[unsigned char]) nogil
        int getLongVector(const unsigned int, vector[dbr_long_t]) nogil
        int getLongLongVector(const unsigned int, vector[long long]) nogil
        int getDoubleVector(const unsigned int, vector[double]) nogil

        int get(unsigned int, PVDataHolder &) nogil
        int getCtrl(unsigned int, PVCtrlHolder &) nogil
        int getCtrlCache(unsigned int, PVCtrlHolder &) nogil

        int getPVArray(vector[unsigned int], PVDataHolder *) nogil
        int getCachePVArray(vector[unsigned int], PVDataHolder * )
        int getCachePVArrayNoWait(vector[unsigned int], PVDataHolder *)

        int getV(vector[unsigned int], vector[int]) nogil
        int getCacheVStr(vector[unsigned int], vector[string] &, vector[int] &) nogil
        int getCacheVLong(vector[unsigned int], vector[dbr_long_t] &, vector[int] &) nogil
        int getCacheVDouble(vector[unsigned int], vector[double] &, vector[int] &) nogil

        int waitForBundledEvents(vector[unsigned int], vector[int]) nogil
        int waitForGetEvent(unsigned int) nogil

        # bs read
        int monitorPulseID() nogil
        int monitorStopPulseID() nogil
        int setPulseIDBufferSize(unsigned int, unsigned short)
        int setPulseIDBufferSizeAll(unsigned short)

        int monitorStart(unsigned int, MonitorPolicy &) nogil 
        int monitorStopAll() nogil
        int monitorStop(unsigned int) nogil
        int monitorStopWithID(unsigned int, unsigned int) nogil
        int getCache(unsigned int, PVDataHolder &)

        MonitorPolicy * createMonitorPolicyArray(const unsigned int)
        int groupMonitorStart(unsigned int, vector[int]&, vector[MonitorPolicy] & ) nogil
        int groupMonitorStop (unsigned int, vector[int] & ) nogil
        int groupDefine(char * , vector[const char * ])
        int groupDefineFromCollection(const char * , const const char *, vector[string ])
        int groupOpen(char * , unsigned int & ) nogil except +
        int groupClose(unsigned int) nogil except +
        int groupCloseKeepName(unsigned int,  bint) nogil except +
        int groupAttach(unsigned int, PVGroup &) nogil
        int groupGet(unsigned int, PVGroup &) nogil
        int groupGetCache(unsigned int, PVGroup &) nogil
        int groupSet(unsigned int, PVGroup &) nogil

        int groupMemberList(const char * , vector[string] & )


        int loadCollectionsFromXML(const char *) nogil
        int loadGroupsFromXML(const char *) nogil
        int devicePositionMap(const char *, map[float, string]&)
        int devicePositionV(const char *, vector[string] & , vector[float] & )

        int collectionDefine(const char *, vector[string]) nogil
        int collectionList(vector[string] &) nogil
        int collectionMemberList(const char *, vector[string] &) nogil

        int restoreFromXML(const char * snapshotFile) nogil
        int snapshot2XML(PVGroup pg) nogil
        void openGroupXMLFile(string fileName) nogil
        void closeGroupXMLFile(string fileName) nogil
        int group2XML(const char * grpName, string fileName) nogil

        int groupList      (vector[string] &)

        bint  isGroup(const char *) nogil
        bint  isCollection(const char *) nogil

        int printStatusIfError(vector[unsigned int], vector[int])

        int setStringArray(unsigned int, char **) nogil
        int setCharArray(unsigned int, unsigned char *) nogil
        int setLongArray(unsigned int, int *) nogil
        int setDoubleArray(unsigned int, double *) nogil

        int setPVArray(vector[unsigned int], PVDataHolder *) nogil

        int setVString(unsigned int, vector[string]) nogil
        int setVChar(unsigned int, vector[unsigned char]) nogil
        int setVShort(unsigned int, vector[short]) nogil
        int setVUShort(unsigned int, vector[unsigned short]) nogil
        int setVLong(unsigned int, vector[dbr_long_t]) nogil
        int setVFloat(unsigned int, vector[float]) nogil
        int setVDouble(unsigned int, vector[double]) nogil

        int setVVString(vector[unsigned int], vector[string], vector[int] & ) nogil
        int setVVChar(vector[unsigned int], vector[unsigned char], vector[int] & ) nogil
        int setVVLong(vector[unsigned int], vector[int], vector[int] & ) nogil
        int setVVDouble(vector[unsigned int], vector[double], vector[int] & ) nogil

        int setString(unsigned int, string) nogil
        int setChar(unsigned int, unsigned char) nogil
        int setShort(unsigned int, short) nogil
        int setUShort(unsigned int, unsigned short) nogil
        int setLong(unsigned int, int) nogil
        int setLongLong(unsigned int, long long) nogil
        int setFloat(unsigned int, float) nogil
        int setDouble(unsigned int, double) nogil

        int setAndMatch(const unsigned int, double, const unsigned int, double tolerance, double timeout, bint printFlag) nogil
        int setAndMatchMany(vector[unsigned int], vector[double], vector[unsigned int], double tolerance, double timeout, bint printFlag) nogil

        int gameSetAndMatch(vector[unsigned int], vector[double], vector[unsigned int], vector[string], vector[unsigned int], double tolerance, double timeout, bint printFlag) nogil

        int match(double, const unsigned int, double tolerance, double timeout, bint printFlag) nogil
        int matchMany(vector[double], vector[unsigned int], double tolerance, double timeout, bint printFlag) nogil

        int matchManyWithStatus(vector[double], vector[unsigned int], double tolerance, double timeout, bint printFlag, vector[int] &) nogil

        #int readDBPMOffsets(DBPMKeeper &)
        #int getDBPM(DBPMKeeper &) nogil
      
        #int prepareDBPM(vector[string] & , vector[unsigned int] & ,  vector[string] & , vector[float] & ) nogil

        int setNelemToOne(const unsigned int)
        int setNelemToPrevious(const unsigned int, const unsigned int)
        int setNelemToRetrieveFromCacheToOne(const unsigned int)
        int setNelemToRetrieveFromCacheToPrevious(const unsigned int, const unsigned int)
        int getNelemToRetrieveFromCache(const unsigned int)
        int terminate() nogil

        # BSREAD
        #int initBSwithCA(BSDataHolder &) nogil
        #int setBS(BSDataHolder &) nogil
        #int setBS2CAGroup(BSDataHolder &) nogil
        #int getBS(BSDataHolder &) nogil
        #int closeBS(BSDataHolder &) nogil

    pass
